
``ReCooDy`` — The Resource-flow-based Cooperation Dynamics model
================================================================

I introduce the ReCooDy model in detail in

* Benjamin Herdeanu. 2021. **Emergence of Cooperation in Evolutionary Social Interaction Networks.** Doctoral dissertation. *heiDOC*. Heidelberg University. DOI: `0.11588/heidok.00030725 <https://doi.org/10.11588/heidok.00030725>`_ 

Abstract
--------

"Cooperation is the cornerstone of life and human societies—its evolution is a perennial question.
Evolutionary Game Theory models elucidated mechanisms promoting cooperation.
However, they pay little attention to its emergence, operate with predefined states of defectors and cooperators, and presume defection as the natural state.
Models need more realism.
Here, I introduce ReCooDy, a model combining population dynamics, limited resources, dynamic networks, coevolution of nine parameters, and optional social dilemma interactions, all implemented in the Utopia framework, which allows performant, flexible, and reliable computer simulation.
ReCooDy investigates the emergence of cooperation and defection in a generalized continuous public goods interaction that incorporates "true defection"—exploitative destruction for selfish benefits.
I show that macroscopically classifying agents as selfish or selfless oversimplifies the intricacy of the emerging mesoscopic social dilemma. 
Simulations exhibit the emergence of cooperation, even for minute synergies if interacting is not crucial. 
For moderate synergy, agents evolve specializations that depend vitally on the interactions. 
Further, defection emerges naturally as a response to cooperation.
ReCooDy exhibits recurrent dynamics patterns with history dependence and frequent strategy collapses through cascaded thresholds caused by Red Queen dynamics. 
Thus, simulations reveal ReCooDy's deterministically chaotic self-organized nature and the overall unintuitive phenomenology of more realistic social dynamics modeling."
