#ifndef UTOPIA_MODELS_RECOODY_PARAMS_HH
#define UTOPIA_MODELS_RECOODY_PARAMS_HH

#include <string>
#include <utility>

#include "utopia/data_io/cfg_utils.hh"
#include "utopia/core/types.hh"
#include "types.hh"

namespace Utopia::Models::ReCooDy
{
/// Parameters required for the evolution mechanism
struct EvoParams
{
    /// The fixed death probability
    const double p_death;

    /// The threshold under which agents will die
    const double death_threshold;

    /// Start tracking death states from this time on.
    const double track_deaths_from_time;

    /// Skip every n agents when tracking death states
    const std::size_t skip_every_n_agents;

    /// The birth probability for new agents
    const double p_birth;

    /// The investment to give birth
    const double birth_investment;

    /// The resources threshold needed to give birth
    const double birth_threshold;

    /// The resources that is transferred from parent to offspring
    const double birth_transferred_resources;

    /// The interval defining strength mutation
    const std::pair<double, double> strength_mutation;

    /// The interval defining investment mutation
    const std::pair<double, double> investment_mutation;

    /// The interval defining interaction probability mutation
    const std::pair<double, double> p_synergistic_mutation;

    /// The probability to add local links
    /** The inverse probability defines global linking
     *  (within the whole population)
     */
    const std::pair<double, double> p_add_local_links_mutation;

    // -- Constructor ---------------------------------------------------------
    EvoParams(const DataIO::Config& cfg) :
        p_death(get_as<double>("p_death", cfg)),
        death_threshold(get_as<double>("death_threshold", cfg)),
        track_deaths_from_time(get_as<double>("track_deaths_from_time", cfg)),
        skip_every_n_agents(get_as<std::size_t>("skip_every_n_agents", cfg, 0)),
        p_birth(get_as<double>("p_birth", cfg)),
        birth_investment(get_as<double>("birth_investment", cfg)),
        birth_threshold(get_as<double>("birth_threshold", cfg)),
        birth_transferred_resources(
            get_as<double>("birth_transferred_resources", cfg)),
        strength_mutation(
            get_as<std::pair<double, double>>("strength_mutation", cfg)),
        investment_mutation(
            get_as<std::pair<double, double>>("investment_mutation", cfg)),
        p_synergistic_mutation(
            get_as<std::pair<double, double>>("p_synergistic_mutation", cfg)),
        p_add_local_links_mutation(
            get_as<std::pair<double, double>>("p_add_local_links_mutation",
                                              cfg)) {};
};

// -- Resource Parameters -----------------------------------------------------
/// Parameters defining the basic resource extraction
struct BasicResourceParams
{
    /// The amount of available resources
    const double amount;

    /// The resource intake of an agent
    const double intake;

    /// The config constructor
    BasicResourceParams(const DataIO::Config& cfg) :
        amount(get_as<double>("amount", cfg)),
        intake(get_as<double>("intake", cfg)) {};
};

/// Parameters defining the synergistic resource extraction
struct SynergisticResourceParams
{
    /// The synergy factor
    const double r;

    /// Whether to allow negative investment
    const bool allow_negative_investment;

    /// The amount of the resource
    const double amount;

    /// The config-constructor
    SynergisticResourceParams(const DataIO::Config& cfg) :
        r(get_as<double>("r", cfg)),
        allow_negative_investment(
            get_as<bool>("allow_negative_investment", cfg)),
        amount(get_as<double>("amount", cfg)) {};
};

/// Parameters defining reesources
struct ResourceParams
{
    /// The basic resource parameters
    const BasicResourceParams basic;

    /// The synergistic resource parameter
    const SynergisticResourceParams synergistic;

    /// The config-constructor
    ResourceParams(const DataIO::Config& cfg) :
        basic(get_as<DataIO::Config>("basic", cfg)),
        synergistic(get_as<DataIO::Config>("synergistic", cfg)) {};
};

// -- Linking Parameters ----------------------------------------------------

/// Get the linking mode from the config
LinkMode get_linking_mode(const std::string& name)
{
    LinkMode mode;

    if (name == "resources") {
        mode = LinkMode::resources;
    }
    else if (name == "p_synergistic") {
        mode = LinkMode::p_synergistic;
    }
    else if (name == "investment") {
        mode = LinkMode::investment;
    }
    else if (name == "payoff") {
        mode = LinkMode::payoff;
    }
    else if (name == "random") {
        mode = LinkMode::random;
    }
    else if (name == "strength") {
        mode = LinkMode::strength;
    }
    else if (name == "NONE") {
        mode = LinkMode::NONE;
    }
    else {
        throw std::invalid_argument(
            "The specified mode '" + name +
            "' is not a valid LinkMode! Valid options are: 'resources', "
            "'p_synergistic', 'investment', 'payoff', 'random', 'strength', "
            "and 'NONE'.");
    }

    return mode;
}

/// Parameters defining the link mode specific parameters
struct LinkModeSpecificParams
{
    /// The cost for the source agent to create a link to a neighbor
    const double cost_source;

    /// The cost for the target agent to create a link to a neighbor
    const double cost_target;

    /// Whether linking is enabled
    /** If nothing is given in the configuration file, set enabled=true .
     */
    const bool enabled;

    /// The config-constructor
    LinkModeSpecificParams(const DataIO::Config& cfg) :
        cost_source(get_as<double>("cost_source", cfg)),
        cost_target(get_as<double>("cost_target", cfg)),
        enabled(get_as<bool>("enabled", cfg, true))
    {}

    /// The default constructor
    LinkModeSpecificParams() : cost_source {}, cost_target {}, enabled {true} {}
};

/// Parameters defining the link mode parameters
struct LinkModeParams
{
    /// The probability to mutate the link mode under procreation
    const double p_mode_mutation;

    /// The resources link mode specific parameters
    const LinkModeSpecificParams resources;

    /// The p_synergistic link mode specific parameters
    const LinkModeSpecificParams p_synergistic;

    /// The investment link mode specific parameters
    const LinkModeSpecificParams investment;

    /// The payoff link mode specific parameters
    const LinkModeSpecificParams payoff;

    /// The random link mode specific parameters
    const LinkModeSpecificParams random;

    /// The strength link mode specific parameters
    const LinkModeSpecificParams strength;

    /// The random link mode specific parameters
    const LinkModeSpecificParams goods;

    /// The config-constructor
    LinkModeParams(const DataIO::Config& cfg) :
        p_mode_mutation(get_as<double>("p_mode_mutation", cfg)),
        resources(get_as<DataIO::Config>("resources", cfg)),
        p_synergistic(get_as<DataIO::Config>("p_synergistic", cfg)),
        investment(get_as<DataIO::Config>("investment", cfg)),
        payoff(get_as<DataIO::Config>("payoff", cfg)),
        random(get_as<DataIO::Config>("random", cfg)),
        strength(get_as<DataIO::Config>("strength", cfg)),
        goods(get_as<DataIO::Config>("goods", cfg))
    {}

    /// The default constructor
    LinkModeParams() :
        p_mode_mutation {}, resources {}, p_synergistic {},
        investment {}, payoff {}, random {}, strength {}, goods {}
    {}
};

/// Parameters defining the link addition
struct LinkAdditionParams
{
    /// The upper limit of desired links
    const unsigned upper_limit;

    /// Parameters defining link addition within the next-local
    const LinkModeParams local;

    /// Parameters defining link addition within the whole global
    const LinkModeParams global;

    /// The config-constructor
    LinkAdditionParams(const DataIO::Config& cfg) :
        upper_limit(get_as<unsigned>("upper_limit", cfg)),
        local(get_as<DataIO::Config>("local", cfg)),
        global(get_as<DataIO::Config>("global", cfg))
    {}
};

/// Link removal Parameter
struct LinkRemovalParams : LinkModeParams
{
    /// The upper limit of maximal links
    const unsigned upper_limit;

    /// The config-constructor
    LinkRemovalParams(const DataIO::Config& cfg) :
        LinkModeParams(cfg), upper_limit(get_as<unsigned>("upper_limit", cfg))
    {}
};

/// Parameters defining linking
struct LinkParams
{
    /// Link every <value> times
    const std::size_t link_every;

    /// Parameters defining link addition
    const LinkAdditionParams add;

    /// Parameters defining link removal
    const LinkRemovalParams remove;

    /// The config-constructor
    LinkParams(const DataIO::Config& cfg) :
        link_every(get_as<std::size_t>("link_every", cfg)),
        add(get_as<DataIO::Config>("add", cfg)),
        remove(get_as<DataIO::Config>("remove", cfg))
    {}
};

}  // namespace Utopia::Models::ReCooDy

#endif  // UTOPIA_MODELS_RECOODY_PARAMS_HH
