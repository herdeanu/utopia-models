#ifndef UTOPIA_MODELS_RECOODY_ADAPTORS_HH
#define UTOPIA_MODELS_RECOODY_ADAPTORS_HH

#include <boost/graph/adjacency_list.hpp>
#include <limits>
#include "types.hh"

namespace Utopia::Models::ReCooDy
{
/// Retrieve the id
auto id = [](const auto v, auto& g) {
    return g[v].id();
};

// -- Get agent state variables -----------------------------------------------
/// Retrieve the agent_number of an agent
auto agent_number = [](const auto v, const auto& g) {
    return g[v].state.agent_number;
};

/// Retrieve the classification of an agent
auto classification = [](const auto v, const auto& g) {
    return g[v].state.classification;
};

/// Retrieve the interaction probability of an agent
auto p_synergistic = [](const auto v, const auto& g) {
    return g[v].state.p_synergistic;
};

/// Retrieve the investment of an agent
auto investment = [](const auto v, const auto& g) {
    return g[v].state.investment;
};

/// Retrieve the expected investment of an agent
auto expected_investment = [](const auto v, const auto& g) {
    return (investment(v, g) * p_synergistic(v, g));
};

/// Retrieve the actual_investment of an agent
auto actual_investment = [](const auto v, const auto& g) {
    return g[v].state.actual_investment;
};

/// Retrieve the cost of an agent
auto cost = [](const auto v, const auto& g) {
    if (std::signbit(actual_investment(v, g))) {
        return 0.;
    }
    else {
        return actual_investment(v, g);
    }
};

/// Retrieve the grabbing of an agent
auto grabbing = [](const auto v, const auto& g) {
    if (not std::signbit(actual_investment(v, g))) {
        return 0.;
    }
    else {
        return -actual_investment(v, g);
    }
};

/// Retrieve the strength of an agent
auto strength = [](const auto v, const auto& g) {
    return g[v].state.strength;
};

/// Retrieve the p_add_local_links
auto p_add_local_links = [](const auto v, const auto& g) {
    return g[v].state.p_add_local_links;
};

/// Retrieve the link_mode_add_local
auto link_mode_add_local = [](const auto v, const auto& g) {
    return g[v].state.link_mode_add_local;
};

/// Retrieve the link_mode_add_global
auto link_mode_add_global = [](const auto v, const auto& g) {
    return g[v].state.link_mode_add_global;
};

/// Retrieve the link_mode_remove
auto link_mode_remove = [](const auto v, const auto& g) {
    return g[v].state.link_mode_remove;
};

/// Retrieve the number of desired links of an agent
auto add_link_threshold = [](const auto v, const auto& g) {
    return g[v].state.add_link_threshold;
};

/// Retrieve the number of max links of an agent
auto remove_link_threshold = [](const auto v, const auto& g) {
    return g[v].state.remove_link_threshold;
};

/// Retrieve the goods of an agent
auto goods = [](const auto v, const auto& g) {
    return g[v].state.goods;
};

/// Retrieve the extracted_synergistic_resources_per_agent of an agent
auto extracted_synergistic_resources_per_agent = [](const auto v,
                                                    const auto& g) {
    return g[v].state.extracted_synergistic_resources_per_agent;
};

/// Retrieve the cooperativity of an agent
auto cooperativity = [](const auto v, const auto& g) {
    return g[v].state.cooperativity;
};

/// Retrieve the payoff of an agent
auto payoff = [](const auto v, const auto& g) {
    return g[v].state.payoff;
};

/// Retrieve the basic_resource of an agent
auto basic_resource = [](const auto v, const auto& g) {
    return g[v].state.basic_resource;
};

/// Retrieve the resources of an agent
auto resources = [](const auto v, const auto& g) {
    return g[v].state.resources;
};

/// Retrieve the age of an agent
auto age = [](const auto v, const auto& g) {
    return g[v].state.age;
};

/// Retrieve the information what extraction mode the agent has
auto extraction_mode = [](const auto v, const auto& g) {
    return g[v].state.extraction_mode;
};

/// Retrieve the old_investment of an agent
auto old_investment = [](const auto v, const auto& g) {
    return g[v].state.old_investment;
};

/// Retrieve the goods_per_agent of the subinteraction centered around v
auto goods_per_agent = [](const auto v, const auto& g) {
    return g[v].state.goods_per_agent;
};

/// Retrieve the spendings for living of an agent
auto spending_living = [](const auto v, const auto& g) {
    return g[v].state.spending_living;
};

/// Retrieve the spendings for strength of an agent
auto spending_strength = [](const auto v, const auto& g) {
    return g[v].state.spending_strength;
};

/// Retrieve the spendings for actual investment of an agent
auto spending_cost = [](const auto v, const auto& g) {
    return g[v].state.spending_cost;
};

/// Retrieve the spendings for link_addition of an agent
auto spending_link_addition = [](const auto v, const auto& g) {
    return g[v].state.spending_link_addition;
};

/// Retrieve the spendings for link removal of an agent
auto spending_link_removal = [](const auto v, const auto& g) {
    return g[v].state.spending_link_removal;
};

/// Retrieve the spendings for birth of an agent
auto spending_birth = [](const auto v, const auto& g) {
    return g[v].state.spending_birth;
};

/// Calculate the number of active links
auto calc_num_active_links = [](const auto v, const auto& g) {
    if (extraction_mode(v, g) == ExtractionMode::basic) {
        return unsigned {0};
    }
    else {
        unsigned a_links = 0;
        for (const auto nb : range<IterateOver::neighbors>(v, g)) {
            if (extraction_mode(nb, g) == ExtractionMode::synergistic) {
                ++a_links;
            }
        }
        return a_links;
    }
};

// -- Get Link States --------------------------------------------------------
/// Retrieve the age of a link
auto link_age = [](const auto e, const auto& g) {
    return g[e].state.age;
};

}  // namespace Utopia::Models::ReCooDy

#endif  // UTOPIA_MODELS_RECOODY_ADAPTORS_HH