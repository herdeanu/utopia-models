#define BOOST_TEST_MODULE ReCooDy statistics test

#include <functional>
#include <boost/test/unit_test.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/test/test_tools.hpp>

#include <utopia/core/graph/creation.hh>
#include "../statistics.hh"

namespace Utopia::Models::ReCooDy::Test
{
// -- Type definitions --------------------------------------------------------

/// A test node
struct Node
{
    /// A test parameter
    double param1;
    double param2;
};

/// A test edge
struct Edge
{
    double e_param1;
};

/// The edge container type
using EdgeContType = boost::listS;

/// Tye vertex container type
using VertexContType = boost::vecS;

/// Test Graph
using Graph = boost::adjacency_list<EdgeContType,
                                    VertexContType,
                                    boost::undirectedS,
                                    Node,
                                    Edge>;

// -- Fixtures ----------------------------------------------------------------

// Test random Graph
struct TestGraph
{
    Graph g;

    // Create a regular test graph with
    TestGraph()
    {
        constexpr std::size_t num_vertices = 5;
        constexpr std::size_t mean_degree  = 2;
        constexpr bool oriented = false;
        DefaultRNG rng {};

        g = Utopia::Graph::create_WattsStrogatz_graph<Graph>(num_vertices,
                                                             mean_degree,
                                                             0.,
                                                             oriented,
                                                             rng);

        // Setting the counter to 1 and set properties incrementally.
        unsigned counter = 1;
        for (auto v : range<IterateOver::vertices>(g)) {
            g[v].param1 = static_cast<double>(counter);
            ++counter;
            g[v].param2 = 0.;
        }

        counter = 1;
        for (auto e : range<IterateOver::edges>(g)) {
            g[e].e_param1 = static_cast<double>(counter);
            ++counter;
        }
    }
};

// Test random Graph
struct TestGraphNegativeParam
{
    Graph g;

    // Create a regular test graph with
    TestGraphNegativeParam()
    {
        constexpr std::size_t num_vertices = 5;

        g = Graph{num_vertices};

        // Setting the counter and set properties incrementally.
        int counter = -2;
        for (auto v : range<IterateOver::vertices>(g)) {
            g[v].param1 = static_cast<double>(counter);
            ++counter;
        }
    }
};

// -- Actual test -------------------------------------------------------------

// Test that the statistics operations yield the correct result
BOOST_FIXTURE_TEST_CASE(statistics, TestGraph)
{
    namespace tt = boost::test_tools;
    {
        // Vertex param1: mean and std
        double mean_expected = 3.;
        double std_expected  = 1.5811388300841898;

        const auto mean = Statistics::mean<IterateOver::vertices>(
            [](auto v, auto& g) {
                return g[v].param1;
            },
            g);

        const auto std = Statistics::std<IterateOver::vertices>(
            [](auto v, auto& g) {
                return g[v].param1;
            },
            g);

        const auto mean_std = Statistics::mean_std<IterateOver::vertices>(
            [](auto v, auto& g) {
                return g[v].param1;
            },
            g);

        BOOST_TEST(mean == mean_expected, tt::tolerance(1e-10));
        BOOST_TEST(std == std_expected, tt::tolerance(1e-10));
        BOOST_TEST(mean_std.first == mean_expected, tt::tolerance(1e-10));
        BOOST_TEST(mean_std.second == std_expected, tt::tolerance(1e-10));
    }  // namespace tt=boost::test_tools;

    {
        // Vertex param2: mean and std
        double mean_expected = 0.;
        double std_expected  = 0.;

        const auto mean = Statistics::mean<IterateOver::vertices>(
            [](auto v, auto& g) {
                return g[v].param2;
            },
            g);

        const auto std = Statistics::std<IterateOver::vertices>(
            [](auto v, auto& g) {
                return g[v].param2;
            },
            g);

        const auto mean_std = Statistics::mean_std<IterateOver::vertices>(
            [](auto v, auto& g) {
                return g[v].param2;
            },
            g);

        BOOST_TEST(mean == mean_expected, tt::tolerance(1e-10));
        BOOST_TEST(std == std_expected, tt::tolerance(1e-10));
        BOOST_TEST(mean_std.first == mean_expected, tt::tolerance(1e-10));
        BOOST_TEST(mean_std.second == std_expected, tt::tolerance(1e-10));
    }

    {
        // Edge e_param1: mean and std
        double mean_expected = 3.;
        double std_expected  = 1.5811388300841898;

        const auto mean = Statistics::mean<IterateOver::edges>(
            [](auto e, auto& g) {
                return g[e].e_param1;
            },
            g);

        const auto std = Statistics::std<IterateOver::edges>(
            [](auto e, auto& g) {
                return g[e].e_param1;
            },
            g);

        const auto mean_std = Statistics::mean_std<IterateOver::edges>(
            [](auto e, auto& g) {
                return g[e].e_param1;
            },
            g);

        BOOST_TEST(mean == mean_expected, tt::tolerance(1e-10));
        BOOST_TEST(std == std_expected, tt::tolerance(1e-10));
        BOOST_TEST(mean_std.first == mean_expected, tt::tolerance(1e-10));
        BOOST_TEST(mean_std.second == std_expected, tt::tolerance(1e-10));
    }

    // Test all possible iterator types (just their corret call)
    {
        {
            // in_edges
            [[maybe_unused]] const auto mean =
                Statistics::mean<IterateOver::in_edges>(
                    [](auto e, auto& g) {
                        return g[e].e_param1;
                    },
                    boost::vertex(0, g),
                    g);

            [[maybe_unused]] const auto std =
                Statistics::std<IterateOver::in_edges>(
                    [](auto e, auto& g) {
                        return g[e].e_param1;
                    },
                    boost::vertex(0, g),
                    g);

            [[maybe_unused]] const auto mean_std =
                Statistics::mean_std<IterateOver::in_edges>(
                    [](auto e, auto& g) {
                        return g[e].e_param1;
                    },
                    boost::vertex(0, g),
                    g);
        }

        {
            // out_edges
            [[maybe_unused]] const auto mean =
                Statistics::mean<IterateOver::out_edges>(
                    [](auto e, auto& g) {
                        return g[e].e_param1;
                    },
                    boost::vertex(0, g),
                    g);

            [[maybe_unused]] const auto std =
                Statistics::std<IterateOver::out_edges>(
                    [](auto e, auto& g) {
                        return g[e].e_param1;
                    },
                    boost::vertex(0, g),
                    g);

            [[maybe_unused]] const auto mean_std =
                Statistics::mean_std<IterateOver::out_edges>(
                    [](auto e, auto& g) {
                        return g[e].e_param1;
                    },
                    boost::vertex(0, g),
                    g);
        }

        {
            // neighbors
            [[maybe_unused]] const auto mean =
                Statistics::mean<IterateOver::neighbors>(
                    [](auto v, auto& g) {
                        return g[v].param1;
                    },
                    boost::vertex(0, g),
                    g);

            [[maybe_unused]] const auto std =
                Statistics::std<IterateOver::neighbors>(
                    [](auto v, auto& g) {
                        return g[v].param1;
                    },
                    boost::vertex(0, g),
                    g);

            [[maybe_unused]] const auto mean_std =
                Statistics::mean_std<IterateOver::neighbors>(
                    [](auto v, auto& g) {
                        return g[v].param1;
                    },
                    boost::vertex(0, g),
                    g);
        }

        {
            // inv_neighbors
            [[maybe_unused]] const auto mean =
                Statistics::mean<IterateOver::inv_neighbors>(
                    [](auto v, auto& g) {
                        return g[v].param1;
                    },
                    boost::vertex(0, g),
                    g);

            [[maybe_unused]] const auto std =
                Statistics::std<IterateOver::inv_neighbors>(
                    [](auto v, auto& g) {
                        return g[v].param1;
                    },
                    boost::vertex(0, g),
                    g);

            [[maybe_unused]] const auto mean_std =
                Statistics::mean_std<IterateOver::inv_neighbors>(
                    [](auto v, auto& g) {
                        return g[v].param1;
                    },
                    boost::vertex(0, g),
                    g);
        }
    }
}

// Test the mean_abs operation
BOOST_FIXTURE_TEST_CASE(mean_abs, TestGraphNegativeParam){
    namespace tt = boost::test_tools;
    {
        // Vertex param1: mean and std
        const double mean_expected = (-2. + -1. + 0. + 1. + 2.) / 5.;
        const double mean_abs_expected = (2. + 1. + 0. + 1. + 2.) / 5.;

        const auto mean = Statistics::mean<IterateOver::vertices>(
            [](auto v, auto& g) {
                return g[v].param1;
            },
            g);

        const auto mean_abs = Statistics::mean_abs<IterateOver::vertices>(
            [](auto v, auto& g) {
                return g[v].param1;
            },
            g);

        BOOST_TEST(mean == mean_expected, tt::tolerance(1e-10));
        BOOST_TEST(mean_abs == mean_abs_expected, tt::tolerance(1e-10));
    }  // namespace tt=boost::test_tools;

}

}  // namespace Utopia::Models::ReCooDy::Test