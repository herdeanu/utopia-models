#define BOOST_TEST_MODULE ReCooDy adaptor test

#include <boost/test/unit_test.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/test/test_tools.hpp>

#include "../ReCooDy.hh"
#include "../adaptors.hh"
#include "../types.hh"

namespace Utopia::Models::ReCooDy
{
BOOST_AUTO_TEST_CASE(adaptors)
{
    namespace tt = boost::test_tools;
    // Create a Graph with a single vertex and set some arbitrary parameters
    Utopia::Models::ReCooDy::Graph g(1);

    const auto v = boost::vertex(0, g);

    g[v].state.agent_number          = 86;
    g[v].state.p_synergistic         = .67;
    g[v].state.investment            = 42.;
    g[v].state.actual_investment     = 349587.;
    g[v].state.strength              = 2.;
    g[v].state.p_add_local_links     = 0.5;
    g[v].state.link_mode_add_local   = LinkMode::resources;
    g[v].state.link_mode_add_global  = LinkMode::payoff;
    g[v].state.link_mode_remove      = LinkMode::investment;
    g[v].state.add_link_threshold    = 234;
    g[v].state.remove_link_threshold = 23456;

    g[v].state.goods                                     = 1000.;
    g[v].state.extracted_synergistic_resources_per_agent = 100.;
    g[v].state.payoff                                    = 0.;
    g[v].state.basic_resource                            = 1234567.;
    g[v].state.resources                                 = 137.;
    g[v].state.age                                       = 42;
    g[v].state.extraction_mode = ExtractionMode::basic;
    g[v].state.old_investment  = 42234.;
    g[v].state.goods_per_agent = 17.;

    g[v].state.spending_living        = 1.1;
    g[v].state.spending_strength      = 1.2;
    g[v].state.spending_cost          = 1.3;
    g[v].state.spending_link_addition = 1.4;
    g[v].state.spending_link_removal  = 1.5;
    g[v].state.spending_birth         = 1.6;

    BOOST_TEST(id(v, g) == g[v].id());

    // Check that the adaptors return the correct values
    BOOST_TEST(agent_number(v, g) == g[v].state.agent_number);
    BOOST_TEST(p_synergistic(v, g) == g[v].state.p_synergistic,
               tt::tolerance(1e-10));
    BOOST_TEST(investment(v, g) == g[v].state.investment, tt::tolerance(1e-10));
    BOOST_TEST(actual_investment(v, g) == g[v].state.actual_investment,
               tt::tolerance(1e-10));
    BOOST_TEST(strength(v, g) == g[v].state.strength, tt::tolerance(1e-10));
    BOOST_TEST(p_add_local_links(v, g) == g[v].state.p_add_local_links,
               tt::tolerance(1e-10));
    // enum classes cannot be directly compared, therefore the static_cast is
    // required.
    BOOST_TEST(static_cast<std::size_t>(link_mode_add_local(v, g)) ==
               static_cast<std::size_t>(g[v].state.link_mode_add_local));
    BOOST_TEST(static_cast<std::size_t>(link_mode_add_global(v, g)) ==
               static_cast<std::size_t>(g[v].state.link_mode_add_global));
    BOOST_TEST(static_cast<std::size_t>(link_mode_remove(v, g)) ==
               static_cast<std::size_t>(g[v].state.link_mode_remove));
    BOOST_TEST(add_link_threshold(v, g) == g[v].state.add_link_threshold);
    BOOST_TEST(remove_link_threshold(v, g) == g[v].state.remove_link_threshold);
    BOOST_TEST(goods(v, g) == g[v].state.goods, tt::tolerance(1e-10));
    BOOST_TEST(extracted_synergistic_resources_per_agent(v, g) ==
                   g[v].state.extracted_synergistic_resources_per_agent,
               tt::tolerance(1e-10));
    BOOST_TEST(payoff(v, g) == g[v].state.payoff, tt::tolerance(1e-10));
    BOOST_TEST(basic_resource(v, g) == g[v].state.basic_resource,
               tt::tolerance(1e-10));
    BOOST_TEST(resources(v, g) == g[v].state.resources, tt::tolerance(1e-10));
    BOOST_TEST(age(v, g) == g[v].state.age);
    BOOST_TEST(static_cast<std::size_t>(extraction_mode(v, g)) ==
               static_cast<std::size_t>(g[v].state.extraction_mode));
    BOOST_TEST(old_investment(v, g) == g[v].state.old_investment,
               tt::tolerance(1e-10));
    BOOST_TEST(goods_per_agent(v, g) == g[v].state.goods_per_agent,
               tt::tolerance(1e-10));

    BOOST_TEST(spending_living(v, g) == g[v].state.spending_living,
               tt::tolerance(1e-10));
    BOOST_TEST(spending_strength(v, g) == g[v].state.spending_strength,
               tt::tolerance(1e-10));
    BOOST_TEST(spending_cost(v, g) == g[v].state.spending_cost,
               tt::tolerance(1e-10));
    BOOST_TEST(spending_link_addition(v, g) ==
                   g[v].state.spending_link_addition,
               tt::tolerance(1e-10));
    BOOST_TEST(spending_link_removal(v, g) == g[v].state.spending_link_removal,
               tt::tolerance(1e-10));
    BOOST_TEST(spending_birth(v, g) == g[v].state.spending_birth,
               tt::tolerance(1e-10));
}

}  // namespace Utopia::Models::ReCooDy