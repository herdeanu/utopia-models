#define BOOST_TEST_MODULE ReCooDy linking test

#include <functional>
#include <boost/test/unit_test.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/test/test_tools.hpp>

#include <utopia/core/graph/creation.hh>
#include "../linking.hh"
#include "../types.hh"
#include "../params.hh"
#include "../adaptors.hh"

using namespace Utopia;
using namespace Utopia::Models::ReCooDy;

// -- Fixtures ----------------------------------------------------------------

// Create a regular test graph to test the linking mechanisms
struct TestGraph
{
    using GraphType = Utopia::Models::ReCooDy::Graph;

    GraphType g;

    LinkModeParams link_mode_params;

    DefaultRNG rng;

    // Create a regular test graph with
    TestGraph() : link_mode_params(), rng {}
    {
        constexpr std::size_t num_vertices = 5;
        constexpr std::size_t mean_degree  = 2;
        constexpr bool oriented = false;

        g = Utopia::Graph::create_WattsStrogatz_graph<GraphType>(num_vertices,
                                                                 mean_degree,
                                                                 0.,
                                                                 oriented,
                                                                 rng);

        // Setting the counter to 1 and set properties
        // incrementally.
        unsigned counter = 1;
        // resources
        for (auto v : range<IterateOver::vertices>(g)) {
            g[v].state.resources = static_cast<double>(counter);
            ++counter;
        }
    }
};

// -- Actual test -------------------------------------------------------------

/// Test the add_link function
BOOST_FIXTURE_TEST_CASE(add_link_test, TestGraph)
{
    namespace tt = boost::test_tools;

    // -- Add a new link ------------------------------------------------------
    const auto source      = boost::vertex(0, g);
    const auto target      = boost::vertex(3, g);
    const auto source_cost = 0.2;
    const auto target_cost = 0.3;
    auto source_resources     = resources(source, g);
    auto target_resources     = resources(target, g);
    auto num_edges         = boost::num_edges(g);

    add_link(source, target, source_cost, target_cost, g);

    // Check that the link was created and link formation envoked an resources cost
    BOOST_TEST(boost::num_edges(g) == num_edges + 1);
    BOOST_TEST(resources(source, g) == source_resources - source_cost,
               tt::tolerance(1e-10));
    BOOST_TEST(resources(target, g) == target_resources - target_cost,
               tt::tolerance(1e-10));

    // -- There now already exists a link -------------------------------------
    source_resources = resources(source, g);
    target_resources = resources(target, g);
    num_edges     = boost::num_edges(g);
    add_link(source, target, source_cost, target_cost, g);

    // No link is added and the resources is constant, too.
    BOOST_TEST(boost::num_edges(g) == num_edges);
    BOOST_TEST(resources(source, g) == source_resources, tt::tolerance(1e-10));
    BOOST_TEST(resources(target, g) == target_resources, tt::tolerance(1e-10));
}

// Test the remove_link function
BOOST_FIXTURE_TEST_CASE(remove_link_test, TestGraph)
{
    namespace tt = boost::test_tools;

    // -- Add a new link ------------------------------------------------------
    const auto source      = boost::vertex(0, g);
    const auto target      = boost::vertex(1, g);
    const auto source_cost = 0.2;
    const auto target_cost = 0.3;

    auto source_resources = resources(source, g);
    auto target_resources = resources(target, g);
    auto num_edges     = boost::num_edges(g);

    remove_link(source, target, source_cost, target_cost, g);

    // Check that the link was removed and link removal envoked an resources cost
    BOOST_TEST(boost::num_edges(g) == num_edges - 1);
    BOOST_TEST(resources(source, g) == source_resources - source_cost,
               tt::tolerance(1e-10));
    BOOST_TEST(resources(target, g) == target_resources - target_cost,
               tt::tolerance(1e-10));

    // -- The link is already removed -----------------------------------------
    // no cost is subtracted
    source_resources = resources(source, g);
    target_resources = resources(target, g);
    num_edges     = boost::num_edges(g);
    remove_link(source, target, source_cost, target_cost, g);

    // No link is added and the resources is constant, too.
    BOOST_TEST(boost::num_edges(g) == num_edges);
    BOOST_TEST(resources(source, g) == source_resources, tt::tolerance(1e-10));
    BOOST_TEST(resources(target, g) == target_resources, tt::tolerance(1e-10));
}

/// Test the nb_to_remove function
BOOST_FIXTURE_TEST_CASE(nb_to_remove_test, TestGraph)
{
    // Note that the energies are set such their value corresponds to the vertex
    // id
    {
        // Determine the neighbor to be removed by the resources.
        // With incrementing energies, vertex 1 is the neighbor with the lowest
        // resources.
        auto to_remove = nb_to_remove(boost::vertex(0, g), g, resources);
        BOOST_TEST(to_remove == boost::vertex(1, g));
    }
    {
        // Vertex 2 is the next-neighbor of vertex 3 with the lowest resources
        auto to_remove = nb_to_remove(boost::vertex(3, g), g, resources);
        BOOST_TEST(to_remove == boost::vertex(2, g));
    }
}

// Test the nb_to_add function
BOOST_FIXTURE_TEST_CASE(nb_to_add_test, TestGraph)
{
    {
        // Determine the next-neighbor to be added by resources predicate.
        // With incrementing energies, vertex num_vertices-2 is the neighbor
        // with the highest resources.
        const auto to_add = nb_to_add(boost::vertex(0, g), g, resources);
        BOOST_TEST(to_add == boost::vertex(boost::num_vertices(g) - 2, g));
    }
    {
        // Vertex 1 is the next-neighbor of vertex 3 with the lowest resources
        const auto to_add = nb_to_add(boost::vertex(3, g), g, resources);
        BOOST_TEST(to_add == boost::vertex(1, g));
    }
}

// Test the agent_from_population_to_add_link function
BOOST_FIXTURE_TEST_CASE(agent_from_population_to_add_link_test, TestGraph)
{
    {
        // Determine the next-neighbor to be added by resources predicate.
        // With incrementing energies, vertex num_vertices-2 is the neighbor
        // with the highest resources.
        auto to_add =
            agent_from_population_to_add_link(boost::vertex(0, g), g, resources);
        BOOST_TEST(to_add == boost::vertex(boost::num_vertices(g) - 1, g));
    }
}

/// Test that the change_link function yields correct results.
BOOST_FIXTURE_TEST_CASE(change_link_test, TestGraph)
{
    // NOTE that this test just tests the functionality on a very rudemantary
    // level.
    const auto link_mode                = LinkMode::resources;
    const auto v                          = boost::vertex(2, g);
    const bool sample_within_neighborhood = false;

    auto num_edges = boost::num_edges(g);
    change_link(link_mode,
                      link_mode_params,
                      v,
                      sample_within_neighborhood,
                      add_link,
                      nb_to_add,
                      g,
                      rng);

    // Check that an edge has been added to the graph
    BOOST_TEST(boost::num_edges(g) == num_edges + 1);

    // Now remove an edge through the remove function and check that there
    // is one fewer edge
    num_edges = boost::num_edges(g);
    change_link(LinkMode::investment,
                      link_mode_params,
                      v,
                      sample_within_neighborhood,
                      remove_link,
                      nb_to_remove,
                      g,
                      rng);
    BOOST_TEST(boost::num_edges(g) == num_edges - 1);
}
