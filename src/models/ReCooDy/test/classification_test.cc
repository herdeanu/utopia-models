#define BOOST_TEST_MODULE ReCooDy classification test

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <utopia/core/graph/creation.hh>
#include "../types.hh"
#include "../classification.hh"
#include "../adaptors.hh"

using namespace Utopia;
using namespace Utopia::Models::ReCooDy;
namespace tt = boost::test_tools;

// -- Fixtures ----------------------------------------------------------------

// Create a regular test graph to test the linking mechanisms
struct TestGraph
{
    using GraphType = Utopia::Models::ReCooDy::Graph;

    GraphType g;
    DefaultRNG rng;

    // Create a test graph with 4 vertices
    TestGraph() : rng {}
    {
        constexpr std::size_t num_vertices = 4;

        g = GraphType(num_vertices);

        // Setting required agent properties
        g[boost::vertex(0, g)].state.investment = 1.1;
        g[boost::vertex(1, g)].state.investment = 0.1;
        g[boost::vertex(2, g)].state.investment = -1;
        g[boost::vertex(3, g)].state.investment = -2;

        for (auto v : range<IterateOver::vertices>(g)) {
            g[v].state.actual_investment = investment(v, g);
        }
    }
};

// -- Actual test -------------------------------------------------------------

/// Test the classification functions
BOOST_FIXTURE_TEST_CASE(classification_test, TestGraph)
{
    // -- Check that the number of interacting agents is correct --------------
    const auto v0 = boost::vertex(0, g);
    const auto v1 = boost::vertex(1, g);
    const auto v2 = boost::vertex(2, g);
    const auto v3 = boost::vertex(3, g);

    // Test BPEM classification
    classify(g);
    BOOST_TEST(static_cast<char>(classification(v0, g)) ==
               static_cast<char>(Classification::B));
    BOOST_TEST(static_cast<char>(classification(v1, g)) ==
               static_cast<char>(Classification::P));
    BOOST_TEST(static_cast<char>(classification(v2, g)) ==
               static_cast<char>(Classification::E));
    BOOST_TEST(static_cast<char>(classification(v3, g)) ==
               static_cast<char>(Classification::M));

    // Test cost and grabbing
    // Agents either pay a cost or grab resources
    for (auto v : range<IterateOver::vertices>(g)) {
        BOOST_TEST(((cost(v, g) == 0.) or (grabbing(v, g) == 0.)));
    }

    BOOST_TEST(cost(v0, g) == 1.1);
    BOOST_TEST(cost(v1, g) == 0.1);
    BOOST_TEST(grabbing(v2, g) == 1);
    BOOST_TEST(grabbing(v3, g) == 2);
}
