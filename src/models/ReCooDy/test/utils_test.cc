#define BOOST_TEST_MODULE ReCooDy statistics test

#include <boost/test/unit_test.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>

#include <utopia/core/graph.hh>
#include "../utils.hh"

namespace Utopia::Models::ReCooDy
{
// -- Type definitions --------------------------------------------------------

/// A test node
struct Node
{};

/// A test edge
struct Edge
{};

/// The edge container type
using EdgeContType = boost::listS;

/// Tye vertex container type
using VertexContType = boost::vecS;

/// Test network
using Network = boost::adjacency_list<EdgeContType,
                                      VertexContType,
                                      boost::undirectedS,
                                      Node,
                                      Edge>;

// -- Fixtures ----------------------------------------------------------------

// Test random network
struct TestNetwork
{
    Network nw;
    DefaultRNG rng;
    const std::size_t num_vertices;
    const std::size_t mean_degree;
    const bool oriented;

    using VertexDesc = boost::graph_traits<Network>::vertex_descriptor;
    using EdgeDesc   = boost::graph_traits<Network>::edge_descriptor;

    // Create a regular test graph with
    TestNetwork() : num_vertices(100), mean_degree(2), oriented(false)
    {
        nw = Graph::create_WattsStrogatz_graph<Network>(num_vertices,
                                                        mean_degree,
                                                        0.,
                                                        oriented,
                                                        rng);
    }
};

// -- Actual test -------------------------------------------------------------

// Test that the statistics operations yield the correct result
BOOST_FIXTURE_TEST_CASE(statistics, TestNetwork)
{
    // -- Vertices ------------------------------------------------------------
    {
        constexpr std::size_t n          = 10;
        constexpr std::size_t n_too_high = 1000;

        auto sample      = Utils::sample<IterateOver::vertices>(n, nw, rng);
        auto sample_zero = Utils::sample<IterateOver::vertices>(0, nw, rng);
        auto sample_all =
            Utils::sample<IterateOver::vertices>(n_too_high, nw, rng);

        BOOST_TEST(sample.size() == n);
        BOOST_TEST(sample_zero.size() == 0);
        BOOST_TEST(sample_all.size() == num_vertices);  // Sample all vertices

        // Check that the first three entries are not corresponding to the
        // first three vertices in the graph
        BOOST_TEST((sample[0] != boost::vertex(0, nw) &&
                    sample[1] != boost::vertex(1, nw) &&
                    sample[2] != boost::vertex(2, nw)));
    }

    // -- Edges ---------------------------------------------------------------
    {
        constexpr std::size_t n          = 10;
        constexpr std::size_t n_too_high = 1000;

        auto sample      = Utils::sample<IterateOver::edges>(n, nw, rng);
        auto sample_zero = Utils::sample<IterateOver::edges>(0, nw, rng);
        auto sample_all =
            Utils::sample<IterateOver::edges>(n_too_high, nw, rng);

        BOOST_TEST(sample.size() == n);
        BOOST_TEST(sample_zero.size() == 0);
        BOOST_TEST(sample_all.size() ==
                   num_vertices * mean_degree / 2);  // Sample all edges
        // The factor 2 is there because links are undirected, so need to be
        // counted twice.
    }

    // -- Neighbors -----------------------------------------------------------
    {
        constexpr std::size_t n          = 1;
        constexpr std::size_t n_too_high = 1000;
        const VertexDesc v_ref           = boost::vertex(0, nw);

        auto sample = Utils::sample<IterateOver::neighbors>(n, v_ref, nw, rng);
        auto sample_zero =
            Utils::sample<IterateOver::neighbors>(0, v_ref, nw, rng);
        auto sample_all =
            Utils::sample<IterateOver::neighbors>(n_too_high, v_ref, nw, rng);

        BOOST_TEST(sample.size() == n);
        BOOST_TEST(sample_zero.size() == 0);
        BOOST_TEST(sample_all.size() == 2);  // Sample all neighbors
    }
}

}  // namespace Utopia::Models::ReCooDy