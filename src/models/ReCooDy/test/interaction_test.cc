#define BOOST_TEST_MODULE ReCooDy interaction test

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <utopia/core/graph/creation.hh>
#include "../types.hh"
#include "../interaction.hh"
#include "../adaptors.hh"

using namespace Utopia;
using namespace Utopia::Models::ReCooDy;
namespace tt = boost::test_tools;

// -- Fixtures ----------------------------------------------------------------

// Create a regular test graph to test the linking mechanisms
struct TestGraph
{
    using GraphType = Utopia::Models::ReCooDy::Graph;

    GraphType g;

    DefaultRNG rng;

    // Create a regular test graph with
    TestGraph() : rng {}
    {
        constexpr std::size_t num_vertices = 6;
        constexpr std::size_t mean_degree  = 2;
        constexpr bool oriented = false;

        g = Utopia::Graph::create_WattsStrogatz_graph<GraphType>(num_vertices,
                                                                 mean_degree,
                                                                 0.,
                                                                 oriented,
                                                                 rng);

        // Setting required agent properties
        int counter = -1;
        for (auto v : range<IterateOver::vertices>(g)) {
            g[v].state.actual_investment = static_cast<double>(counter);
            g[v].state.investment        = static_cast<double>(counter);
            g[v].state.goods_per_agent   = static_cast<double>(counter);
            g[v].state.resources = static_cast<double>(num_vertices - counter);
            ++counter;
        }

        g[boost::vertex(0, g)].state.extraction_mode =
            ExtractionMode::synergistic;
        g[boost::vertex(1, g)].state.extraction_mode =
            ExtractionMode::synergistic;
        g[boost::vertex(2, g)].state.extraction_mode =
            ExtractionMode::synergistic;
        g[boost::vertex(3, g)].state.extraction_mode = ExtractionMode::basic;
        g[boost::vertex(4, g)].state.extraction_mode = ExtractionMode::basic;
    }
};

// -- Actual test -------------------------------------------------------------

/// Test the calculate_actual_investment functions
BOOST_FIXTURE_TEST_CASE(calculate_actual_investment_test, TestGraph)
{
    // -- Check that the number of interacting agents is correct --------------
    const auto v0 = boost::vertex(0, g);
    const auto v1 = boost::vertex(1, g);
    const auto v2 = boost::vertex(2, g);
    const auto v3 = boost::vertex(3, g);
    const auto v4 = boost::vertex(4, g);
    const auto v5 = boost::vertex(5, g);

    std::vector<std::pair<std::size_t, std::size_t>> actual_investment {
        // resources=7 actual_interaction=-1
        {-1, calculate_actual_investment(v0, g)},
        // resources=6 actual_interaction=0
        {0, calculate_actual_investment(v1, g)},
        // resources=5 actual_interaction=1
        {1, calculate_actual_investment(v2, g)},
        // resources=4 actual_interaction=2
        {2, calculate_actual_investment(v3, g)},
        // resources=3 actual_interaction=3
        {3, calculate_actual_investment(v4, g)},
        // resources=2 actual_interaction=4
        {2, calculate_actual_investment(v5, g)},
    };

    for (const auto& [expected, calculated] : actual_investment) {
        BOOST_TEST(expected == calculated, tt::tolerance(1e-10));
    }
}

/// Test the calculate_goods_per_agent functions
BOOST_FIXTURE_TEST_CASE(num_interacting_agents_test, TestGraph)
{
    // -- Check that the number of interacting agents is correct --------------
    const auto v0 = boost::vertex(0, g);
    const auto v1 = boost::vertex(1, g);
    const auto v2 = boost::vertex(2, g);
    const auto v3 = boost::vertex(3, g);
    const auto v4 = boost::vertex(4, g);
    const auto v5 = boost::vertex(5, g);

    std::vector<std::pair<std::size_t, std::size_t>> num_ia_agents {
        {2, num_interacting_agents(v0, g)},
        {3, num_interacting_agents(v1, g)},
        {2, num_interacting_agents(v2, g)},
        {1, num_interacting_agents(v3, g)},
        {0, num_interacting_agents(v4, g)},
        {1, num_interacting_agents(v5, g)},
    };

    for (const auto& [expected, calculated] : num_ia_agents) {
        BOOST_TEST(expected == calculated, tt::tolerance(1e-10));
    }
}

/// Test the calculate_goods_per_agent functions
BOOST_FIXTURE_TEST_CASE(calculate_goods_and_resources_per_agent_test, TestGraph)
{
    // -- Check that the correct goods per agent is calculated --------------
    const auto r                   = 2.;
    const auto v0                  = boost::vertex(0, g);
    const auto v1                  = boost::vertex(1, g);
    const auto v2                  = boost::vertex(2, g);
    [[maybe_unused]] const auto v3 = boost::vertex(3, g);

    {
        // Sum up the investments. Note that in the regular graph with degree 2,
        // each agent has 2 neighbors, thus invests in 3 interactions.
        // Therefore, each investment has to be divided by 3.
        const auto sum_investments =
            investment(v1, g) / 2. + investment(v2, g) / 2.;
        const auto abs_sum_investments = std::fabs(investment(v1, g)) / 2. +
                                         std::fabs(investment(v2, g)) / 2.;

        const auto syn_per_a       = sum_investments * r / 2.;
        const auto resources_per_a = abs_sum_investments * r / 2.;

        auto [syn, e] = calculate_goods_and_resources_per_agent(r, v2, g);

        BOOST_TEST(syn_per_a == syn, tt::tolerance(1e-10));
        BOOST_TEST(resources_per_a == e, tt::tolerance(1e-10));
    }

    {
        // Sum up the investments. Note that in the regular graph with degree 2,
        // each agent has 2 neighbors, thus invests in 3 interactions.
        // Therefore, the each investment has to be divided by 3.
        const auto sum_investments = actual_investment(v0, g) / 2. +  // 2 nbs
                                     actual_investment(v1, g) / 3. +  // 3 nbs
                                     actual_investment(v2, g) / 2.;   // 2 nbs
        const auto abs_sum_investments =
            std::abs(actual_investment(v0, g)) / 2. +
            std::abs(actual_investment(v1, g)) / 3. +
            std::abs(actual_investment(v2, g)) / 2.;

        const auto syn_per_a                = sum_investments * r / 3.;
        const auto resources_per_a          = abs_sum_investments * r / 3.;
        const double destructed_resources_a = [v0, v1, v2, this]() {
            double destructed {0.};
            if (actual_investment(v0, this->g) < 0.) {
                destructed += std::abs(actual_investment(v0, this->g)) / 2.;
            }
            if (actual_investment(v1, this->g) < 0.) {
                destructed += std::abs(actual_investment(v1, this->g)) / 3.;
            }
            if (actual_investment(v2, this->g) < 0.) {
                destructed += std::abs(actual_investment(v2, this->g)) / 2.;
            }
            return destructed;
        }();

        auto [syn, e] = calculate_goods_and_resources_per_agent(r, v1, g);

        BOOST_TEST(syn_per_a == syn, tt::tolerance(1e-10));
        BOOST_TEST((resources_per_a + destructed_resources_a) == e,
                   tt::tolerance(1e-10));
    }
}

/// Test the collect_goods functions
BOOST_FIXTURE_TEST_CASE(collect_goods_test, TestGraph)
{
    const auto v0                  = boost::vertex(0, g);
    const auto v1                  = boost::vertex(1, g);
    const auto v2                  = boost::vertex(2, g);
    [[maybe_unused]] const auto v3 = boost::vertex(3, g);

    {
        const auto collected_synergies_exp = goods_per_agent(v0, g) +
                                             goods_per_agent(v1, g) +
                                             goods_per_agent(v2, g);

        collect_goods(v1, g);

        BOOST_TEST(collected_synergies_exp == goods(v1, g),
                   tt::tolerance(1e-10));
    }

    {
        const auto collected_synergies_exp =
            goods_per_agent(v1, g) + goods_per_agent(v2, g);

        collect_goods(v2, g);

        BOOST_TEST(collected_synergies_exp == goods(v2, g),
                   tt::tolerance(1e-10));
    }
}
