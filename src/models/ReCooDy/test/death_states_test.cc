#define BOOST_TEST_MODULE ReCooDy death_states test

#include <boost/test/unit_test.hpp>
#include <boost/test/test_tools.hpp>

#include "../death_states.hh"
#include "../types.hh"

namespace Utopia::Models::ReCooDy
{
BOOST_AUTO_TEST_CASE(death_states)
{
    namespace tt = boost::test_tools;
    
    {
    AgentState state1, state2;

    std::size_t time = 10;

    Utopia::Models::ReCooDy::DeathStates ds{time}; 

    BOOST_TEST(ds.get().size() == 0);
    ds.add(std::move(state1));
    BOOST_TEST(ds.get().size() == 1);
    ds.add(std::move(state2));
    BOOST_TEST(ds.get().size() == 2);
    ds.to_be_cleared();
    BOOST_TEST(ds.get().size() == 2);
    ds.clear();
    BOOST_TEST(ds.get().size() == 0);
    }
    
    // Case: Has not started tracking
    {
    AgentState state1, state2;

    std::size_t time = 10;

    Utopia::Models::ReCooDy::DeathStates ds{time, 20}; 

    BOOST_TEST(ds.get().size() == 0);
    ds.add(std::move(state1));
    BOOST_TEST(ds.get().size() == 0);
    ds.add(std::move(state2));
    BOOST_TEST(ds.get().size() == 0);
    ds.to_be_cleared();
    BOOST_TEST(ds.get().size() == 0);
    ds.clear();
    BOOST_TEST(ds.get().size() == 0);
    }

    // Case: Track every second state
    {
    AgentState state1, state2, state3;

    std::size_t time = 0;

    Utopia::Models::ReCooDy::DeathStates ds{time, 0, 1}; 

    BOOST_TEST(ds.get().size() == 0);
    ds.add(std::move(state1));
    BOOST_TEST(ds.get().size() == 0);
    ds.add(std::move(state2));
    BOOST_TEST(ds.get().size() == 1);
    ds.add(std::move(state3));
    BOOST_TEST(ds.get().size() == 1);
    ds.to_be_cleared();
    BOOST_TEST(ds.get().size() == 1);
    ds.clear();
    BOOST_TEST(ds.get().size() == 0);
    }

}

}  // namespace Utopia::Models::ReCooDy