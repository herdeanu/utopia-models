#ifndef UTOPIA_MODELS_RECOODY_INTERACTION_HH
#define UTOPIA_MODELS_RECOODY_INTERACTION_HH

#include "adaptors.hh"
#include <utopia/core/graph/iterator.hh>

namespace Utopia::Models::ReCooDy
{
/// Calculate the number of interacting agents around vertex v
auto num_interacting_agents = [](const auto v, const auto& g) {
    auto num_counter = 0u;

    if (extraction_mode(v, g) == ExtractionMode::synergistic) {
        ++num_counter;
    }

    for (auto nb : range<IterateOver::neighbors>(v, g)) {
        if (extraction_mode(nb, g) == ExtractionMode::synergistic) {
            ++num_counter;
        }
    }

    return num_counter;
};

/// Calculate the actual investment
/** An agent can invest at most the amount of resources it has. Thus, if the
 * desired investment exceeds the agent's resources, all of the resources is
 * investment but nothing more. This yields the actual_investment which is
 * calculated via this function.
 * Also, if no resourcess are left the actual investment is zero.
 */
auto calculate_actual_investment = [](const auto v, auto& g) {
    if (std::signbit(resources(v,g))){
        return 0.;
    }
    else{
        return std::min(investment(v, g), resources(v, g));
    }
};

/// Calculate the actual investment with uprising
/** An agent can invest at most the amount of resources it has. Thus, if the
 * desired investment exceeds the agent's resources, all of the resources is
 * investment but nothing more. This yields the actual_investment which is
 * calculated via this function.
 * Also, if your resources is negative, the agent can still in a last uprising
 * invest negative costs to take out resources from the synergistic resource
 * pool with every agent connected paying for it.
 */
auto calculate_actual_investment_uprising = [](const auto v, auto& g) {
    // The actual investment is at most the agent's resources
    return std::min(investment(v, g), resources(v, g));
};

/// Calculate the goods created around a vertex
/** The goods is the sum of all actual_investments into this interaction.
 * Hereby, agents plit up their actual_investments such that each interaction
 * they participate in receives an equal investment.
 *
 * \param v The focal vertex of the interaction
 *
 * \return double The goods per agent
 */
auto calculate_goods_and_resources_per_agent = [](const auto r,
                                                  const auto v,
                                                  const auto& g) {
    if (extraction_mode(v, g) == ExtractionMode::synergistic) {
        const double n_v = static_cast<double>(num_interacting_agents(v, g));

        // Accumulate the normalized actual_investments from the neighbors
        // starting with the actual_investment of the agent.
        double investment_fraction = actual_investment(v, g) / n_v;
        double extracted_resources = std::fabs(investment_fraction);

        // If the actual investment is negative, it means that agents
        // actively take out resources from the pool. Thus, it needs to be
        // added to the extracted resources. Collect these destructed energies.
        // If the investment_fraction gets negative, the actual investment
        // had to be zero.
        double destructed_resources = 0.;
        if (std::signbit(investment_fraction)) {
            destructed_resources += std::fabs(investment_fraction);
        }

        // Accumulate quantities of neighbors
        for (const auto nb : range<IterateOver::neighbors>(v, g)) {
            if (extraction_mode(nb, g) == ExtractionMode::synergistic) {
                const double fraction_to_add =
                    actual_investment(nb, g) /
                    (static_cast<double>(num_interacting_agents(nb, g)));

                investment_fraction += fraction_to_add;
                extracted_resources += std::fabs(fraction_to_add);

                if (std::signbit(fraction_to_add)) {
                    destructed_resources += std::fabs(fraction_to_add);
                }
            }
        }

        // Calculate the goods and resources using the synergy factor
        // and all accumulated quantities from above.
        const double goods_per_a = investment_fraction * r / n_v;
        const double extr_resources_per_agent =
            (extracted_resources * r / n_v) + destructed_resources;

        return std::pair(goods_per_a, extr_resources_per_agent);
    }
    else {
        return std::pair(0., 0.);
    }
};

/// Collect the subgame goods shares for agent on vertex v
/** Accumulate the subgame goods shares from all subgames of the
 *  neighboring games. The initial value of the accumulation is the
 *  goods share the of agent's location.
 */
auto collect_goods = [](const auto v, auto& g) {
    if (extraction_mode(v, g) == ExtractionMode::synergistic) {
        double _goods = goods_per_agent(v, g);

        for (const auto nb : range<IterateOver::neighbors>(v, g)) {
            if (extraction_mode(nb, g) == ExtractionMode::synergistic) {
                _goods += goods_per_agent(nb, g);
            }
        }

        g[v].state.goods = _goods;
    }
};
}  // namespace Utopia::Models::ReCooDy

#endif  // UTOPIA_MODELS_RECOODY_INTERACTION_HH