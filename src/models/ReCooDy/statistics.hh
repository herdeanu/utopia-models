#ifndef UTOPIA_MODELS_RECOODY_STATISTICS_HH
#define UTOPIA_MODELS_RECOODY_STATISTICS_HH

#include <cmath>
#include <boost/graph/graph_traits.hpp>

#include <utopia/core/graph.hh>
#include "types.hh"
#include "adaptors.hh"

namespace Utopia::Models::ReCooDy::Statistics
{
/// Calculate the standard deviation
/** This algorithm uses Welford's Online algorithm.
 *
 * \tparam IterateOver  Over which entity to iterate \ref IterateOver
 * \tparam Graph        Type of graph
 * \tparam Adaptor      The adaptor function type
 *
 * \param adaptor       The adaptor function that takes as input arguments
 *                      (vertex_descriptor, graph) and returns the quantity
 *                      over which to calculate the standard deviation.
 * \param g             The graph
 *
 * \note This function returns 0 if the number of GraphEntities to iterate over
 *       is smaller than or equal 1, it returns 0.
 *
 * \return double       The standard deviation
 */
template<IterateOver iterate_over, typename Graph, typename Adaptor>
double std(Adaptor&& adaptor, const Graph& g)
{
    auto [it, it_end] = GraphUtils::iterator_pair<iterate_over>(g);

    if (std::distance(it, it_end) <= 1) {
        return 0.;
    }

    auto n       = 0.;
    auto mean    = 0.;
    auto M2      = 0.;
    auto d       = 0.;
    auto element = 0.;
    for (auto entity : range<iterate_over>(g)) {
        element = adaptor(entity, g);
        n += 1.;
        d = element - mean;
        mean += d / n;
        M2 += d * (element - mean);
    }
    return std::sqrt(M2 / (n - 1.));
}

/// Calculate the standard deviation
/** This algorithm uses Welford's Online algorithm.
 *
 * \tparam IterateOver  Over which entity to iterate \ref IterateOver
 * \tparam Graph        Type of graph
 * \tparam Adaptor      The adaptor function type
 *
 * \param ref_vertex    The reference vertex
 * \param adaptor       The adaptor function that takes as input arguments
 *                      (vertex_descriptor, graph) and returns the quantity
 *                      over which to calculate the standard deviation.
 * \param g             The graph
 *
 * \note This function returns 0 if the number of GraphEntities to iterate
 *       over is smaller than or equal 1, it returns 0.
 *
 * \return double       The standard deviation
 */
template<IterateOver iterate_over, typename Graph, typename Adaptor>
double std(Adaptor&& adaptor,
           typename boost::graph_traits<Graph>::vertex_descriptor ref_vertex,
           const Graph& g)
{
    auto [it, it_end] = GraphUtils::iterator_pair<iterate_over>(ref_vertex, g);

    if (std::distance(it, it_end) <= 1) {
        return 0.;
    }

    auto n       = 0.;
    auto mean    = 0.;
    auto M2      = 0.;
    auto d       = 0.;
    auto element = 0.;
    for (auto entity : range<iterate_over>(ref_vertex, g)) {
        element = adaptor(entity, g);
        n += 1.;
        d = element - mean;
        mean += d / n;
        M2 += d * (element - mean);
    }
    return std::sqrt(M2 / (n - 1.));
}

/// Calculate the mean
/** This algorithm uses Welford's Online algorithm.
 *
 * \tparam IterateOver  Over which entity to iterate \ref IterateOver
 * \tparam Graph        Type of graph
 * \tparam Adaptor      The adaptor function type
 *
 * \param g             The graph
 * \param adaptor       The adaptor function that takes as input arguments
 *                      (vertex_descriptor, graph) and returns the quantity
 *                      over which to calculate the standard deviation.
 *
 * \return double       The standard deviation
 */
template<IterateOver iterate_over, typename Graph, typename Adaptor>
double mean(Adaptor&& adaptor, const Graph& g)
{
    auto [it, it_end] = GraphUtils::iterator_pair<iterate_over>(g);

    if (std::distance(it, it_end) < 1) {
        return 0.;
    }

    auto mean = 0.;
    for (auto entity : range<iterate_over>(g)) {
        mean += adaptor(entity, g);
    }
    return mean / static_cast<double>(boost::num_vertices(g));
}

/// Calculate the mean
/** This algorithm uses Welford's Online algorithm.
 *  It calculates the mean over the absolut value.
 *
 * \tparam IterateOver  Over which entity to iterate \ref IterateOver
 * \tparam Graph        Type of graph
 * \tparam Adaptor      The adaptor function type
 *
 * \param g             The graph
 * \param adaptor       The adaptor function that takes as input arguments
 *                      (vertex_descriptor, graph) and returns the quantity
 *                      over which to calculate the standard deviation.
 *
 * \return double       The standard deviation
 */
template<IterateOver iterate_over, typename Graph, typename Adaptor>
double mean_abs(Adaptor&& adaptor, const Graph& g)
{
    auto [it, it_end] = GraphUtils::iterator_pair<iterate_over>(g);

    if (std::distance(it, it_end) < 1) {
        return 0.;
    }

    auto mean = 0.;
    for (auto entity : range<iterate_over>(g)) {
        mean += std::abs(adaptor(entity, g));
    }
    return mean / static_cast<double>(boost::num_vertices(g));
}

/// Calculate the mean
/** This algorithm uses Welford's Online algorithm.
 *
 * \tparam IterateOver  Over which entity to iterate \ref IterateOver
 * \tparam Graph        Type of graph
 * \tparam Adaptor      The adaptor function type
 *
 * \param adaptor       The adaptor function that takes as input arguments
 *                      (vertex_descriptor, graph) and returns the quantity
 *                      over which to calculate the standard deviation.
 * \param ref_vertex    The reference vertex
 * \param g             The graph
 *
 * \return double       The standard deviation
 */
template<IterateOver iterate_over, typename Graph, typename Adaptor>
double mean(Adaptor&& adaptor,
            typename boost::graph_traits<Graph>::vertex_descriptor ref_vertex,
            const Graph& g)
{
    auto [it, it_end] = GraphUtils::iterator_pair<iterate_over>(ref_vertex, g);

    if (std::distance(it, it_end) < 1) {
        return 0.;
    }
    auto mean = 0.;
    for (auto entity : range<iterate_over>(ref_vertex, g)) {
        mean += adaptor(entity, g);
    }
    return mean / static_cast<double>(boost::num_vertices(g));
}

/// Calculate the mean and standard deviation
/**
 * \tparam IterateOver  Over which entity to iterate \ref IterateOver
 * \tparam Graph        Type of graph
 * \tparam Adaptor      The adaptor function type
 *
 * \param adaptor       The adaptor function that takes as input arguments
 *                      (vertex_descriptor, graph) and returns the quantity
 *                      over which to calculate the standard deviation.
 * \param ref_vertex    The reference vertex
 * \param g             The graph
 *
 * \return double       The standard deviation
 */
template<IterateOver iterate_over, typename Graph, typename Adaptor>
std::pair<double, double> mean_std(Adaptor&& adaptor, const Graph& g)
{
    auto [it, it_end] = GraphUtils::iterator_pair<iterate_over>(g);

    if (std::distance(it, it_end) < 1) {
        return {0., 0.};
    }

    // Calculate mean
    double _mean =
        mean<iterate_over, Graph, Adaptor>(std::forward<Adaptor>(adaptor), g);

    double sq_sum = 0.;
    for (auto entity : range<iterate_over>(g)) {
        sq_sum += (adaptor(entity, g) - _mean) * (adaptor(entity, g) - _mean);
    }

    if constexpr (iterate_over == IterateOver::vertices) {
        const auto n = boost::num_vertices(g);

        double std = std::sqrt(sq_sum / (n - 1.));

        return {_mean, std};
    }
    else if (iterate_over == IterateOver::edges) {
        const auto n = boost::num_edges(g);

        double std = std::sqrt(sq_sum / (n - 1.));

        return {_mean, std};
    }
    else {
        throw std::invalid_argument(
            "The iteration needs to be over vertices or "
            "edges. Set IterateOver differently!");
    }
}

/// Calculate the mean and standard deviation
/**
 * \tparam IterateOver  Over which entity to iterate \ref IterateOver
 * \tparam Graph        Type of graph
 * \tparam Adaptor      The adaptor function type
 *
 * \param adaptor       The adaptor function that takes as input arguments
 *                      (vertex_descriptor, graph) and returns the quantity
 *                      over which to calculate the standard deviation.
 * \param ref_vertex    The reference vertex
 * \param g             The graph
 *
 * \return double       The standard deviation
 */
template<IterateOver iterate_over, typename Graph, typename Adaptor>
std::pair<double, double>
mean_std(Adaptor&& adaptor,
         typename boost::graph_traits<Graph>::vertex_descriptor ref_vertex,
         const Graph& g)
{
    auto [i, i_end] = GraphUtils::iterator_pair<iterate_over>(ref_vertex, g);

    if (std::distance(i, i_end) <= 1) {
        return {0., 0.};
    }

    // Calculate mean
    double _mean =
        mean<iterate_over, Graph, Adaptor>(std::forward<Adaptor>(adaptor),
                                           ref_vertex,
                                           g);

    double sq_sum = 0.;
    for (auto entity : range<iterate_over>(ref_vertex, g)) {
        sq_sum += (adaptor(entity, g) - _mean) * (adaptor(entity, g) - _mean);
    }

    auto [it, it_end] = GraphUtils::iterator_pair<iterate_over>(ref_vertex, g);
    const auto n      = std::distance(it, it_end);

    double std = [&]() {
        if (n <= 1) {
            return 0.;
        }
        else {
            return std::sqrt(sq_sum / (n - 1.));
        }
    }();

    return {_mean, std};
}

template<IterateOver iterate_over,
         LinkMechanism link_mechanism,
         typename Graph,
         LinkMode num_link_mode_bins = LinkMode::COUNT>
decltype(auto) link_bins(const Graph& g)
{
    // Create an array containing num_link_mode_bins initialized to 0
    std::array<std::size_t, static_cast<std::size_t>(num_link_mode_bins)>
        bins {};

    // Explicitely initialize all bins with 0
    for (auto& b : bins) {
        b = 0;
    }

    for (auto v : range<iterate_over>(g)) {
        if constexpr (link_mechanism == LinkMechanism::add_local) {
            ++bins[static_cast<std::size_t>(link_mode_add_local(v, g))];
        }
        else if (link_mechanism == LinkMechanism::add_global) {
            ++bins[static_cast<std::size_t>(link_mode_add_global(v, g))];
        }
        else if (link_mechanism == LinkMechanism::remove) {
            ++bins[static_cast<std::size_t>(link_mode_remove(v, g))];
        }
        else {
            throw std::invalid_argument("Invalid LinkMechanism");
        }
    }

    return bins;
}

/// Calculate the BPEM classification bins
template<IterateOver iterate_over, typename Graph>
decltype(auto) BPEM_bins(const Graph& g)
{
    // Create an array containing 4 bins initialized to 0
    std::array<std::size_t, 4> bins {};

    // Explicitely initialize all bins with 0
    for (auto& b : bins) {
        b = 0;
    }

    // Count the bins of the agent classifications
    for (auto v : range<iterate_over>(g)) {
        ++bins[static_cast<std::size_t>(classification(v, g))];
    }

    return bins;
}

}  // namespace Utopia::Models::ReCooDy::Statistics

#endif  // UTOPIA_MODELS_RECOODY_STATISTICS_HH
