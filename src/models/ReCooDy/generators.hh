/** @file generators.hpp
 *  @brief In this file xorshift-family -Random number generators are
 * implemented (Wikipedia provides a good intro).
 *  @author Harald Mack, harald.mack@iup.uni-heidelberg.de
 *  @note Taken from the author with his permission.
 */

#ifndef UTOPIA_MODELS_AMEEMULTI_GENERATORS_HH
#define UTOPIA_MODELS_AMEEMULTI_GENERATORS_HH
#include <array>
#include <random>
#include <cstdint>

namespace Utopia
{
namespace Models
{
namespace Amee
{
namespace Generators
{

/**
 * @brief  This is a fixed-increment version of Java 8's SplittableRandom
 *         generator, as given here: http://xoshiro.di.unimi.it/splitmix64.c
 *         See http://dx.doi.org/10.1145/2714064.2660195 and
 *         http://docs.oracle.com/javase/8/docs/api/java/util/SplittableRandom.html
 *         Periodlength 2^64.
 *
 *         README: This has only a dummy discard function which does nothing!
 */
class SplitMix64
{
    std::array< uint64_t, 1 > _state;

  public:
    using result_type = uint64_t;
    using state_type  = std::array< uint64_t, 1 >;



    void
    swap(SplitMix64& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            using std::swap;
            swap(_state, other._state);
        }
    }

    /**
     * @brief get next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        uint64_t z = (this->_state[0] += 0x9e3779b97f4a7c15);
        z               = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
        z               = (z ^ (z >> 27)) * 0x94d049bb133111eb;
        return z ^ (z >> 31);
    }

    SplitMix64(typename state_type::value_type single_state) :
    _state(state_type{single_state})
    {
    }
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(SplitMix64& lhs, SplitMix64& rhs)
{
    lhs.swap(rhs);
}


/**
 * @brief Abstract base class for random number generators
 *
 * @tparam Statetype Type of the state the rng holds
 * @tparam Resulttype Type of the random number the RNG's call operator produces
 */
template < typename Statetype, typename Resulttype >
class GeneratorBase
{
  private:
  protected:
    Statetype _state;

  public:
    /**
     * @brief type alias to enable usage in std random distributions
     *
     */
    using result_type = Resulttype;
    /**
     * @brief type alias to enable usage in std random distributions
     *
     */
    using state_type = Statetype;

    /**
     * @brief Bit-rotate function for Xoroshiro generators. Only use with 64 bit
     * state value_types
     *
     */
    static inline uint64_t
    rotl(const uint64_t x, int k)
    {
        return (x << k) | (x >> (64 - k));
    }

    /**
     * @brief get the maximum value able to be stored in the result type
     *
     * @return constexpr result_type
     */
    constexpr static result_type
    max()
    {
        return std::numeric_limits< result_type >::max();
    }

    /**
     * @brief Get the state object
     *
     * @return state_type
     */
    state_type
    get_state()
    {
        return _state;
    }

    /**
     * @brief Set the state object
     *
     */
    void
    set_state(state_type state)
    {
        _state = state;
    }

    /**
     * @brief get the minimum number able to be stored in the result type
     *
     * @return constexpr result_type
     */
    constexpr static result_type
    min()
    {
        return std::numeric_limits< result_type >::min();
    }

    /**
     * @brief Construct a new Xor Shift Base object
     *
     */
    GeneratorBase() = default;

    /**
     * @brief Destroy the Xor Shift Base object
     *
     */
    virtual ~GeneratorBase() = default;

    /**
     * @brief Construct a new Xor Shift Base object
     *
     * @param other
     */
    GeneratorBase(const GeneratorBase& other) = default;

    /**
     * @brief Construct a new Xor Shift Base object
     *
     * @param other
     */
    GeneratorBase(GeneratorBase&& other) = default;

    /**
     * @brief Move assign object
     *
     * @return GeneratorBase&
     */
    GeneratorBase&
    operator=(GeneratorBase&&) = default;

    /**
     * @brief copy assign object
     *
     * @return GeneratorBase&
     */
    GeneratorBase&
    operator=(const GeneratorBase&) = default;

    /**
     * @brief Construct a new Xor Shift Base object
     *
     * @param state
     */
    GeneratorBase(state_type state) : _state(std::forward< state_type >(state))
    {
    }

    /**
     * @brief Construct a new GeneratorBase object, using a single number
     *        which is transformed into an appropriate seed of uniformly
     *        distributed numbers for the rng in question. This is done
     *        using std::seed_seq
     *
     * @param singlestate - a single unsigned int number
     */
    GeneratorBase(typename Statetype::value_type single_state) :
        _state([&]() {

            // use splitmix generator to create seeds
            SplitMix64 spm(single_state);
            Statetype     state;

            for(auto&& s: state)
            {
                s = spm();
            }

            return state;
        }())
    {
    }

    /**
     * @brief swap the state of caller and other
     *
     * @param other
     */
    void
    swap(GeneratorBase& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            using std::swap;
            swap(_state, other._state);
        }
    }
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
template < typename Statetype, typename Resulttype >
void
swap(GeneratorBase< Statetype, Resulttype >& lhs,
     GeneratorBase< Statetype, Resulttype >& rhs)
{
    lhs.swap(rhs);
}

/**
 * @brief Precursor to Xoroshiro128. Fast and reliable, but not quite as fast/
 *        good as Xoroshiro. Period length 2^128.
 *        Implementation given here:
 * http://vigna.di.unimi.it/ftp/papers/xorshiftplus.pdf
 */
class XorShift128plus
    : public GeneratorBase< std::array< uint64_t, 2 >, uint64_t >
{
  public:
    using Base = GeneratorBase< std::array< uint64_t, 2 >, uint64_t >;

    using result_type = typename Base::result_type;

    using state_type = typename Base::state_type;

    void
    swap(XorShift128plus& other)
    {
        Base::swap(static_cast< Base& >(other));
    }

    /**
     * @brief Get the next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        result_type       x = this->_state[0];
        result_type const y = this->_state[1];
        this->_state[0]     = y;
        x ^= x << 23;                                    // a
        this->_state[1] = x ^ y ^ (x >> 17) ^ (y >> 26); // b, c
        return this->_state[1] + y;
    }

    /**
     * @brief discard function equivalent to 2^64 calls to operator().
     *        Can be used to generate 2^64 non-overlapping sequences.
     */
    void
    discard()
    {
        static const uint64_t discard[] = { 0x8a5cd789635d2dff,
                                                 0x121fd2155c472f96 };
        uint64_t              s0        = 0;
        uint64_t              s1        = 0;
        for (std::size_t i = 0; i < sizeof(discard) / sizeof(*discard); i++)
        {
            for (int b = 0; b < 64; b++)
            {
                if (discard[i] & 1ULL << b)
                {
                    s0 ^= this->_state[0];
                    s1 ^= this->_state[1];
                }
                this->operator()();
            }
        }
        this->_state[0] = s0;
        this->_state[1] = s1;
    }

    /**
     * @brief Construct a new XorShift128plus object from a single number
     *
     * @param single_state
     */
    XorShift128plus(typename state_type::value_type single_state) :
        Base(single_state)
    {
    }

    virtual ~XorShift128plus() = default;
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(XorShift128plus& lhs, XorShift128plus& rhs)
{
    lhs.swap(rhs);
}

/**
 * @brief XorShift1024* generator, presented in
 * http://vigna.di.unimi.it/ftp/papers/xorshift.pdf. Period is 2^1024, and
 * according to the authors this passes BigCrush entirely.
 *
 */
class XorShift1024star
    : public GeneratorBase< std::array< uint64_t, 16 >, uint64_t >
{
    int _p;

  public:
    using Base =
        GeneratorBase< std::array< uint64_t, 16 >, uint64_t >;
    using result_type = typename Base::result_type;
    using state_type  = typename Base::state_type;

    void
    swap(XorShift1024star& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            static_cast< Base& >(*this).swap(static_cast< Base& >(other));
        }
    }

    /**
     * @brief Call function for generating the next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        const result_type s0 = this->_state[static_cast< std::size_t >(_p++)];
        result_type       s1 = this->_state[_p &= 15];
        s1 ^= s1 << 31;        // a
        s1 ^= s1 >> 11;        // b
        s1 ^= s0 ^ (s0 >> 30); // c
        this->_state[static_cast< std::size_t >(_p)] = s1;
        return s1 * (result_type)1181783497276652981;
    }

    /**
     * @brief This is the discard function for the generator. It is equivalent
     * to 2^512 calls to operator()(); it can be used to generate 2^512
     * non-overlapping subsequences for parallel computations.
     */
    void
    discard(void)
    {
        static const uint64_t discard[] = {
            0x84242f96eca9c41d, 0xa3c65b8776f96855, 0x5b34a39f070b5837,
            0x4489affce4f31a1e, 0x2ffeeb0a48316f40, 0xdc2d9891fe68c022,
            0x3659132bb12fea70, 0xaac17d8efa43cab8, 0xc4cb815590989b13,
            0x5ee975283d71c93b, 0x691548c86c1bd540, 0x7910c41d10a1e6a5,
            0x0b5fc64563b3e2a8, 0x047f7684e9fc949d, 0xb99181f2d8f685ca,
            0x284600e3f30e38c3
        };

        uint64_t t[16] = { 0 };

        for (std::size_t i = 0; i < sizeof(discard) / sizeof(*discard); ++i)
        {
            for (int b = 0; b < 64; ++b)
            {
                if (discard[i] & 1ULL << b)
                {
                    for (int j = 0; j < 16; j++)
                    {
                        t[j] ^= this->_state[(j + _p) & 15];
                    }
                }
                this->operator()();
            }
        }
        for (int j = 0; j < 16; j++)
            this->_state[(j + _p) & 15] = t[j];
    }

    /**
     * @brief Construct a new XorShift1024star object from
     *        a single number as seed, which then is used to
     *        generate the PRNG's internal state using std::seedseq
     *
     * @param single_state
     */
    XorShift1024star(typename state_type::value_type single_state) :
        Base(single_state),
        _p(15) // 15 taken from Phillip Geier's implementation on gitlab
    {
    }

    virtual ~XorShift1024star() = default;
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(XorShift1024star& lhs, XorShift1024star& rhs)
{
    lhs.swap(rhs);
}


/**
 * @brief Workhorse RNG, described here
 * http://vigna.di.unimi.it/ftp/papers/ScrambledLinear.pdf.
 * Analogous to Xoroshiro256starstar, but with  a shorter state and period
 * (2^128), but is a little bit faster in practice (should be) As suggested by
 * the authors, this is the 'workhorse' generator, which passes all tests of
 * BigCrush (and more) and is fast and long enough for everything other than
 * the most massively parallel computations
 *
 */
class Xoroshiro128starstar
    : public GeneratorBase< std::array< uint64_t, 2 >, uint64_t >
{
  public:
    using Base = GeneratorBase< std::array< uint64_t, 2 >, uint64_t >;
    using state_type  = typename Base::state_type;
    using result_type = typename Base::result_type;

    void
    swap(Xoroshiro128starstar& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            static_cast< Base& >(*this).swap(static_cast< Base& >(other));
        }
    }

    /**
     * @brief get next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        const uint64_t s0     = this->_state[0];
        uint64_t       s1     = this->_state[1];
        const uint64_t result = rotl(s0 * 5, 7) * 9;

        s1 ^= s0;
        this->_state[0] = rotl(s0, 24) ^ s1 ^ (s1 << 16); // a, b
        this->_state[1] = rotl(s1, 37);                   // c

        return result;
    }

    /**
     * @brief This is the discard function for the generator. It is equivalent
     * to 2^64 calls to next(); it can be used to generate 2^64
     * non-overlapping subsequences for parallel computations.
     */
    void
    discard()
    {
        static const uint64_t discard[] = { 0xdf900294d8f554a5,
                                                 0x170865df4b3201fc };

        uint64_t s0 = 0;
        uint64_t s1 = 0;
        for (std::size_t i = 0; i < sizeof(discard) / sizeof(*discard); i++)
            for (int b = 0; b < 64; b++)
            {
                if (discard[i] & UINT64_C(1) << b)
                {
                    s0 ^= this->_state[0];
                    s1 ^= this->_state[1];
                }
                this->operator()();
            }

        this->_state[0] = s0;
        this->_state[1] = s1;
    }

    /**
     * @brief Construct a new Xoroshiro128starstar object from a single number.
     * @param single_state
     */
    Xoroshiro128starstar(typename state_type::value_type single_state):Base(single_state){}

    virtual ~Xoroshiro128starstar() = default;
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(Xoroshiro128starstar& lhs, Xoroshiro128starstar& rhs)
{
    lhs.swap(rhs);
}

/**
 * @brief Xoroshiro128plus generator. Similar to Xoroshiro128star, but with
 *        somewhat worse statisitcal properties in the lower bits.  It is thus
 *        suggested by its Inventors to use this to generator 64bit floating
 *        point numbers only, where it is about 15% faster than
 Xoroshiro128star.
 *
 *        Original comment by the authors (from here:
 http://xoshiro.di.unimi.it/xoshiro128plus.c)
 *        " This is xoshiro256+ 1.0, our best and fastest generator for
 floating-point numbers. We suggest to use its upper bits for floating-point
            generation, as it is slightly faster than xoshiro128**. It passes
 all tests we are aware of except for the lowest three bits, which might fail
 linearity tests (and just those), so if low linear complexity is not considered
 an issue (as it is usually the case) it can be used to generate 64-bit outputs,
 too.

            We suggest to use a sign test to extract a random Boolean value, and
            right shifts to extract subsets of bits.

            The state must be seeded so that it is not everywhere zero. If you
 have a 64-bit seed, we suggest to seed a splitmix64 generator and use its
            output to fill s.
        "
 */
class Xoroshiro128plus
    : public GeneratorBase< std::array< uint64_t, 2 >, uint64_t >
{
  public:
    using Base = GeneratorBase< std::array< uint64_t, 2 >, uint64_t >;
    using state_type  = typename Base::state_type;
    using result_type = typename Base::result_type;

    void
    swap(Xoroshiro128plus& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            static_cast< Base& >(*this).swap(static_cast< Base& >(other));
        }
    }
    /**
     * @brief get next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        const uint64_t s0     = this->_state[0];
        uint64_t       s1     = this->_state[1];
        const uint64_t result = s0 + s1;

        s1 ^= s0;
        this->_state[0] = rotl(s0, 24) ^ s1 ^ (s1 << 16); // a, b
        this->_state[1] = rotl(s1, 37);                   // c

        return result;
    }

    /**
     * @brief This is the discard function for the generator. It is equivalent
     * to 2^64 calls to next(); it can be used to generate 2^64
     * non-overlapping subsequences for parallel computations.
     */
    void
    discard()
    {
        static const uint64_t discard[] = { 0xdf900294d8f554a5,
                                                 0x170865df4b3201fc };

        uint64_t s0 = 0;
        uint64_t s1 = 0;
        for (std::size_t i = 0; i < sizeof(discard) / sizeof(*discard); i++)
            for (int b = 0; b < 64; b++)
            {
                if (discard[i] & UINT64_C(1) << b)
                {
                    s0 ^= this->_state[0];
                    s1 ^= this->_state[1];
                }
                this->operator()();
            }

        this->_state[0] = s0;
        this->_state[1] = s1;
    }

    /**
     * @brief Construct a new Xoroshiro128plus object from a single number.
     *        As suggested by Blackman&Vina, it uses a splitmix64 generator
     *        to seed the state instead of std::seedseq
     *
     * @param single_state
     */
    Xoroshiro128plus(typename state_type::value_type single_state):Base(single_state)
    {}

    virtual ~Xoroshiro128plus() = default;
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(Xoroshiro128plus& lhs, Xoroshiro128plus& rhs)
{
    lhs.swap(rhs);
}

/**
 * @brief Workhorse RNG, described here
 * http://vigna.di.unimi.it/ftp/papers/ScrambledLinear.pdf. As suggested by
 * the authors, this is the 'workhorse' generator, which passes all tests of
 * BigCrush (and more) and is fast and long enough for everything other than
 * the most massively parallel computations
 *
 */
class Xoroshiro256starstar
    : public GeneratorBase< std::array< uint64_t, 4 >, uint64_t >
{
  private:
  public:
    using Base = GeneratorBase< std::array< uint64_t, 4 >, uint64_t >;
    using result_type = typename Base::result_type;
    using state_type  = typename Base::state_type;

    void
    swap(Xoroshiro256starstar& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            static_cast< Base& >(*this).swap(static_cast< Base& >(other));
        }
    }

    /**
     * @brief get the next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        const uint64_t result_starstar =
            this->rotl(this->_state[1] * 5, 7) * 9;

        const uint64_t t = this->_state[1] << 17;

        this->_state[2] ^= this->_state[0];
        this->_state[3] ^= this->_state[1];
        this->_state[1] ^= this->_state[2];
        this->_state[0] ^= this->_state[3];

        this->_state[2] ^= t;

        this->_state[3] = rotl(this->_state[3], 45);

        return result_starstar;
    }

    /**
  * @brief This is the discard function for the generator. It is equivalent
  to 2^128 calls to next(); it can be used to generate 2^128
  non-overlapping subsequences for parallel computations.
  */
    void
    discard()
    {
        static const uint64_t discard[] = { 0x180ec6d33cfd0aba,
                                                 0xd5a61266f0c9392c,
                                                 0xa9582618e03fc9aa,
                                                 0x39abdc4529b1661c };

        uint64_t s0 = 0;
        uint64_t s1 = 0;
        uint64_t s2 = 0;
        uint64_t s3 = 0;
        for (std::size_t i = 0; i < sizeof discard / sizeof *discard; i++)
        {
            for (int b = 0; b < 64; b++)
            {
                if (discard[i] & UINT64_C(1) << b)
                {
                    s0 ^= this->_state[0];
                    s1 ^= this->_state[1];
                    s2 ^= this->_state[2];
                    s3 ^= this->_state[3];
                }
                this->operator()();
            }
        }
        this->_state[0] = s0;
        this->_state[1] = s1;
        this->_state[2] = s2;
        this->_state[3] = s3;
    }

    /**
     * @brief Construct a new Xoroshiro256star object from a single number.
     *        As suggested by Blackman&Vina, it uses a splitmix64 generator
     *        to seed the state instead of std::seedseq
     *
     * @param single_state
     */
    Xoroshiro256starstar(typename state_type::value_type single_state):Base(single_state){}
    virtual ~Xoroshiro256starstar() = default;
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(Xoroshiro256starstar& lhs, Xoroshiro256starstar& rhs)
{
    lhs.swap(rhs);
}

/**
 * @brief Xoroshiro256plus generator. Similar to Xoroshiro256star, but with
 *        somewhat worse statisitcal properties in the lower bits.  It is thus
 *        suggested by its Inventors to use this to generator 64bit floating
 *        point numbers only, where it is about 15% faster than
 Xoroshiro256star.
 *
 *        Original comment by the authors (from here:
 http://xoshiro.di.unimi.it/xoshiro256plus.c)
 *        " This is xoshiro256+ 1.0, our best and fastest generator for
 floating-point numbers. We suggest to use its upper bits for floating-point
            generation, as it is slightly faster than xoshiro256**. It passes
 all tests we are aware of except for the lowest three bits, which might fail
 linearity tests (and just those), so if low linear complexity is not considered
 an issue (as it is usually the case) it can be used to generate 64-bit outputs,
 too.

            We suggest to use a sign test to extract a random Boolean value, and
            right shifts to extract subsets of bits.

            The state must be seeded so that it is not everywhere zero. If you
 have a 64-bit seed, we suggest to seed a splitmix64 generator and use its
            output to fill s.
        "
 */
class Xoroshiro256plus
    : public GeneratorBase< std::array< uint64_t, 4 >, uint64_t >
{
  public:
    using Base = GeneratorBase< std::array< uint64_t, 4 >, uint64_t >;
    using result_type = typename Base::result_type;
    using state_type  = typename Base::state_type;

    void
    swap(Xoroshiro256plus& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            static_cast< Base& >(*this).swap(static_cast< Base& >(other));
        }
    }

    /**
     * @brief Generate the next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        const uint64_t result_plus = this->_state[0] + this->_state[3];

        const uint64_t t = this->_state[1] << 17;

        this->_state[2] ^= this->_state[0];
        this->_state[3] ^= this->_state[1];
        this->_state[1] ^= this->_state[2];
        this->_state[0] ^= this->_state[3];

        this->_state[2] ^= t;

        this->_state[3] = this->rotl(this->_state[3], 45);

        return result_plus;
    }

    /**
     * @brief This is the discard function for the generator. It is equivalent
     * to 2^128 calls to operator()(); it can be used to generate 2^128
     * non-overlapping subsequences for parallel computations.
     */
    void
    discard()
    {
        static const uint64_t discard[] = { 0x180ec6d33cfd0aba,
                                                 0xd5a61266f0c9392c,
                                                 0xa9582618e03fc9aa,
                                                 0x39abdc4529b1661c };

        uint64_t s0 = 0;
        uint64_t s1 = 0;
        uint64_t s2 = 0;
        uint64_t s3 = 0;
        for (std::size_t i = 0; i < sizeof(discard) / sizeof(*discard); i++)
        {
            for (int b = 0; b < 64; b++)
            {
                if (discard[i] & UINT64_C(1) << b)
                {
                    s0 ^= this->_state[0];
                    s1 ^= this->_state[1];
                    s2 ^= this->_state[2];
                    s3 ^= this->_state[3];
                }
                this->operator()();
            }
        }

        this->_state[0] = s0;
        this->_state[1] = s1;
        this->_state[2] = s2;
        this->_state[3] = s3;
    }

    /**
     * @brief Construct a new Xoroshiro256plus object from a single number
     * using the SplitMix64 generator as suggested by Blackman&Vino
     *
     * @param single_state
     */
    Xoroshiro256plus(typename state_type::value_type single_state):Base(single_state){}

    virtual ~Xoroshiro256plus() = default;
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(Xoroshiro256plus& lhs, Xoroshiro256plus& rhs)
{
    lhs.swap(rhs);
}

/**
 * @brief Verson of Xoroshiro256starstar with longer period and larger state
 * of 512 bits, should result in period of 2^512.
 *
 */
class Xoroshiro512starstar
    : public GeneratorBase< std::array< uint64_t, 8 >, uint64_t >
{
  private:
  public:
    using Base = GeneratorBase< std::array< uint64_t, 8 >, uint64_t >;
    using result_type = typename Base::result_type;
    using state_type  = typename Base::state_type;

    void
    swap(Xoroshiro512starstar& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            static_cast< Base& >(*this).swap(static_cast< Base& >(other));
        }
    }

    /**
     * @brief Get the next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        const uint64_t result_starstar = rotl(this->_state[1] * 5, 7) * 9;

        const uint64_t t = this->_state[1] << 11;

        this->_state[2] ^= this->_state[0];
        this->_state[5] ^= this->_state[1];
        this->_state[1] ^= this->_state[2];
        this->_state[7] ^= this->_state[3];
        this->_state[3] ^= this->_state[4];
        this->_state[4] ^= this->_state[5];
        this->_state[0] ^= this->_state[6];
        this->_state[6] ^= this->_state[7];

        this->_state[6] ^= t;

        this->_state[7] = rotl(this->_state[7], 21);

        return result_starstar;
    }

    /**
     * @brief This is the discard function for the generator. It is equivalent
     * to 2^256 calls to next(); it can be used to generate 2^256
     * non-overlapping subsequences for parallel computations.
     */
    void
    discard()
    {
        static const uint64_t discard[] = {
            0x33ed89b6e7a353f9, 0x760083d7955323be, 0x2837f2fbb5f22fae,
            0x4b8c5674d309511c, 0xb11ac47a7ba28c25, 0xf1be7667092bcc1c,
            0x53851efdb6df0aaf, 0x1ebbc8b23eaf25db
        };

        uint64_t t[sizeof(this->_state) / sizeof(*this->_state.data())];
        std::memset(t, 0, sizeof t);
        for (std::size_t i = 0; i < sizeof(discard) / sizeof(*discard); i++)
        {
            for (int b = 0; b < 64; b++)
            {
                if (discard[i] & UINT64_C(1) << b)
                {
                    for (std::size_t w = 0;
                         w <
                         sizeof(this->_state) / sizeof(*this->_state.data());
                         w++)
                    {
                        t[w] ^= this->_state[w];
                    }
                }
                this->operator()();
            }
        }

        std::memcpy(this->_state.data(), t, sizeof(this->_state));
    }

    /**
     * @brief Construct a new Xoroshiro512starstar object from a single
     * number using the SplitMix64 generator as suggested by Blackman&Vino
     *
     * @param single_state
     */
    Xoroshiro512starstar(typename state_type::value_type single_state):Base(single_state){}

    virtual ~Xoroshiro512starstar() = default;
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(Xoroshiro512starstar& lhs, Xoroshiro512starstar& rhs)
{
    lhs.swap(rhs);
}

/**
 * @brief Slightly faster version of Xoroshiro512starstar
 *
 * According to the authors (here: http://xoshiro.di.unimi.it/xoshiro512plus.c):
 * "This is xoshiro512+ 1.0, our
 * generator for floating-point numbers with increased state size. We suggest to
 * use its upper bits for floating-point generation, as it is slightly faster
 * than xoshiro512**. It passes all tests we are aware of except for the lowest
 * three bits, which might fail linearity tests (and just those), so if low
 * linear complexity is not considered an issue (as it is usually the case) it
 *        can be used to generate 64-bit outputs, too."
 *
 * We suggest to use a sign test to extract a random Boolean value, and
 * right shifts to extract subsets of bits.

 * The state must be seeded so that it is not everywhere zero. If you have
 * a 64-bit seed, we suggest to seed a splitmix64 generator and use its
 * output to fill s."
 */

class Xoroshiro512plus
    : public GeneratorBase< std::array< uint64_t, 8 >, uint64_t >
{
  private:
  public:
    using Base = GeneratorBase< std::array< uint64_t, 8 >, uint64_t >;
    using result_type = typename Base::result_type;
    using state_type  = typename Base::state_type;

    void
    swap(Xoroshiro512plus& other)
    {
        if (this == &other)
        {
            return;
        }
        else
        {
            static_cast< Base& >(*this).swap(static_cast< Base& >(other));
        }
    }

    /**
     * @brief Get the next random number
     *
     * @return result_type
     */
    inline result_type
    operator()()
    {
        const uint64_t result_plus = this->_state[0] + this->_state[2];

        const uint64_t t = this->_state[1] << 11;

        this->_state[2] ^= this->_state[0];
        this->_state[5] ^= this->_state[1];
        this->_state[1] ^= this->_state[2];
        this->_state[7] ^= this->_state[3];
        this->_state[3] ^= this->_state[4];
        this->_state[4] ^= this->_state[5];
        this->_state[0] ^= this->_state[6];
        this->_state[6] ^= this->_state[7];

        this->_state[6] ^= t;

        this->_state[7] = rotl(this->_state[7], 21);

        return result_plus;
    }

    /**
     * @brief This is the discard function for the generator. It is equivalent
     * to 2^256 calls to next(); it can be used to generate 2^256
     * non-overlapping subsequences for parallel computations.
     */
    void
    discard()
    {
        static const uint64_t discard[] = {
            0x33ed89b6e7a353f9, 0x760083d7955323be, 0x2837f2fbb5f22fae,
            0x4b8c5674d309511c, 0xb11ac47a7ba28c25, 0xf1be7667092bcc1c,
            0x53851efdb6df0aaf, 0x1ebbc8b23eaf25db
        };
        uint64_t t[sizeof(this->_state) / sizeof(*this->_state.data())];
        std::memset(t, 0, sizeof t);
        for (std::size_t i = 0; i < sizeof(discard) / sizeof(*discard); i++)
        {
            for (int b = 0; b < 64; b++)
            {
                if (discard[i] & UINT64_C(1) << b)
                {
                    for (std::size_t w = 0;
                         w <
                         sizeof(this->_state) / sizeof(*this->_state.data());
                         w++)
                    {
                        t[w] ^= this->_state[w];
                    }
                }
                this->operator()();
            }
        }

        std::memcpy(this->_state.data(), t, sizeof(this->_state));
    }

    /**
     * @brief Construct a new Xoroshiro512plus object from a single number
     * using the SplitMix64 generator as suggested by Blackman&Vino
     *
     * @param single_state
     */
    Xoroshiro512plus(typename state_type::value_type single_state):Base(single_state){}

    virtual ~Xoroshiro512plus() = default;
};

/**
 * @brief Swaps states of lhs and rhs
 *
 * @param lhs
 * @param rhs
 */
void
swap(Xoroshiro512plus& lhs, Xoroshiro512plus& rhs)
{
    lhs.swap(rhs);
}
} // namespace Utils
} // namespace Amee
} // namespace Models
} // namespace Utopia
#endif
