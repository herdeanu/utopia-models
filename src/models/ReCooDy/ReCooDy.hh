#ifndef UTOPIA_MODELS_RECOODY_HH
#define UTOPIA_MODELS_RECOODY_HH

#include <algorithm>
#include <functional>
#include <iterator>
#include <limits>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/random.hpp>

#include <utopia/core/graph.hh>
#include <utopia/core/model.hh>
#include <utopia/core/types.hh>
#include <utopia/data_io/graph_utils.hh>

#include "adaptors.hh"
#include "params.hh"
#include "interaction.hh"
#include "classification.hh"
#include "linking.hh"
#include "statistics.hh"
#include "types.hh"
#include "utils.hh"
#include "write_tasks.hh"
#include "death_states.hh"

namespace Utopia::Models::ReCooDy
{
/// The ReCooDy Model
/** The ReCooDy model is a tool to investigate the emergence of cooperation.
 *  Agents are connected through a social Graph and interact with each other
 *  through a continuous Public Goods Game (PGG). An evolutionary process is
 *  introduced which selects for success in the PGG and inherits agent's
 *  personal investments.
 */
class ReCooDy : public Model<ReCooDy, ReCooDyTypes>
{
  public:
    /// The base model type
    using Base = Model<ReCooDy, ReCooDyTypes>;

    /// Data type that holds the configuration
    using Config = typename Base::Config;

    /// Data type of the group to write model data to, holding datasets
    using DataGroup = typename Base::DataGroup;

    /// Data type for a dataset
    using DataSet = typename Base::DataSet;

    /// Data type of the shared RNG
    using RNG = typename Base::RNG;

    /// Data type for vertex descriptor
    using VertexDesc = boost::graph_traits<Graph>::vertex_descriptor;

    /// Data type for vertex iterator
    using VertexIter = boost::graph_traits<Graph>::vertex_iterator;

    /// Vertex filter predicate for filtered graph
    using VertexFilter = std::function<bool(VertexDesc)>;

    /// Vertex filter predicate for filtered graph
    using VertexFilter2 = std::function<bool(VertexDesc, Graph)>;

    /// The type of the filtered graph
    using FilteredGraph =
        boost::filtered_graph<Graph, boost::keep_all, VertexFilter>;

    /// Data type for rule function applied to vertices of the Graph
    using RuleFunc = std::function<void(VertexDesc, Graph&)>;

  private:
    // Base members: _time, _name, _cfg, _hdfgrp, _rng, _log, _monitor

    // -- Members of this model -----------------------------------------------

    /// The cost of living
    const double _cost_of_living;

    /// The upper limit of the resources
    const double _resources_upper_limit;

    /// Parameters defining the resources
    ResourceParams _resources;

    /// Parameters needed for the evolution mechanism
    EvoParams _evo;

    /// Parameters needed for the link mechanism
    LinkParams _link;

    // .. Distributions .......................................................
    /// Distribution specifying the mutation of an agent's strength
    std::uniform_real_distribution<double> _distr_strength_mutation;

    /// Distribution specifying the mutation of an agent's investment
    std::uniform_real_distribution<double> _distr_investment_mutation;

    /// Distribution specifying the mutation of an agent's interaction
    /// probability
    std::uniform_real_distribution<double> _distr_interact_mutation;

    /// Distribution specifying the mutation of an agent's local linking
    /// probability
    std::uniform_real_distribution<double> _distr_p_add_local_links_mutation;

    /// Distribution specifying the link mode mutation
    std::uniform_int_distribution<std::size_t> _distr_link_mode;

    /// Distribution defining a random integer step from [-1, 0, 1]
    std::uniform_int_distribution<int> _distr_random_int_step;

    /// Distribution defining a unit interval
    std::uniform_real_distribution<double> _distr_unit_interval;

    /// The agent number counter
    std::size_t _agent_number_counter;

    // .. The Graph ...........................................................
    /// The Graph
    Graph _g;

    /// The filtered graph for basic interaction
    FilteredGraph _g_int_basic;

    // .. Temporary tracking parameters .......................................
    /// The remaining basic resources
    double _remaining_basic_resources;

    /// The remaining synergistic resources
    double _remaining_synergistic_resources;

    /// The dead agents' states that are temporarily stored to write them out
    /// together
    DeathStates _death_states;

    // .. Datasets and groups .................................................
    /// Data group that holds the Graph structure and property datasets
    std::shared_ptr<DataGroup> _grp_g;

    /// Dataset for the synergy factor r
    const std::shared_ptr<DataSet> _dset_r;

    /// Dataset for the remaining basic resources
    const std::shared_ptr<DataSet> _dset_remaining_basic_resources;

    /// Dataset for the remaining synergistic resources
    const std::shared_ptr<DataSet> _dset_remaining_synergistic_resources;

    /// Dataset for the mean investments
    const std::shared_ptr<DataSet> _dset_p_synergistic_mean;

    /// Dataset for the mean investments
    const std::shared_ptr<DataSet> _dset_investment_mean;

    /// Dataset for the standard deviation of investments
    const std::shared_ptr<DataSet> _dset_investment_std;

    /// Dataset for the mean payoffs
    const std::shared_ptr<DataSet> _dset_payoff_mean;

    /// Dataset for the standard_deviation of payoff
    const std::shared_ptr<DataSet> _dset_payoff_std;

    /// Dataset for the mean resources
    const std::shared_ptr<DataSet> _dset_basic_resource_mean;

    /// Dataset for the mean energies
    const std::shared_ptr<DataSet> _dset_resources_mean;

    /// Dataset for the standard_deviation of resources
    const std::shared_ptr<DataSet> _dset_resources_std;

    /// Dataset for the mean ages
    const std::shared_ptr<DataSet> _dset_age_mean;

    /// Dataset for the standard_deviation of age
    const std::shared_ptr<DataSet> _dset_age_std;

    /// Dataset for the mean degree
    const std::shared_ptr<DataSet> _dset_degree_mean;

    /// Dataset for the mean add_link_threshold
    const std::shared_ptr<DataSet> _dset_add_link_threshold_mean;

    /// Dataset for the standard_deviation of add_link_threshold
    const std::shared_ptr<DataSet> _dset_add_link_threshold_std;

    /// Dataset for the mean remove_link_threshold
    const std::shared_ptr<DataSet> _dset_remove_link_threshold_mean;

    /// Dataset for the standard_deviation of remove_link_threshold
    const std::shared_ptr<DataSet> _dset_remove_link_threshold_std;

    /// Dataset for the mean p_add_local_links
    const std::shared_ptr<DataSet> _dset_p_add_local_links_mean;

    /// Dataset for the standard_deviation of p_add_local_links
    const std::shared_ptr<DataSet> _dset_p_add_local_links_std;

    /// Dataset for the bins of link mechanism add_local
    const std::shared_ptr<DataSet> _dset_bins_add_local;

    /// Dataset for the bins of link mechanism add_global
    const std::shared_ptr<DataSet> _dset_bins_add_global;

    /// Dataset for the bins of link mechanism remove
    const std::shared_ptr<DataSet> _dset_bins_remove;

    /// Dataset for the number of all agents
    const std::shared_ptr<DataSet> _dset_num_agents;

  public:
    /// Construct the ReCooDy model
    /** \param name     Name of this model instance
     *  \param parent   The parent model this model instance resides in
     *  \param write_tasks The write tasks
     */
    template<class ParentModel, typename... WriteTasks>
    ReCooDy(const std::string& name,
            ParentModel& parent,
            const DataIO::Config& custom_cfg      = {},
            std::tuple<WriteTasks...> write_tasks = {}) :
        // Initialize first via base model
        Base(name, parent, custom_cfg, write_tasks),

        // Set model parameter
        _cost_of_living(get_as<double>("cost_of_living", this->_cfg)),
        _resources_upper_limit(
            get_as<double>("resources_upper_limit", this->_cfg)),
        _resources(get_as<DataIO::Config>("resources", this->_cfg)),
        _evo(get_as<DataIO::Config>("evo", this->_cfg)),
        _link(get_as<DataIO::Config>("link", this->_cfg)),

        // Set distributions
        _distr_strength_mutation {_evo.strength_mutation.first,
                                  _evo.strength_mutation.second},
        _distr_investment_mutation {_evo.investment_mutation.first,
                                    _evo.investment_mutation.second},
        _distr_interact_mutation {_evo.p_synergistic_mutation.first,
                                  _evo.p_synergistic_mutation.second},
        _distr_p_add_local_links_mutation {
            _evo.p_add_local_links_mutation.first,
            _evo.p_add_local_links_mutation.second},
        _distr_link_mode {0, static_cast<std::size_t>(LinkMode::COUNT) - 1},
        _distr_random_int_step {-1, 1}, _distr_unit_interval {0., 1.},

        // Initialize the parent Graph and all filtered views
        // NOTE Someday, plese clean this up... Normally, it _should_ be no
        //      problem to store the filtered graphs in unordered_map
        //      with LinkModes as keys. However... this does not work due to
        //      mysterious BGL errors. So, first get it working, and then
        //      try making this code more concise.
        _agent_number_counter(0), _g(this->initialize_g()),
        _g_int_basic {_g,
                      boost::keep_all {},
                      static_cast<VertexFilter>([this](VertexDesc v) {
                          return (extraction_mode(v, this->_g) ==
                                  ExtractionMode::basic);
                      })},

        // Temporary variables
        _remaining_basic_resources(_resources.basic.amount),
        _remaining_synergistic_resources(_resources.synergistic.amount),
        _death_states (_time,  _evo.track_deaths_from_time, _evo.skip_every_n_agents),

        // Create the datasets
        _grp_g(Utopia::DataIO::create_graph_group(_g, this->_hdfgrp, "graph")),
        _dset_r(this->_hdfgrp->open_dataset("r", {1}, {}, 1)),
        _dset_remaining_basic_resources(
            this->create_dset("remaining_basic_resources", this->_hdfgrp, {})),
        _dset_remaining_synergistic_resources(
            this->create_dset("remaining_synergistic_resources",
                              this->_hdfgrp,
                              {})),
        _dset_p_synergistic_mean(
            this->create_dset("p_synergistic_mean", this->_hdfgrp, {})),
        _dset_investment_mean(
            this->create_dset("investment_mean", this->_hdfgrp, {})),
        _dset_investment_std(
            this->create_dset("investment_std", this->_hdfgrp, {})),
        _dset_payoff_mean(this->create_dset("payoff_mean", this->_hdfgrp, {})),
        _dset_payoff_std(this->create_dset("payoff_std", this->_hdfgrp, {})),
        _dset_basic_resource_mean(
            this->create_dset("basic_resource_mean", this->_hdfgrp, {})),
        _dset_resources_mean(
            this->create_dset("resources_mean", this->_hdfgrp, {})),
        _dset_resources_std(
            this->create_dset("resources_std", this->_hdfgrp, {})),
        _dset_age_mean(this->create_dset("age_mean", this->_hdfgrp, {})),
        _dset_age_std(this->create_dset("age_std", this->_hdfgrp, {})),
        _dset_degree_mean(this->create_dset("degree_mean", this->_hdfgrp, {})),
        _dset_add_link_threshold_mean(
            this->create_dset("add_link_threshold_mean", this->_hdfgrp, {})),
        _dset_add_link_threshold_std(
            this->create_dset("add_link_threshold_std", this->_hdfgrp, {})),
        _dset_remove_link_threshold_mean(
            this->create_dset("remove_link_threshold_mean", this->_hdfgrp, {})),
        _dset_remove_link_threshold_std(
            this->create_dset("remove_link_threshold_std", this->_hdfgrp, {})),
        _dset_p_add_local_links_mean(
            this->create_dset("p_add_local_links_mean", this->_hdfgrp, {})),
        _dset_p_add_local_links_std(
            this->create_dset("p_add_local_links_std", this->_hdfgrp, {})),
        _dset_bins_add_local(
            this->create_dset("bins_add_local",
                              this->_hdfgrp,
                              {static_cast<std::size_t>(LinkMode::COUNT)})),
        _dset_bins_add_global(
            this->create_dset("bins_add_global",
                              this->_hdfgrp,
                              {static_cast<std::size_t>(LinkMode::COUNT)})),
        _dset_bins_remove(
            this->create_dset("bins_remove",
                              this->_hdfgrp,
                              {static_cast<std::size_t>(LinkMode::COUNT)})),
        _dset_num_agents(this->create_dset("num_agents", this->_hdfgrp, {}))
    {
        this->_log->debug("Constructing the ReCooDy model ...");

        // Write single values
        _dset_r->write(_resources.synergistic.r);
        _dset_r->add_attribute("dim_name__0", "r");

        // Add attributes to datatset
        _dset_bins_add_local->add_attribute("dim_name__1", "bins");
        _dset_bins_add_global->add_attribute("dim_name__1", "bins");
        _dset_bins_remove->add_attribute("dim_name__1", "bins");
    }

  private:
    // -- Setup functions -----------------------------------------------------
    /// Initialize the link mode from a configuration node
    LinkMode initialize_link_mode(const DataIO::Config& cfg)
    {
        // Get the probabilities for the modes
        const auto& cfg_p_mode = get_as<DataIO::Config>("p_mode", cfg);
        const auto p_resources = get_as<double>("resources", cfg_p_mode);
        const auto p_p_synergistic =
            get_as<double>("p_synergistic", cfg_p_mode);
        const auto p_investment = get_as<double>("investment", cfg_p_mode);
        const auto p_random     = get_as<double>("random", cfg_p_mode);
        const auto p_strength   = get_as<double>("strength", cfg_p_mode);
        const auto p_goods      = get_as<double>("goods", cfg_p_mode);
        const auto p_payoff     = get_as<double>("payoff", cfg_p_mode);
        const auto p_NONE       = get_as<double>("NONE", cfg_p_mode);

        // Check that the probabilities add up to one
        if (std::fabs((p_resources + p_p_synergistic + p_investment + p_random +
                       p_strength + p_goods + p_payoff + p_NONE) -
                      1.) > 2 * std::numeric_limits<double>::epsilon()) {
            throw std::invalid_argument(
                "The probabilities `p_resources` + `p_p_synergistic` + "
                "`p_investment` +`p_random` + `p_strength` + "
                "`p_goods` + `p_payoff` in the configuration node `p_mode` "
                "do not add up to one!");
        }

        // Draw a number from a uniform real distribution in the unit interval
        const auto rand = _distr_unit_interval(*this->_rng);

        // Select the LinkMode to return with given probabilities
        if (rand < p_resources) {
            return LinkMode::resources;
        }
        else if (rand < p_resources + p_p_synergistic) {
            return LinkMode::p_synergistic;
        }
        else if (rand < p_resources + p_p_synergistic + p_investment) {
            return LinkMode::investment;
        }
        else if (rand <
                 p_resources + p_p_synergistic + p_investment + p_random) {
            return LinkMode::random;
        }
        else if (rand < p_resources + p_p_synergistic + p_investment +
                            p_random + p_strength) {
            return LinkMode::strength;
        }
        else if (rand < p_resources + p_p_synergistic + p_investment +
                            p_random + p_strength + p_goods) {
            return LinkMode::goods;
        }
        else if (rand < p_resources + p_p_synergistic + p_investment +
                            p_random + p_strength + p_goods + p_payoff) {
            return LinkMode::payoff;
        }
        else if (rand < p_resources + p_p_synergistic + p_investment +
                            p_random + p_strength + p_goods + p_payoff +
                            p_NONE) {
            return LinkMode::NONE;
        }
        else {
            throw std::invalid_argument(
                "The link mode initialization through initial probabilities "
                "did not run correctly: The probabilities do not add up to "
                "one!");
        }
    }

    /// Initialize all agents
    /** Agents are initialized using the model configuration parameter
     *  that are provided in the model configuration under the subnode 'init'.
     */
    void initialize_agents(Graph& g)
    {
        this->_log->debug("Initialize all agents ...");

        // Initialize all agents
        for (const auto v : range<IterateOver::vertices>(g)) {
            // Get the initialization configuration node
            const auto& cfg = get_as<DataIO::Config>("init", this->_cfg);

            g[v].state.agent_number = set_new_agent_number();

            // Set state variables from the configuration node
            g[v].state.p_synergistic     = get_as<double>("p_synergistic", cfg);
            g[v].state.extraction_mode   = ExtractionMode::basic;
            g[v].state.investment        = get_as<double>("investment", cfg);
            g[v].state.actual_investment = 0.;
            g[v].state.strength          = get_as<double>("strength", cfg);
            g[v].state.old_investment    = g[v].state.investment;
            g[v].state.resources         = get_as<double>("resources", cfg);
            const auto& cfg_link         = get_as<DataIO::Config>("link", cfg);
            g[v].state.p_add_local_links =
                get_as<double>("p_add_local_links",
                               get_as<DataIO::Config>("add", cfg_link));
            g[v].state.link_mode_add_local =
                initialize_link_mode(get_as<DataIO::Config>(
                    "local",
                    get_as<DataIO::Config>("add", cfg_link)));
            g[v].state.link_mode_add_global =
                initialize_link_mode(get_as<DataIO::Config>(
                    "global",
                    get_as<DataIO::Config>("add", cfg_link)));
            g[v].state.link_mode_remove = initialize_link_mode(
                get_as<DataIO::Config>("remove", cfg_link));

            g[v].state.add_link_threshold =
                get_as<unsigned>("add_link_threshold", cfg);
            g[v].state.remove_link_threshold =
                get_as<unsigned>("remove_link_threshold", cfg);
            g[v].state.num_links        = boost::out_degree(v, g);
            g[v].state.num_active_links = calc_num_active_links(v, g);

            // Set the agent's spendings to zero
            g[v].state.spending_living         = 0.;
            g[v].state.spending_strength       = 0.;
            g[v].state.spending_cost           = 0.;
            g[v].state.spending_link_addition  = 0.;
            g[v].state.spending_link_removal   = 0.;
            g[v].state.spending_birth          = 0.;
            g[v].state.spendings_living        = 0.;
            g[v].state.spendings_strength      = 0.;
            g[v].state.spendings_cost          = 0.;
            g[v].state.spendings_link_addition = 0.;
            g[v].state.spendings_link_removal  = 0.;
            g[v].state.spendings_birth         = 0.;
            g[v].state.grabbing                = 0.;
            g[v].state.grabbings               = 0.;
            g[v].state.classification          = Classification::B;

            // Set developing agent state parameter
            g[v].state.basic_resource       = 0.;
            g[v].state.cooperativity        = 0.;
            g[v].state.payoff               = 0.;
            g[v].state.payoffs              = 0.;
            g[v].state.basic_resources      = 0.;
            g[v].state.goods_per_agent      = 0.;
            g[v].state.age                  = 0;
            g[v].state.time_birth           = 0;
            g[v].state.time_death           = 0;
            g[v].state.death_cause          = DeathCause::NONE;
            g[v].state.num_offspring        = 0;
            g[v].state.num_added_links      = 0;
            g[v].state.num_removed_links    = 0;
            g[v].state.resources_throughput = resources(v, g);
        }

        // Initialize all links
        for (const auto e : range<IterateOver::edges>(g)) {
            g[e].state.age = 0;
        }

        this->_log->debug("Finished agents initialization ...");
    }

    Graph initialize_g()
    {
        this->_log->debug("Create and initialize the Graph ...");

        auto g = Utopia::Graph::create_graph<Graph>(this->_cfg["create_graph"],
                                                    *this->_rng);

        // Initialize agents
        this->initialize_agents(g);

        // Return the Graph
        return g;
    }

    std::size_t set_new_agent_number()
    {
        return ++_agent_number_counter;
    }

  private:
    // -- Resource Extraction -------------------------------------------------
    /// Extract resources
    void extract_resources()
    {
        this->_log->debug("Extracting resources ...");

        // Update the extraction mode of all agents
        apply_rule<IterateOver::vertices, Update::async, Shuffle::off>(
            [this](const auto v, auto& g) {
                if (this->_distr_unit_interval(*this->_rng) <
                    p_synergistic(v, g)) {
                    g[v].state.extraction_mode = ExtractionMode::synergistic;
                }
                else {
                    g[v].state.extraction_mode = ExtractionMode::basic;
                }
            },
            _g);

        // Extract basic resources individually
        _remaining_basic_resources = extract_basic_resources();

        // Extract resources through synergistic public goods game interaction
        extract_synergistic_resources();

        // -- Update resource extraction tracking parameters
        // Basic resource extracted for the synergistic extraction is zero
        apply_rule<IterateOver::vertices, Update::async, Shuffle::off>(
            [](const auto v, auto& g) {
                if (extraction_mode(v, g) == ExtractionMode::synergistic) {
                    g[v].state.basic_resource = 0.;
                }
                else if (extraction_mode(v, g) == ExtractionMode::basic) {
                    g[v].state.payoff = 0.;
                    g[v].state.goods  = 0.;
                }
            },
            _g);

        this->_log->debug("Finished resource extraction!");
    }

    // -- Basic Resource Extraction -------------------------------------------
    /// Extract the basic resources
    double extract_basic_resources()
    {
        // Get the vertex iterators ...
        auto [v, v_end] =
            GraphUtils::iterator_pair<IterateOver::vertices>(_g_int_basic);

        // ... and sort them corresponding to the agent's strength
        // NOTE Unfortunately, it is not possible to directly sort the
        //      vertices because they are stored in a std::list which is
        //      not compatible with std::sort. That's why the vertex
        //      descriptors need to be copied into a vector first.
        std::vector<VertexDesc> vec(v, v_end);
        std::sort(vec.begin(), vec.end(), [this](const auto a0, const auto a1) {
            return (strength(a0, this->_g) > strength(a1, this->_g));
        });

        // Now go through all strength-sorted agents and extract resources
        // from the basic resource reservoir. If nothing is left in the
        // reservoir, agents do not receive any resources.
        auto remaining_resources = _resources.basic.amount;
        for (auto _v : vec) {
            // Leave the loop if no resources are available any more
            if (remaining_resources <= 0) {
                _g[_v].state.basic_resource = 0.;
            }
            else {
                // Calculate the actual resources intake. If the agent already
                // reached its upper resources limit, it only extracts so many
                // resources that it's resources reservoir is completely filled.
                const auto intake = [this, _v]() {
                    const auto max_intake = _resources.basic.intake;
                    const auto new_resources =
                        resources(_v, this->_g) + max_intake;

                    // The resources limit is not reached. Just extract exactly
                    // the resources intake.
                    if (new_resources < _resources_upper_limit) {
                        return max_intake;
                    }

                    // The resources intake would exceed the upper resources
                    // limit. Just extract the difference to fill the agent's
                    // resources reservoir.
                    else {
                        return (max_intake -
                                (new_resources - _resources_upper_limit));
                    }
                }();

                // There are not enough resources in the pool. Just extract
                // all the remaining resources
                if ((remaining_resources - intake) < 0.) {
                    _g[_v].state.resources += remaining_resources;
                    _g[_v].state.resources_throughput += remaining_resources;
                    _g[_v].state.basic_resource = remaining_resources;
                    _g[_v].state.basic_resources += remaining_resources;

                    remaining_resources = 0.;
                }
                else {
                    // Add the intake to the agents resources
                    _g[_v].state.resources += intake;
                    _g[_v].state.resources_throughput += intake;
                    _g[_v].state.basic_resource = intake;
                    _g[_v].state.basic_resources += intake;

                    // Shrink the available resources
                    remaining_resources -= intake;
                }
            }
        }
        return remaining_resources;
    }

    // -- Synergistic Resource Extraction -------------------------------------
    /// Set the resources to a new resources and clip it if it exceeds the
    /// maximum
    std::function<void(VertexDesc, double, double)> set_and_clip_resources =
        [this](const VertexDesc v,
               const double resources,
               const double to_add) {
            const double new_resources =
                std::min(_resources_upper_limit, resources + to_add);

            const double added = new_resources - resources;

            this->_g[v].state.resources = new_resources;
            this->_g[v].state.resources_throughput += added;
        };

    /// Calculate the payoffs of the agents
    /** To calculate the payoff take the previously calculated goods that is
     *  stored as property of the vertex in the Graph and subtract the
     * investment of the agent. The resulting payoff is accumulated to the
     *  resources and additionally stored in the payoff.
     *
     *  \attention  This function realies on the previously calculated
     *              bundled vertex property 'goods'.
     *
     *  \return The remaining resources
     */
    template<typename GraphType>
    double calculate_payoffs_and_add_to_resources(GraphType& g)
    {
        // Sort the agents corresponding to the agent's payoff from the
        // interactions
        //  NOTE Unfortunately, it is not possible to directly sort the
        //      vertices because they are stored in a std::list which is
        //      not compatible with std::sort. That's why the vertex
        //      descriptors need to be copied into a vector first.
        auto [v_unsorted, v_unsorted_end] = boost::vertices(g);

        // Copy all vertices that have ExtractionMode::synergistic into a vector
        // which then is sorted.
        std::vector<VertexDesc> vec;
        vec.reserve(boost::num_vertices(g));
        std::copy_if(v_unsorted,
                     v_unsorted_end,
                     std::back_inserter(vec),
                     [&g](auto v) {
                         return (extraction_mode(v, g) ==
                                 ExtractionMode::synergistic);
                     });

        std::sort(vec.begin(), vec.end(), [this](const auto a0, const auto a1) {
            return (goods(a0, this->_g) - actual_investment(a0, this->_g) >
                    goods(a1, this->_g) - actual_investment(a1, this->_g));
        });

        // Now go through all payoff-sorted agents and extract resources
        // from the synergistic resource reservoir. If nothing is left in the
        // reservoir, agents do not receive any resources.
        auto remaining_resources = _resources.synergistic.amount;
        for (auto v : vec) {
            if (extraction_mode(v, g) == ExtractionMode::synergistic) {
                // Store the actual investment spending, however, partitioned
                // in costs and grabbings.
                g[v].state.spending_cost = cost(v, g);
                g[v].state.spendings_cost += cost(v, g);
                g[v].state.grabbing = grabbing(v, g);
                g[v].state.grabbings += grabbing(v, g);

                // The agent receives just the remaining resources.
                // If there is no resource left, it does not receive any
                // resource.
                if (remaining_resources <=
                    extracted_synergistic_resources_per_agent(v, g)) {
                    g[v].state.payoff = remaining_resources;
                    g[v].state.payoffs += remaining_resources;
                    remaining_resources = 0.;

                    // If the actual investment is positive, it still needs to
                    // be subtracted. It thus is an investment without return.
                    // However, if the actual investment is negative, the
                    // resources is directly extracted from the resource pool.
                    // and does not need to be subtracted from the agent's
                    // resources.
                    if (actual_investment(v, g) > 0.) {
                        g[v].state.payoff -= actual_investment(v, g);
                        g[v].state.payoffs -= actual_investment(v, g);
                    }

                    // Clip the resources to the upper limit of the internal
                    // resources
                    set_and_clip_resources(v, resources(v, g), payoff(v, g));
                }
                else {
                    const auto new_payoff =
                        goods(v, g) - actual_investment(v, g);

                    g[v].state.payoff = new_payoff;
                    g[v].state.payoffs += new_payoff;
                    // Clip the resources to the upper limit of the internal
                    // resources
                    set_and_clip_resources(v, resources(v, g), new_payoff);

                    remaining_resources -=
                        extracted_synergistic_resources_per_agent(v, g);
                }
            }
        }

        return remaining_resources;
    }

    /// Let the population interact to extract resources synergistically
    /** The interaction is based on a generalized version of a continuous Public
     * Goods Game. The process is comparted into the following steps:
     *  1. For each agent calculate the goods share and store it in the vertex
     *     property: goods_per_agent
     *  2. For each agent collect the goods shares from all subgames
     *     she participated in and store the result in the goods of the
     * vertex.
     *  3. For each agent subtract the actual_investments from the goods share
     * to calculate the payoff and store the result in the payoff of the vertex.
     */
    void extract_synergistic_resources()
    {
        // Determine the actual_investments that are equal to the minimum of
        // investment and resources. Using the net investment instead of the
        // investment assures that an agent invests at most the resources
        // it currently has in the interactions.
        apply_rule<IterateOver::vertices, Update::async, Shuffle::off>(
            [](const auto v, auto& g) {
                if (extraction_mode(v, g) == ExtractionMode::synergistic) {
                    g[v].state.actual_investment =
                        calculate_actual_investment(v, g);
                    g[v].state.cooperativity += actual_investment(v, g);
                }
                // ExtractionMode::basic --> no net investment, and no payoff
                else {
                    g[v].state.actual_investment = 0.;
                    g[v].state.spending_cost     = 0.;
                    g[v].state.grabbing          = 0.;
                    g[v].state.payoff            = 0.;
                }
            },
            _g);

        this->_log->debug(
            "Calculate goods shares for all agents, distribute them to "
            "the "
            "node locations, calculate payoffs and add them to the agents' "
            "resources ...");

        apply_rule<IterateOver::vertices, Update::async, Shuffle::off>(
            [this](auto v, auto& g) {
                auto [goods, extracted_resources] =
                    calculate_goods_and_resources_per_agent(
                        this->_resources.synergistic.r,
                        v,
                        g);

                g[v].state.goods_per_agent = goods;
                g[v].state.extracted_synergistic_resources_per_agent =
                    extracted_resources;
            },
            _g);

        apply_rule<IterateOver::vertices, Update::async, Shuffle::off>(
            collect_goods,
            _g);

        _remaining_synergistic_resources =
            calculate_payoffs_and_add_to_resources(_g);
    }

    // -- Linking -----------------------------------------------------------

    /// Add new links to agents
    /** Evaluate the probability to add links within the neighborhood or the
     * whole population and call the correponding link function.
     */
    RuleFunc add_links = [this](const auto v, auto& g) {
        if (this->_distr_unit_interval(*this->_rng) < p_add_local_links(v, g)) {
            add_local_links(v, g);
        }
        else {
            add_global_links(v, g);
        }
    };

    /// Add new links to agents within the neighborhood
    /** Go through all vertices and add links according to their
     * corresponding update rule.
     */
    RuleFunc add_local_links = [this](const auto v, auto& g) {
        // Do nothing if the agent
        // - has reached the desired number of links to other agents
        // - has no neighbors
        if ((boost::out_degree(v, g) > add_link_threshold(v, g)) or
            (boost::out_degree(v, g) == 0)) {
            return;
        }
        // Add one link following the agent's local link addition mode
        else {
            const auto link_mode = link_mode_add_local(v, g);
            constexpr bool sample_within_neighborhood = true;

            change_link(link_mode,
                        this->_link.add.local,
                        v,
                        sample_within_neighborhood,
                        add_link,
                        nb_to_add,
                        g,
                        *this->_rng);
        }
    };

    /// Add new links to agents within the whole population
    RuleFunc add_global_links = [this](const auto v, auto& g) {
        // Do nothing if the agent
        // - has reached the desired number of links to other agents
        if (boost::out_degree(v, g) > add_link_threshold(v, g)) {
            return;
        }
        // Add one link following the agent's add population mode
        else {
            const auto link_mode = link_mode_add_global(v, g);
            constexpr bool sample_within_neighborhood = false;

            change_link(link_mode,
                        this->_link.add.global,
                        v,
                        sample_within_neighborhood,
                        add_link,
                        agent_from_population_to_add_link,
                        g,
                        *this->_rng);
        }
    };

    /// Remove links from agents
    /** Go through all vertices and remove links according to their
     * corresponding update rule.
     */
    RuleFunc remove_links = [this](const auto v, auto& g) {
        const auto threshold = remove_link_threshold(v, g);
        auto num_links       = boost::out_degree(v, g);

        // Do nothing if the agent
        // - has not reached the remove link threshold
        // - has no neighbors
        if ((num_links <= threshold) or (num_links == 0)) {
            return;
        }
        // Remove links until the threshold is reached following the agent's
        // remove mode
        else {
            while (num_links > threshold) {
                const auto link_mode = link_mode_remove(v, g);
                constexpr bool sample_within_neighborhood = true;

                change_link(link_mode,
                            this->_link.remove,
                            v,
                            sample_within_neighborhood,
                            remove_link,
                            nb_to_remove,
                            g,
                            *this->_rng);

                --num_links;
            }
        }
    };

    /// Agents link
    /** First remove links then add links
     */
    void agents_link()
    {
        // Increment link ages
        apply_rule<IterateOver::edges, Update::async, Shuffle::off>(
            [](auto e, auto& g) {
                ++g[e].state.age;
            },
            _g);

        // Remove links dependent on the link mode
        this->_log->debug("Removing agent links ...");
        apply_rule<IterateOver::vertices, Update::async, Shuffle::on>(
            remove_links,
            _g,
            *this->_rng);
        this->_log->debug("Finished removing agent links!");

        // add links dependent on the link mode
        this->_log->debug("Adding agent links ...");
        apply_rule<IterateOver::vertices, Update::async, Shuffle::on>(
            add_links,
            _g,
            *this->_rng);
        this->_log->debug("Finished adding agent links!");

        // Set the agents number of (active) links
        apply_rule<IterateOver::vertices, Update::async, Shuffle::off>(
            [](auto v, auto& g) {
                g[v].state.num_links        = boost::out_degree(v, g);
                g[v].state.num_active_links = calc_num_active_links(v, g);
            },
            _g);
    }

    // -- Evolution -----------------------------------------------------------

    /// Agent die randomly and if their resources is below a threshold
    /** Dead agents are not removed from the Graph but rather just marked
     *  as being dead and all edges from and towards the agents are removed.
     *  There are two processes that lead to dying agents:
     *  (i)     agents die if they do not have enough resources left, thus
     *          if their resources level frops below the threshold
     *  (ii)    agents die randomly with a given probability
     */
    void agents_die()
    {
        using namespace boost;

        // Clear the death states. Internally, they are only cleared
        // if the _clear flag is set beforehand. 
        _death_states.clear();

        graph_traits<Graph>::vertex_iterator vi, vi_end, next;
        tie(vi, vi_end) = vertices(_g);
        for (next = vi; vi != vi_end; vi = next) {
            ++next;

            // Agents die if they do not have resources left,
            // thus if their resources are below a threshold
            if (_g[*vi].state.resources < _evo.death_threshold) {
                // Set the death time and cause of death
                _g[*vi].state.time_death  = this->get_time();
                _g[*vi].state.death_cause = DeathCause::exhaustion;

                // Move the dying agent's state to the _death_states container
                _death_states.add(std::move(_g[*vi].state));

                // Remove all edges to and from the agent and remove the agent
                clear_vertex(*vi, _g);
                remove_vertex(*vi, _g);
            }
            // Agents also die randomly with a specified probability
            else if (_distr_unit_interval(*_rng) < _evo.p_death) {
                // Set the death time
                _g[*vi].state.time_death  = this->get_time();
                _g[*vi].state.death_cause = DeathCause::random;

                // Move the dying agent's state to the _death_states container
                _death_states.add(std::move(_g[*vi].state));

                // Remove all edges to and from the agent and remove the agent
                clear_vertex(*vi, _g);
                remove_vertex(*vi, _g);
            }
        }
    }

    /// The offspring inherits the parent's investment with a small mutation
    /** The mutation is drawn from the uniform real distribution _distr_cots
     */
    std::function<void(VertexDesc, VertexDesc)> inherit_investment =
        [this](const VertexDesc offspring, const VertexDesc parent) {
            auto new_investment = _g[parent].state.investment +
                                  _distr_investment_mutation(*this->_rng);

            // The investment is capped by the maximal resources limit
            // of an agent. Higher investments will not have an effect on the
            // actual investments in the synergistic interaction.
            // They would in principle lead to random mutations that would
            // not
            // be evaluated through selection. However, there are situations
            // in which unlimited investments are selected for indirectly
            // through in case of link_mode::investment.
            new_investment =
                std::min(new_investment, this->_resources_upper_limit);

            // Set negative investments to zero if only positive investments
            // are allowed
            if (not _resources.synergistic.allow_negative_investment) {
                new_investment = std::max(0., new_investment);
            }

            _g[offspring].state.investment     = new_investment;
            _g[offspring].state.old_investment = new_investment;
        };

    // .. Graph manipulation ..................................................
    /// Inherit the desired number of links with a small mutation
    /** The offspring inherits the parent's desired number of links
     *  with a small mutation given by a random integer step
     */
    std::function<void(VertexDesc, VertexDesc)> inherit_add_link_threshold =
        [this](const VertexDesc offspring, const VertexDesc parent) {
            _g[offspring].state.add_link_threshold =
                // First, cast the number to int such that you prevent an
                // underflow yielding wrong results for the clamp operation.
                // Afterward, cast back to an unsigned number.
                static_cast<unsigned>(std::clamp(
                    static_cast<int>(_g[parent].state.add_link_threshold) +
                        _distr_random_int_step(*this->_rng),
                    0,
                    static_cast<int>(this->_link.add.upper_limit)));
        };

    /// Inherit the max number of links with a small mutation
    /** The offspring inherits the parent's max number of links
     *  with a small mutation given by a random integer step
     */
    std::function<void(VertexDesc, VertexDesc)> inherit_remove_link_threshold =
        [this](const VertexDesc offspring, const VertexDesc parent) {
            _g[offspring].state.remove_link_threshold =
                // First cast the number to int such that you prevent an
                // underflow yielding wrong results for the clamp operation.
                // Afterward, cast back to an unsigned number.
                static_cast<unsigned>(std::clamp(
                    static_cast<int>(_g[parent].state.remove_link_threshold) +
                        _distr_random_int_step(*this->_rng),
                    0,
                    static_cast<int>(this->_link.remove.upper_limit)));
        };

    /// Inherit the link mode specifying the mode to add agents in the
    /// neighborhood
    std::function<void(VertexDesc, VertexDesc)> inherit_link_mode_add_local =
        [this](const VertexDesc offspring, const VertexDesc parent) {
            // Mutate with probability p_mode_mutation
            if (_distr_unit_interval(*this->_rng) <
                _link.add.local.p_mode_mutation) {
                _g[offspring].state.link_mode_add_local =
                    static_cast<LinkMode>(_distr_link_mode(*this->_rng));
            }
            else {
                _g[offspring].state.link_mode_add_local =
                    _g[parent].state.link_mode_add_local;
            }
        };

    /// Inherit the link mode specifying the mode to add agents in the
    /// population
    std::function<void(VertexDesc, VertexDesc)> inherit_link_mode_add_global =
        [this](const VertexDesc offspring, const VertexDesc parent) {
            // Mutate with probability p_mode_mutation
            if (_distr_unit_interval(*this->_rng) <
                _link.add.global.p_mode_mutation) {
                _g[offspring].state.link_mode_add_global =
                    static_cast<LinkMode>(_distr_link_mode(*this->_rng));
            }
            else {
                _g[offspring].state.link_mode_add_global =
                    _g[parent].state.link_mode_add_global;
            }
        };

    /// Inherit the link mode specifying the mode to remove agents
    std::function<void(VertexDesc, VertexDesc)> inherit_link_mode_remove =
        [this](const VertexDesc offspring, const VertexDesc parent) {
            // Mutate with probability p_mode_mutation
            if (_distr_unit_interval(*this->_rng) <
                _link.remove.p_mode_mutation) {
                _g[offspring].state.link_mode_remove =
                    static_cast<LinkMode>(_distr_link_mode(*this->_rng));
            }
            else {
                _g[offspring].state.link_mode_remove =
                    _g[parent].state.link_mode_remove;
            }
        };

    /// Add a new agent to the population
    /** The new agent gets a new agent number, is connected to its parent, and
     * receives half of the resources from its parent.
     * Increment the parent's num_offspring counter.
     */
    VertexDesc create_offspring(const VertexDesc parent)
    {
        // Create a new agent, set it's agent number and link it to its parent
        const auto offspring             = boost::add_vertex(_g);
        _g[offspring].state.agent_number = set_new_agent_number();

        // The parent pays a cost to give birth and transfers resources
        // to the offspring
        const double birth_cost =
            _evo.birth_investment + _evo.birth_transferred_resources;
        _g[parent].state.resources -= birth_cost;
        _g[offspring].state.resources = _evo.birth_transferred_resources;
        _g[offspring].state.resources_throughput =
            _evo.birth_transferred_resources;
        _g[parent].state.spending_birth =
            birth_cost + _evo.birth_transferred_resources;
        _g[parent].state.spendings_birth +=
            birth_cost + _evo.birth_transferred_resources;

        // Increase the parent's counter tracking the number of offspring
        ++_g[parent].state.num_offspring;

        // Add a link to the parent. This has to be done after the resources
        // transfer because creating links investments resources. Therefore,
        // resources already needs to have been transferred beforehand.
        // NOTE Creating the first link between offspring and parent does
        //      not require a link formation cost.
        add_link(offspring, parent, 0., 0., _g);

        // -- Inherit mutation properties
        // strength
        _g[offspring].state.strength = std::max(
            0.,
            strength(parent, _g) + _distr_strength_mutation(*this->_rng));

        // Interaction probability
        _g[offspring].state.p_synergistic =
            std::clamp(_g[parent].state.p_synergistic +
                           _distr_interact_mutation(*this->_rng),
                       0.,
                       1.);

        inherit_investment(offspring, parent);
        inherit_add_link_threshold(offspring, parent);
        inherit_remove_link_threshold(offspring, parent);
        _g[offspring].state.p_add_local_links =
            std::clamp(p_add_local_links(parent, _g) +
                           _distr_p_add_local_links_mutation(*this->_rng),
                       0.,
                       1.);
        inherit_link_mode_add_local(offspring, parent);
        inherit_link_mode_add_global(offspring, parent);
        inherit_link_mode_remove(offspring, parent);

        // Set the other agent tracking parameters
        _g[offspring].state.goods_per_agent         = 0.;
        _g[offspring].state.age                     = 0;
        _g[offspring].state.time_birth              = this->get_time();
        _g[offspring].state.death_cause             = DeathCause::NONE;
        _g[offspring].state.num_offspring           = 0;
        _g[offspring].state.num_added_links         = 0;
        _g[offspring].state.num_removed_links       = 0;
        _g[offspring].state.cooperativity           = 0.;
        _g[offspring].state.payoff                  = 0.;
        _g[offspring].state.spendings_living        = 0.;
        _g[offspring].state.spendings_strength      = 0.;
        _g[offspring].state.spendings_cost          = 0.;
        _g[offspring].state.grabbing                = 0.;
        _g[offspring].state.grabbings               = 0.;
        _g[offspring].state.spendings_link_addition = 0.;
        _g[offspring].state.spendings_link_removal  = 0.;
        _g[offspring].state.spendings_birth         = 0.;

        return offspring;
    }

    /// Agents reproduce creating offspring
    /** To reproduce agents need to exceed a reproduction threshold
     *  and then only reproduce with a given probability.
     *  The offspring receives a link to its parent and to other agents
     *  - either random or neighbors of the parent - with given
     *  internal probabiities.
     */
    void agents_reproduce()
    {
        for (const auto v : range<IterateOver::vertices>(_g)) {
            if ((resources(v, _g) > _evo.birth_threshold) and
                (_distr_unit_interval(*this->_rng) < _evo.p_birth)) {
                create_offspring(v);
            }
        }
    }

  public:
    /// Iterate a single step
    /**
     *  Within a single iteration step an agent on a vertex is selected
     *  randomly to die. It's vertex is replaced by an offspring
     *  which inherits the investment of its parent. The parent is selected
     *  fitness dependent among the neighborhood.
     */
    void perform_step()
    {
        extract_resources();

        // Live: Pay a cost of living and a strength cost
        // Increment the age
        apply_rule<IterateOver::vertices, Update::async, Shuffle::off>(
            [this](const auto v, auto& g) {
                // Just subtract the cost of living. Also, being strong
                // costs. NOTE that here, it is not necessary to use the
                // set_and_clip_resources
                //      function because we just subtract a cost and there
                //      is now lower limit for the resources to be clipped.
                g[v].state.resources -= (_cost_of_living + strength(v, g));

                // Store the spendings
                g[v].state.spending_living = _cost_of_living;
                g[v].state.spendings_living += _cost_of_living;
                g[v].state.spending_strength = strength(v, g);
                g[v].state.spendings_strength += strength(v, g);

                // Increase the agent's age
                // This needs to be done at the beginning to avoid undefined
                // division errors during normalization of quantities w.r.t age.
                ++g[v].state.age;
            },
            _g);

        // Let agents link every link_every times
        if (this->_time % _link.link_every == 0) {
            agents_link();
        }

        // Let agents die
        agents_die();

        // Let agents reproduce
        agents_reproduce();

        // After death and birth, need to be classified again
        classify(_g);
    }

    /// Monitor model information
    void monitor()
    {
        using namespace Statistics;

        // Monitor the mean investment and the mean payoff development
        _monitor.set_entry("num_agents", boost::num_vertices(_g));

        _monitor.set_entry("num_links", boost::num_edges(_g));

        _monitor.set_entry("remaining_basic_res", _remaining_basic_resources);

        _monitor.set_entry("remaining_synergistic_res",
                           _remaining_synergistic_resources);

        _monitor.set_entry("strength_mean",
                           mean<IterateOver::vertices>(
                               [](const auto v, auto& g) {
                                   return g[v].state.strength;
                               },
                               _g));
        _monitor.set_entry("investment_mean",
                           mean<IterateOver::vertices>(
                               [](const auto v, auto& g) {
                                   return g[v].state.investment;
                               },
                               _g));
        _monitor.set_entry("resources_mean",
                           mean<IterateOver::vertices>(
                               [](const auto v, auto& g) {
                                   return g[v].state.resources;
                               },
                               _g));
    }

    /// Write data
    /** \note that this function is only called if the write mode is set to
     * manual and not managed.
     */
    void write_data()
    {
        using namespace Statistics;
        using namespace WriteUtils;

        // -- Agent properties ------------------------------------------------
        // Only write vertex and edge properties if there are agents still
        // living
        if (boost::num_vertices(_g) > 0) {
            save_vertex_properties(_g,
                                   _grp_g,
                                   std::to_string(get_time()),
                                   vertex_desc_vals);
            save_edge_properties(_g,
                                 _grp_g,
                                 std::to_string(get_time()),
                                 edge_desc_vals);
        }

        // -- Tracking parameters ---------------------------------------------
        _dset_remaining_basic_resources->write(_remaining_basic_resources);
        _dset_remaining_synergistic_resources->write(
            _remaining_synergistic_resources);

        // -- Statistically evaluated data ------------------------------------
        // Interaction probability
        const auto p_synergistic_mean =
            mean<IterateOver::vertices>(p_synergistic, _g);
        _dset_p_synergistic_mean->write(p_synergistic_mean);

        // investment
        const auto investment_mean =
            mean<IterateOver::vertices>(investment, _g);
        const auto investment_std = std<IterateOver::vertices>(investment, _g);
        _dset_investment_mean->write(investment_mean);
        _dset_investment_std->write(investment_std);

        // Payoff
        const auto payoff_mean = mean<IterateOver::vertices>(payoff, _g);
        const auto payoff_std  = std<IterateOver::vertices>(payoff, _g);
        _dset_payoff_mean->write(payoff_mean);
        _dset_payoff_std->write(payoff_std);

        // Basic resource
        const auto basic_resource_mean =
            mean<IterateOver::vertices>(basic_resource, _g);
        _dset_basic_resource_mean->write(basic_resource_mean);

        // Resources
        const auto resources_mean = mean<IterateOver::vertices>(resources, _g);
        const auto resources_std  = std<IterateOver::vertices>(resources, _g);
        _dset_resources_mean->write(resources_mean);
        _dset_resources_std->write(resources_std);

        // Age
        const auto age_mean = mean<IterateOver::vertices>(age, _g);
        const auto age_std  = std<IterateOver::vertices>(age, _g);
        _dset_age_mean->write(age_mean);
        _dset_age_std->write(age_std);

        // Degree
        const auto num_edges  = boost::num_edges(_g);
        const auto num_agents = boost::num_vertices(_g);
        const auto mean_degree =
            static_cast<double>(num_edges) / num_agents * 2.;
        _dset_degree_mean->write(mean_degree);

        // Number of desired links
        const auto add_link_threshold_mean =
            mean<IterateOver::vertices>(add_link_threshold, _g);
        const auto add_link_threshold_std =
            std<IterateOver::vertices>(add_link_threshold, _g);
        _dset_add_link_threshold_mean->write(add_link_threshold_mean);
        _dset_add_link_threshold_std->write(add_link_threshold_std);

        // Number of maximal links
        const auto remove_link_threshold_mean =
            mean<IterateOver::vertices>(remove_link_threshold, _g);
        const auto remove_link_threshold_std =
            std<IterateOver::vertices>(remove_link_threshold, _g);
        _dset_remove_link_threshold_mean->write(remove_link_threshold_mean);
        _dset_remove_link_threshold_std->write(remove_link_threshold_std);

        // Add neighborhood links mean
        const auto p_add_local_links_mean =
            mean<IterateOver::vertices>(p_add_local_links, _g);
        const auto p_add_local_links_std =
            std<IterateOver::vertices>(p_add_local_links, _g);
        _dset_p_add_local_links_mean->write(p_add_local_links_mean);
        _dset_p_add_local_links_std->write(p_add_local_links_std);

        // Bins: link mechanism: add in neighborhood
        const auto bins_add_in_nbh =
            link_bins<IterateOver::vertices, LinkMechanism::add_local>(_g);
        _dset_bins_add_local->write(std::begin(bins_add_in_nbh),
                                    std::end(bins_add_in_nbh),
                                    [](auto element) {
                                        return element;
                                    });

        // Bins: link mechanism: add in population
        const auto bins_add_in_pop =
            link_bins<IterateOver::vertices, LinkMechanism::add_global>(_g);
        _dset_bins_add_global->write(std::begin(bins_add_in_pop),
                                     std::end(bins_add_in_pop),
                                     [](auto element) {
                                         return element;
                                     });

        // Bins: link mechanism: remove
        const auto bins_remove =
            link_bins<IterateOver::vertices, LinkMechanism::remove>(_g);
        _dset_bins_remove->write(std::begin(bins_remove),
                                 std::end(bins_remove),
                                 [](auto element) {
                                     return element;
                                 });

        // -- Further Quantities ----------------------------------------------
        _dset_num_agents->write(boost::num_vertices(_g));
    }

    // -- Utility -------------------------------------------------------------
    /// Set a flag such that death states are cleared in the next time step.
    void clear_death_states_next_time() noexcept
    {
        _death_states.to_be_cleared();
    }

    // -- Getters and Setters -------------------------------------------------
    /// Get the graph object
    const Graph& get_graph()
    {
        return _g;
    }

    /// Get the remaining basic resources
    double get_remaining_basic_resources() const
    {
        return _remaining_basic_resources;
    }

    /// Get the remaining synergistic resources
    double get_remaining_synergistic_resources() const
    {
        return _remaining_synergistic_resources;
    }

    /// Get the death states
    decltype(auto) get_death_states() const
    {
        return _death_states.get();
    }

};  // namespace Utopia::Models::ReCooDy

}  // namespace Utopia::Models::ReCooDy

#endif  // UTOPIA_MODELS_RECOODY_HH
