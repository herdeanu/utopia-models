#ifndef UTOPIA_MODELS_RECOODY_CLASSIFICATION_HH
#define UTOPIA_MODELS_RECOODY_CLASSIFICATION_HH

#include "adaptors.hh"
#include "statistics.hh"
#include <utopia/core/graph/iterator.hh>

namespace Utopia::Models::ReCooDy
{
/// Classify all agents within a graph
/** Valid classifications are: B(enefactor), (P)rofiteer, (E)xploiter,
 * (M)alefactor.
 */
auto classify = [](auto& g) {
    const auto mean_abs =
        Statistics::mean_abs<IterateOver::vertices>(investment, g);

    for (auto&& v : range<IterateOver::vertices>(g)) {
        const auto i = investment(v, g);

        // Positive investments
        if (not std::signbit(i)) {
            if (i >= mean_abs) {
                g[v].state.classification = Classification::B;
            }
            else {
                g[v].state.classification = Classification::P;
            }
        }
        // Negative investments
        else {
            if (i >= -mean_abs) {
                g[v].state.classification = Classification::E;
            }
            else {
                g[v].state.classification = Classification::M;
            }
        }
    }
};
}  // namespace Utopia::Models::ReCooDy

#endif  // UTOPIA_MODELS_RECOODY_CLASSIFICATION_HH