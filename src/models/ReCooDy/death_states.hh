#ifndef UTOPIA_MODELS_RECOODY_DEATH_STATES_HH
#define UTOPIA_MODELS_RECOODY_DEATH_STATES_HH

#include <utility>

#include "types.hh"

namespace Utopia::Models::ReCooDy
{

/// Store and manage dead agent's states
/** */
class DeathStates
{
  public:
    /// The time type
    using Time = std::size_t;

    /// The container storing dead agent's states type
    using DeathStateContainer = std::vector<AgentState>;
  private:
    /// The container storing the dead states
    DeathStateContainer _death_states;

    /// Whether to clear the death states vector
    bool _clear;

    /// A shared pointer to the model time
    Time& _time;

    /// At which time to start tracking the dead agent's states.
    Time _start_tracking_at;

    /// Track every Nth dead agent's state
    std::size_t _skip_every_n_agents;

    /// Track every counter;
    std::size_t _skip_every_n_agents_counter;

  public:
    /// Constructor
    DeathStates(Time& time, 
                std::size_t start_tracking_at = 0, 
                std::size_t skip_every_n_agents = 0) :
        _clear {false}, _time {time}, _start_tracking_at {start_tracking_at},
        _skip_every_n_agents{skip_every_n_agents}, 
        _skip_every_n_agents_counter{0}
    {
        _death_states.reserve(10000);
    };

    /// Destructor
    ~DeathStates() {};

    /// Add a dead agent's state to the death states
    void add(AgentState&& state)
    {
        if (_time >= _start_tracking_at) {
            if (_skip_every_n_agents_counter == _skip_every_n_agents){
                _death_states.emplace_back(std::forward<AgentState>(state));
                
                _skip_every_n_agents_counter = 0;
            }
            else{
                // increment the track every counter.
                ++_skip_every_n_agents_counter;
            }
        }
    }

    /// Set the flag to clear the death_states container in the next time clear is called
    void to_be_cleared()
    {
        _clear = true;
    }

    /// Clear the death states and reset the _clear flag to false
    void clear()
    {
        if (_clear == true){
            _death_states.clear();
        }
        _clear = false;
    }

    const DeathStateContainer& get() const {
        return _death_states;
    }
};

}  // namespace Utopia::Models::ReCooDy

#endif  // UTOPIA_MODELS_RECOODY_DEATH_STATES_HH