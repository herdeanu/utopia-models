# Default plots for the ReCooDy model
---

mean_expected_investment_evo:
  based_on: 
    - .default_style_and_helpers
    - multiplot
    - dag.options.enable_caching
    - .h0_line
    - style.thesis
    - style.half_size_figure
  select_and_combine: 
    fields: 
      data_raw:
        path: data/ReCooDy/expected_investment_mean
        transform:
          - .isel: [!dag_prev , {}]
  transform:
    # - .mean: [!dag_tag data_raw, [seed]]
    # - .rename: [!dag_prev , 'Mean Investment']
    - .sel: [!dag_tag data_raw, {r: [1.0, 1.01, 1.05, 1.1, 1.15, 1.2]}]
    - .rename: [!dag_prev , 'Mean Expected Investment']
      tag: mean
    - .to_dataframe: [!dag_prev ]
    - callattr: [!dag_prev , reset_index]
      tag: df
  to_plot:
    - function: sns.lineplot
      data: !dag_result df
      err_style: band
  hue: r
  y: Mean Expected Investment
  x: time
  palette: plasma_r
  show_hints: False
  helpers:
    set_labels:
      x: Time
      y: Mean Expected Investment, $\bar{\mu}_i$
    set_legend:
      loc: upper left

violin_investment:
  based_on: 
    - .default_style_and_helpers
    - multiplot
    - style.thesis
    - style.half_size_figure
    - dag.options.enable_caching
    - .h0_line
  creator: multiverse
  select_and_combine: 
    fields: 
      data:
        path: data/ReCooDy/investment_mean
        transform:
          - .isel: [!dag_prev , {time: -1}]
  # transform:
  #   - .mean: [!dag_tag data_raw, {dim: seed}]
  #     tag: mean
  transform:
    # - .isel: [!dag_prev , {r: !slice [1, -1]}] # do not select r=1 and r=1.2. They are huge outliers.
    - .to_dataframe: [!dag_prev ]
    - callattr: [!dag_prev , reset_index]
      tag: df
  to_plot:
    - function: sns.violinplot
      data: !dag_result df
      inner: quartile
      scale: count
      saturation: 1.
      cut: 0
      palette: plasma_r
    # - function: sns.stripplot
    #   data: !dag_result df
    #   size: 2
    #   color: white
  y: investment_mean
  x: r
  show_hints: false
  helpers:
    set_labels:
      y: Final Mean Investment, $\bar{i}^T$
      x: Synergy Factor, $r$
      # y: $\langle i \rangle _T$
      # x: $r$
    set_hv_lines:
      hlines: 
        - pos: 0.
          color: black
          linestyle: '-'
          # alpha: 0.2
          linewidth: 0.5
    set_tick_locators:
      x:
        major: 
          name: AutoLocator

violin_investment_strip:
  based_on: 
    - .default_style_and_helpers
    - multiplot
    - style.thesis
    - style.half_size_figure
    - dag.options.enable_caching
  creator: multiverse
  select_and_combine: 
    fields: 
      data:
        path: data/ReCooDy/investment_mean
        transform:
          - .isel: [!dag_prev , {time: -1}]
  transform:
    - .to_dataframe: [!dag_prev ]
    - callattr: [!dag_prev , reset_index]
      tag: df
  to_plot:
    - function: sns.violinplot
      data: !dag_result df
      inner: quartile
      # inner: ~
      scale: count
      saturation: 1.
      cut: 0
      palette: ~
      color: lightgray
      linewidth: 1.
    - function: sns.stripplot
      data: !dag_result df
      size: 1.
      jitter: 0.15
      # size: 1.5
      # jitter: 0.05
      palette: plasma_r
  y: investment_mean
  x: r
  show_hints: false
  helpers:
    set_labels:
      y: Final Mean Investment, $\bar{i}^T$
      x: Synergy Factor, $r$
      # y: Investment $\bar{i} i \rangle _{P}$
      # x: $r$
    set_hv_lines:
      hlines: 
        - pos: 0.
          color: black
          linestyle: '-'
          # alpha: 0.2
          linewidth: 0.5
    set_tick_locators:
      x:
        major: 
          name: MaxNLocator
          nbins: 5

violin_investment_strip_zoom:
  based_on: violin_investment_strip
  helpers:
    set_limits:
      y: [-42, 42]

violin_expected_investment_strip:
  based_on: 
    - .default_style_and_helpers
    - multiplot
    - style.thesis
    - style.half_size_figure
    - dag.options.enable_caching
  creator: multiverse
  select_and_combine: 
    fields: 
      data:
        path: data/ReCooDy/expected_investment_mean
        transform:
          - .isel: [!dag_prev , {time: -1}]
  transform:
    - .to_dataframe: [!dag_prev ]
    - callattr: [!dag_prev , reset_index]
      tag: df
  to_plot:
    - function: sns.violinplot
      data: !dag_result df
      inner: quartile
      # inner: ~
      scale: count
      saturation: 1.
      cut: 0
      palette: ~
      color: lightgray
      linewidth: 1.
      # bw: 0.1
    - function: sns.stripplot
      data: !dag_result df
      size: 1.
      jitter: 0.15
      # size: 1.5
      # jitter: 0.05
      palette: plasma_r
  y: expected_investment_mean
  x: r
  show_hints: false
  helpers:
    set_labels:
      x: Synergy Factor, $r$
      y: Final Mean Expected Investment, $\bar{\mu}^T_i$
      # y: $\langle \mu _i ^{t=T} \rangle _{P}$
      # x: $r$
    set_hv_lines:
      hlines: 
        - pos: 0.
          color: black
          linestyle: '-'
          # alpha: 0.2
          linewidth: 0.5
    set_tick_locators:
      x:
        major: 
          name: MaxNLocator
          nbins: 5
          
violin_expected_investment_strip_zoom:
  based_on: violin_expected_investment_strip
  helpers:
    set_limits:
      y: [-2.5, 7]


violin_p_synergistic_strip:
  based_on:
    - .default_style_and_helpers 
    - multiplot
    - style.thesis
    - style.half_size_figure
    - dag.options.enable_caching
  creator: multiverse
  select_and_combine: 
    fields: 
      data:
        path: data/ReCooDy/p_synergistic_mean
        transform:
          - .isel: [!dag_prev , {time: -1}]
          - .rename: [!dag_prev , p_synergistic_mean]
  transform:
    - .to_dataframe: [!dag_prev ]
    - callattr: [!dag_prev , reset_index]
      tag: df
  to_plot:
    - function: sns.violinplot
      data: !dag_result df
      inner: quartile
      # inner: ~
      scale: count
      saturation: 1.
      cut: 0
      palette: ~
      color: lightgray
      linewidth: 1.
    - function: sns.stripplot
      data: !dag_result df
      size: 1.
      jitter: 0.15
      # size: 1.5
      # jitter: 0.05
      palette: plasma_r
  y: p_synergistic_mean
  x: r
  show_hints: false
  helpers:
    set_labels:
      y: Final Mean Syn. Prob., $\bar{p}_s$
      x: Synergy Factor, $r$
    set_hv_lines:
      hlines: 
        - pos: 0.
          color: black
          linestyle: '-'
          # alpha: 0.2
          linewidth: 0.5
    set_tick_locators:
      x:
        major: 
          name: MaxNLocator
          nbins: 5


violin_num_agents_strip:
  based_on:
    - .default_style_and_helpers 
    - multiplot
    - style.thesis
    - style.half_size_figure
    - dag.options.enable_caching
  creator: multiverse
  select_and_combine: 
    fields: 
      data:
        path: data/ReCooDy/num_agents
        transform:
          - .isel: [!dag_prev , {time: -1}]
  transform:
    - .to_dataframe: [!dag_prev ]
    - callattr: [!dag_prev , reset_index]
      tag: df
  to_plot:
    - function: sns.violinplot
      data: !dag_result df
      inner: quartile
      # inner: ~
      scale: count
      saturation: 1.
      cut: 0
      palette: ~
      color: lightgray
      linewidth: 1.
    - function: sns.stripplot
      data: !dag_result df
      size: 1.
      jitter: 0.15
      # size: 1.5
      # jitter: 0.05
      palette: plasma_r
  y: num_agents
  x: r
  show_hints: false
  helpers:
    set_labels:
      y: Final Mean Expected Investment $\langle i \rangle$
      x: Synergy Factor $r$
    set_hv_lines:
      hlines: 
        - pos: 0.
          color: black
          linestyle: '-'
          # alpha: 0.2
          linewidth: 0.5
    set_tick_locators:
      x:
        major: 
          name: MaxNLocator
          nbins: 5




# mean_final_investment_facet:
#   based_on: 
#     - .default_style_and_helpers
#     - mv.facet
#     - dag.options.enable_caching
#     - .h0_line
#     - style.thesis
#     # - style.half_size_figure
#   select_and_combine: 
#     fields: 
#       data_raw:
#         path: investment_mean
#         transform:
#           - .isel: [!dag_prev , {}]
#   transform:
#     - .mean: [!dag_tag data_raw, [seed]]
#       tag: mean
#     - .std: [!dag_tag data_raw, [seed]]
#       tag: std
#     - xr.Dataset: 
#       - 'mean': !dag_tag mean
#         'std': !dag_tag std
#       tag: data
#   kind: errorbars
#   use_bands: true
#   y: mean
#   yerr: std
#   hue: r
#   helpers:
#     set_title:
#       title: Investment
#     set_legend:
#       enabled: false

# mean_investment_evo:
#   based_on: 
#     - .default_style_and_helpers
#     - multiplot
#     - dag.options.enable_caching
#     - .h0_line
#     - style.thesis
#     # - style.half_size_figure
#   select_and_combine: 
#     fields: 
#       data_raw:
#         path: data/ReCooDy/investment_mean
#         transform:
#           - .isel: [!dag_prev , {}]
#   transform:
#     # - .mean: [!dag_tag data_raw, [seed]]
#     # - .rename: [!dag_prev , 'Mean Investment']
#     - .rename: [!dag_tag data_raw , 'Mean Investment']
#       tag: mean
#     - .to_dataframe: [!dag_prev ]
#     - callattr: [!dag_prev , reset_index]
#     - print: !dag_prev
#       tag: df
#   to_plot:
#     - function: sns.lineplot
#       data: !dag_result df
#       err_style: ~
#   hue: r
#   y: Mean Investment
#   x: time
#   palette: plasma_r

violin_p_synergistic:
  based_on: 
    - .default_style_and_helpers
    - multiplot
    - style.thesis
    - dag.options.enable_caching
    - .h0_line
  creator: multiverse
  select_and_combine: 
    fields: 
      data:
        path: data/ReCooDy/p_synergistic_mean
        transform:
          - .isel: [!dag_prev , {time: -1}]
          - .rename: [!dag_prev , $p_s$]
          - .rename: [!dag_prev , {p_synergistic_mean: "prob syn"}]
  transform:
    - .to_dataframe: [!dag_prev ]
    - callattr: [!dag_prev , reset_index]
      tag: df
  to_plot:
    - function: sns.violinplot
      data: !dag_result df
      inner: quartile
      scale: count
      saturation: 1.
      cut: 0
      palette: plasma_r
    # - function: sns.stripplot
    #   data: !dag_result df
    #   size: 2
    #   color: white
  y: "prob syn"
  x: r
  show_hints: false
  helpers:
    set_labels:
      # y: Mean Synergistic Interaction Probability $\langle p_s \rangle$
      # x: Synergy Factor $r$
      y: $\langle p_s \rangle _{N^T}$
      x: $r$


violin_link_age:
  based_on: 
    - .default_style_and_helpers
    - multiplot
    - style.thesis
    - dag.options.enable_caching
    - .h0_line
  creator: multiverse
  select_and_combine: 
    fields: 
      data:
        path: data/ReCooDy/link_age_mean
        transform:
          - .isel: [!dag_prev , {time: -1}]
  transform:
    # - .isel: [!dag_prev , {r: !slice [1, -1]}] # do not select r=1 and r=1.2. They are huge outliers.
    - .to_dataframe: [!dag_prev ]
    - callattr: [!dag_prev , reset_index]
      tag: df
  to_plot:
    - function: sns.violinplot
      data: !dag_result df
      inner: quartile
      scale: count
      saturation: 1.
      cut: 0
      palette: plasma_r
    # - function: sns.stripplot
    #   data: !dag_result df
    #   size: 2
    #   color: white
  y: link_age_mean
  x: r
  show_hints: false
  helpers:
    set_labels:
      # y: Mean Link Age $\langle A_l \rangle$
      # x: Synergy Factor $r$
      y: $\langle A_l \rangle _T$
      x: $r$


# connected_components_hist: !pspace
#   based_on: 
#     - .default_style_and_helpers
#     - .dag.multiplot
#     - dag.meta_ops.connected_components
#     - style.thesis
#   creator: multiverse
#   select_and_combine:
#     fields:
#       graph_group: 
#         path: data/ReCooDy/graph
#         transform:
#           - operation: calculate_connected_components
#             args: [!dag_prev ]
#             kwargs: 
#               at_time_idx: -1
#   transform:
#     - .isel: 
#       r: !pdim
#         default: 0
#         range: [21]
#     - .to_dataframe: [!dag_tag graph_group ]
#       tag: df
#   to_plot:
#     - function: sns.histplot
#       data: !dag_result df
#       discrete: true
#   x: connected_components
#   show_hints: false
#   helpers:
#     set_labels:
#       x: "Connected Components"
#     set_scales:
#       x: linear
#       y: log

# connected_components_hist:
#   based_on: 
#     - .default_style_and_helpers
#     - .dag.multiplot
#     - dag.meta_ops.connected_components
#     - style.thesis
#   creator: multiverse
#   select_and_combine:
#     fields:
#       graph_group: 
#         path: data/ReCooDy/graph
#         transform:
#           - operation: calculate_connected_components
#             args: [!dag_prev ]
#             kwargs: 
#               at_time_idx: -1
#   transform:
#     - .to_dataframe: [!dag_tag graph_group ]
#       tag: df
#   to_plot:
#     - function: sns.histplot
#       data: !dag_result df
#       discrete: true
#       hue: r
#       element: step
#   x: connected_components
#   show_hints: false
#   helpers:
#     set_labels:
#       x: "Connected Components"
#     set_scales:
#       x: linear
#       y: log


# violin_num_agents:
#   based_on: 
#     - multiplot
#     - style.thesis
#     - dag.options.enable_caching
#   creator: multiverse
#   select_and_combine: 
#     fields: 
#       data:
#         path: data/ReCooDy/num_agents
#         transform:
#           - .isel: [!dag_prev , {time: -1}]
#   transform:
#    - .isel: [!dag_prev , {r: !slice [1, -1]}] # do not select r=1 and r=1.2. They are huge outliers.
#     - .to_dataframe: [!dag_prev ]
#     - callattr: [!dag_prev , reset_index]
#       tag: df
#   to_plot:
#     - function: sns.violinplot
#       data: !dag_result df
#       inner: quartile
#       scale: count
#       saturation: 1.
#       cut: 0
#       palette: plasma_r
#     # - function: sns.stripplot
#     #   data: !dag_result df
#     #   size: 2
#     #   color: white
#   x: num_agents
#   y: r
#   show_hints: false
#   helpers:
#     set_labels:
#       y: Mean Number of Agents $\langle N \rangle$
#       x: Synergy Factor $r$




# # -- Fast Overview ------------------------------------------------------------
# # states_sel_death:
# #   based_on: [mv.facet, animation_kwargs]
# #   select_and_combine: 
# #     fields: 
# #       data: 
# #         path: death_states
# #         transform:
# #           - .sel: [!dag_prev , {quantity: [investment, strength, payoff]}]
# #   col: quantity
# #   linestyle: ''
# #   marker: .
# #   sharey: col
# #   markersize: 1
# #   alpha: 0.05
# #   frames: seed


# # mean_d_investment:
# #   based_on: [mv.facet, animation_kwargs]
# #   select_and_combine: 
# #     fields: 
# #       data_raw: 
# #         path: death_states
# #         transform:
# #           - .sel: [!dag_prev , {quantity: [investment]}]
# #   transform:
# #     - .mean: [!dag_tag data_raw, {dim: seed}]
# #       tag: mean
# #     - .std: [!dag_tag data_raw, {dim: seed}]
# #       tag: std
# #     - xr.DataArray: [[!dag_tag mean, !dag_tag std]]
# #       tag: data


# # mean_final_investment:
# #   based_on: 
# #     - mv.facet
# #     - .h0_line
# #     - style.thesis
# #     - dag.options.enable_caching
# #   select_and_combine: 
# #     fields: 
# #       data_raw:
# #         path: investment_mean
# #         transform:
# #           - .isel: [!dag_prev , {}]
# #   transform:
# #     - .mean: [!dag_tag data_raw, [seed]]
# #       tag: mean
# #     - .std: [!dag_tag data_raw, [seed]]
# #       tag: std
# #     - xr.Dataset: 
# #       - 'mean': !dag_tag mean
# #         'std': !dag_tag std
# #       tag: data
# #   kind: errorbars
# #   use_bands: true
# #   y: mean
# #   yerr: std
# #   col: r
# #   helpers:
# #     set_title:
# #       title: Investment


# mean_final_p_synergistic:
#   based_on: 
#     - mv.facet
#     - .h0_line
#     - style.thesis
#     - dag.options.enable_caching
#   select_and_combine: 
#     fields: 
#       data_raw:
#         path: p_synergistic_mean
#         transform:
#           - .isel: [!dag_prev , {}]
#   transform:
#     - .mean: [!dag_tag data_raw, [seed]]
#       tag: mean
#     - .std: [!dag_tag data_raw, [seed]]
#       tag: std
#     - xr.Dataset: 
#       - 'mean': !dag_tag mean
#         'std': !dag_tag std
#       tag: data
#   kind: errorbars
#   use_bands: true
#   y: mean
#   yerr: std
#   helpers:
#     set_title:
#       title: Synergistic Interaction Probability



# # -- pcolormesh death state comparissons --------------------------------------
# # .. Settings .................................................................
# .colorbar:
#   add_colorbar: false

# .vminmax:
#   vmin: 0
#   vmax: 200

# .vminmax.zoom:
#   vmin: 0
#   vmax: 10

# .pcolor_encoding:
#   col: seed
#   x: agent
#   cmap: viridis
#   kind: pcolormesh


# investment:
#   based_on: [investments_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: RdBu

# payoff_hue:
#   based_on: [payoff_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# resources_hue:
#   based_on: [resources_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# investment_hue:
#   based_on: [investments_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# actual_investment_hue:
#   based_on: [actual_investments_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# num_agents_hue:
#   based_on: [num_agents_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# num_agents:
#   based_on: [num_agents_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues

# strength_hue:
#   based_on: [strength_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# strength:
#   based_on: [strength_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues

# age_hue:
#   based_on: [age_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# age:
#   based_on: [age_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues

# degree_hue:
#   based_on: [degree_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# degree:
#   based_on: [degree_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues

# extracted_basic_resources_per_agent_hue:
#   based_on: [extracted_basic_resources_per_agent_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# extracted_basic_resources_per_agent:
#   based_on: [extracted_basic_resources_per_agent_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues

# extracted_synergistic_resources_per_agent_hue:
#   based_on: [extracted_synergistic_resources_per_agent_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# extracted_synergistic_resources_per_agent:
#   based_on: [extracted_synergistic_resources_per_agent_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues

# goods_hue:
#   based_on: [goods_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# goods:
#   based_on: [goods_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues

# p_synergistic_hue:
#   based_on: [p_synergistic_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: r

# p_synergistic:
#   based_on: [p_synergistic_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues

# link_mode_add_local_hue:
#   based_on: [link_mode_add_local_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: link_mode_bins
#   col: r
#   col_wrap: 3

# link_mode_add_local:
#   based_on: [link_mode_add_local_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues
#   x: time
#   col: r
#   col_wrap: 7

# link_mode_add_global_hue:
#   based_on: [link_mode_add_global_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: link_mode_bins
#   col: r
#   col_wrap: 3

# link_mode_add_global:
#   based_on: [link_mode_add_global_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues
#   col: r
#   x: time
#   col_wrap: 7

# link_mode_remove_hue:
#   based_on: [link_mode_remove_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   hue: link_mode_bins
#   col: r
#   col_wrap: 3

# link_mode_remove:
#   based_on: [link_mode_remove_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
#   enabled: true
#   cmap: Blues
#   x: time
#   col: r
#   col_wrap: 7

# spendings:
#   based_on: [spendings, animation_kwargs]
#   transform:
#     - operation: xr.concat
#       args:  [[!dag_tag spending_living_mean, 
#                !dag_tag spending_strength_mean, 
#                !dag_tag spending_cost_mean, 
#                !dag_tag spending_link_addition_mean, 
#                !dag_tag spending_link_removal_mean, 
#                !dag_tag spending_birth_mean]]
#       kwargs:
#         dim: spendings
#     - operation: .assign_coords
#       args: [!dag_prev ]
#       kwargs:
#         coords:  
#           spendings:
#             - living
#             - strength
#             - actual investment
#             - link addition
#             - link removal
#             - birth
#       tag: data
#   hue: spendings
#   frames: r

# correlations:
#   based_on: [correlations, animation_kwargs]
#   transform:
#     - operation: xr.merge
#       args:  [[
#               !dag_tag spending_living_mean,
#               !dag_tag age_mean,
#               !dag_tag spending_birth_mean,
#               !dag_tag resources_mean,
#               !dag_tag p_synergistic_mean,
#               !dag_tag basic_resource_mean,
#               !dag_tag strength_mean,
#               !dag_tag spending_strength_mean,
#               !dag_tag extracted_synergistic_resources_per_agent_mean,
#               !dag_tag goods_mean,
#               !dag_tag investment_mean,
#               !dag_tag old_investment_mean,
#               !dag_tag spending_cost_mean,
#               !dag_tag actual_investment_mean,
#               !dag_tag goods_per_agent_mean,
#               !dag_tag payoff_mean,
#               !dag_tag add_link_threshold_mean,
#               !dag_tag remove_link_threshold_mean,
#               !dag_tag spending_link_addition_mean,
#               !dag_tag spending_link_removal_mean,
#               !dag_tag degree_mean, 
#               !dag_tag p_add_local_links_mean]]
#       tag: data_raw
#     - .to_dataframe: !dag_prev
#     - .groupby: [!dag_prev , [r]]
#     - callattr: [!dag_prev , corr]
#     - callattr: [!dag_prev , to_xarray]
#     - .to_array: !dag_prev
#     - callattr: [!dag_prev , assign_coords]
#       kwargs: 
#         variable: !arange [22]
#         level_1: !arange [22]
#       tag: data
#   cmap: RdBu
#   col: r
#   helpers:
#     set_title:
#       title: ~
# # -- Single Quantity Time developments ----------------------------------------
# graph_component_distribution_screenshot:
#   based_on: draw_graph_component_distribution_screenshot

# graph_component_distribution_development_pcolormesh:
#   based_on: draw_graph_component_distribution_development_pcolormesh

# graph:
#   based_on: graph

# graph_components:
#   based_on: graph_components
  
