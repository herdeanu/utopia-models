# Default plots for the ReCooDy model
---

# -- Fast Overview ------------------------------------------------------------
# states_sel_death:
#   based_on: [mv.facet, animation_kwargs]
#   select_and_combine: 
#     fields: 
#       data: 
#         path: death_states
#         transform:
#           - .sel: [!dag_prev , {quantity: [investment, strength, payoff]}]
#   col: quantity
#   linestyle: ''
#   marker: .
#   sharey: col
#   markersize: 1
#   alpha: 0.05
#   frames: seed


# mean_d_investment:
#   based_on: [mv.facet, animation_kwargs]
#   select_and_combine: 
#     fields: 
#       data_raw: 
#         path: death_states
#         transform:
#           - .sel: [!dag_prev , {quantity: [investment]}]
#   transform:
#     - .mean: [!dag_tag data_raw, {dim: seed}]
#     - print: !dag_prev
#       tag: mean
#     - .std: [!dag_tag data_raw, {dim: seed}]
#     - print: !dag_prev
#       tag: std
#     - xr.DataArray: [[!dag_tag mean, !dag_tag std]]
#       tag: data


connected_components_hist:
  based_on: 
    - .default_style_and_helpers
    - .dag.multiplot
    - dag.meta_ops.connected_components
    - style.thesis
    - dag.options.enable_caching
    - style.half_size_figure
  creator: multiverse
  select_and_combine:
    fields:
      cc: 
        path: data/ReCooDy/graph
        transform:
          - operation: calc_cc
            args: [!dag_prev ]
            kwargs: 
              at_time_idx: -1
  transform:
    - .to_dataframe: [!dag_tag cc]
      tag: df_hist
    - operation: fit_powerlaw
      args: [!dag_tag cc]
      kwargs:
        p0: [-3, 8]
    - getitem: [!dag_prev , powerlaw fit]
      tag: fit
    - getattr: [!dag_prev , 'attrs']
    - getitem: [!dag_prev , 'fit_eq']
    - callattr: [!dag_tag fit , rename, !dag_prev ]
    - .to_dataframe: [!dag_prev ]
      tag: df_line
    - getattr: [!dag_tag fit, name]
      tag: label
  to_plot:
    - function: sns.histplot
      data: !dag_result df_hist
      log_scale: true
      multiple: stack
      legend: False
      palette: tab20
      hue: seed
      x: connected component size
      linewidth: 0.
    - function: sns.lineplot
      data: !dag_result df_line
      color: gray
  show_hints: false
  helpers:
    set_labels:
      x: "Connected Component Size"
      y: "Count"
    set_scales:
      y: log
    
  
degree_dist:
  based_on: 
    - .default_style_and_helpers
    - .dag.multiplot
    - style.thesis
    - style.half_size_figure
    - dag.options.enable_caching
  creator: multiverse
  select_and_combine:
    fields:
      degree: 
        path: data/ReCooDy/graph/_edges
        transform:
          - .isel: [!dag_prev , {time: -1}]  # isel also needed to create an xarray object
          - operation: calc_out_degree
            args:  [!dag_prev ]
  transform:
    - .to_dataframe: [!dag_prev ]
      tag: df_hist
    - operation: fit_powerlaw
      args: [!dag_tag degree]
      kwargs:
        p0: [-3, 8]
    - getitem: [!dag_prev , powerlaw fit]
      tag: fit
    - getattr: [!dag_prev , 'attrs']
    - getitem: [!dag_prev , 'fit_eq']
    - callattr: [!dag_tag fit , rename, !dag_prev ]
    - .to_dataframe: [!dag_prev ]
      tag: df_line
    - getattr: [!dag_tag fit, name]
      tag: label
  to_plot:
    - function: sns.histplot
      data: !dag_result df_hist
      log_scale: true
      multiple: stack
      legend: False
      palette: tab20
      hue: seed
      x: out degree
      linewidth: 0.
    - function: sns.lineplot
      data: !dag_result df_line
      color: gray
  show_hints: false
  helpers:
    set_labels:
      y: "Count"
      x: Out-Degree
    set_scales:
      x: log
      y: log

mean_num_agents:
  based_on: 
    - mv.facet
    - dag.options.enable_caching
    - .h0_line
    - style.thesis
    - dag.options.enable_caching
    - style.half_size_figure
  select_and_combine: 
    fields: 
      data_raw:
        path: num_agents
        transform:
          - .isel: [!dag_prev , {}]
  transform:
    - .mean: [!dag_tag data_raw, [seed]]
      tag: mean
    - .std: [!dag_tag data_raw, [seed]]
      tag: std
    - xr.Dataset: 
      - 'mean': !dag_tag mean
        'std': !dag_tag std
      tag: data
  kind: errorbars
  use_bands: true
  y: mean
  yerr: std
  helpers:
    set_labels:
      y: $\bar{N}$
      x: time

mean_final_investment:
  based_on: 
    - mv.facet
    - dag.options.enable_caching
    - .h0_line
    - style.thesis
    - dag.options.enable_caching
    - style.half_size_figure
  select_and_combine: 
    fields: 
      data_raw:
        path: investment_mean
        transform:
          - .isel: [!dag_prev , {}]
  transform:
    - .mean: [!dag_tag data_raw, [seed]]
      tag: mean
    - .std: [!dag_tag data_raw, [seed]]
      tag: std
    - xr.Dataset: 
      - 'mean': !dag_tag mean
        'std': !dag_tag std
      tag: data
  kind: errorbars
  use_bands: true
  y: mean
  yerr: std
  helpers:
    set_labels:
      y: $\langle i \rangle _s$
      x: time


mean_final_investment_sel:
  based_on: 
    - mv.facet
    - dag.options.enable_caching
    - .h0_line
    - style.thesis
    - dag.options.enable_caching
    - style.half_size_figure
  select_and_combine: 
    fields: 
      data_raw:
        path: investment_mean
        transform:
          - .isel: [!dag_prev , {}]
  transform:
    - .sel: [!dag_tag data_raw, {r: [1.0, 1.01, 1.1, 1.2]}]
      tag: data_sel
    - .mean: [!dag_tag data_sel, [seed]]
      tag: mean
    - .std: [!dag_tag data_sel, [seed]]
      tag: std
    - xr.Dataset: 
      - 'mean': !dag_tag mean
        'std': !dag_tag std
      tag: data
  kind: errorbars
  use_bands: true
  y: mean
  yerr: std
  helpers:
    set_labels:
      y: $\langle i \rangle _s$
      x: time

mean_final_p_synergistic:
  based_on: 
    - mv.facet
    - dag.options.enable_caching
    - .h0_line
    - style.thesis
    - dag.options.enable_caching
    - style.half_size_figure
  select_and_combine: 
    fields: 
      data_raw:
        path: p_synergistic_mean
        transform:
          - .isel: [!dag_prev , {}]
  transform:
    - .mean: [!dag_tag data_raw, [seed]]
      tag: mean
    - .std: [!dag_tag data_raw, [seed]]
      tag: std
    - xr.Dataset: 
      - 'mean': !dag_tag mean
        'std': !dag_tag std
      tag: data
  kind: errorbars
  use_bands: true
  y: mean
  yerr: std
  helpers:
    set_labels:
      y: $p_s$
      x: time


mean_final_degree:
  based_on: 
    - mv.facet
    - dag.options.enable_caching
    - .h0_line
    - style.thesis
    - dag.options.enable_caching
    - style.half_size_figure
  select_and_combine: 
    fields: 
      data_raw:
        path: degree_mean
        transform:
          - .isel: [!dag_prev , {}]
  transform:
    - .mean: [!dag_tag data_raw, [seed]]
      tag: mean
    - .std: [!dag_tag data_raw, [seed]]
      tag: std
    - xr.Dataset: 
      - 'mean': !dag_tag mean
        'std': !dag_tag std
      tag: data
  kind: errorbars
  use_bands: true
  y: mean
  yerr: std
  helpers:
    set_labels:
      y: $k_a$
      x: time


# -- pcolormesh death state comparissons --------------------------------------
# .. Settings .................................................................
.colorbar:
  add_colorbar: false

.vminmax:
  vmin: 0
  vmax: 200

.vminmax.zoom:
  vmin: 0
  vmax: 10

.pcolor_encoding:
  col: seed
  x: agent
  cmap: viridis
  kind: pcolormesh


investment:
  based_on: [investments_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: RdBu

payoff_hue:
  based_on: [payoff_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

resources_hue:
  based_on: [resources_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

investment_hue:
  based_on: [investments_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

actual_investment_hue:
  based_on: [actual_investments_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

num_agents_hue:
  based_on: [num_agents_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: seed

num_agents:
  based_on: [num_agents_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues

strength_hue:
  based_on: [strength_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

strength:
  based_on: [strength_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues

age_hue:
  based_on: [age_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

age:
  based_on: [age_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues

degree_hue:
  based_on: [degree_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

degree:
  based_on: [degree_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues

extracted_basic_resources_per_agent_hue:
  based_on: [extracted_basic_resources_per_agent_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

extracted_basic_resources_per_agent:
  based_on: [extracted_basic_resources_per_agent_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues

extracted_synergistic_resources_per_agent_hue:
  based_on: [extracted_synergistic_resources_per_agent_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

extracted_synergistic_resources_per_agent:
  based_on: [extracted_synergistic_resources_per_agent_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues

goods_hue:
  based_on: [goods_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

goods:
  based_on: [goods_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues

p_synergistic_hue:
  based_on: [p_synergistic_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: r

p_synergistic:
  based_on: [p_synergistic_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues

link_mode_add_local_hue:
  based_on: [link_mode_add_local_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: link_mode_bins
  col: r
  col_wrap: 3

link_mode_add_local:
  based_on: [link_mode_add_local_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues
  x: time
  col: r
  col_wrap: 7

link_mode_add_global_hue:
  based_on: [link_mode_add_global_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: link_mode_bins
  col: r
  col_wrap: 3

link_mode_add_global:
  based_on: [link_mode_add_global_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues
  col: r
  x: time
  col_wrap: 7

link_mode_remove_hue:
  based_on: [link_mode_remove_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  hue: link_mode_bins
  col: r
  col_wrap: 3

link_mode_remove:
  based_on: [link_mode_remove_base, style.thesis, style.half_size_figure, dag.options.enable_caching]
  enabled: true
  cmap: Blues
  x: time
  col: r
  col_wrap: 7

spendings:
  based_on: [spendings, animation_kwargs]
  transform:
    - operation: xr.concat
      args:  [[!dag_tag spending_living_mean, 
               !dag_tag spending_strength_mean, 
               !dag_tag spending_cost_mean, 
               !dag_tag spending_link_addition_mean, 
               !dag_tag spending_link_removal_mean, 
               !dag_tag spending_birth_mean]]
      kwargs:
        dim: spendings
    - operation: .assign_coords
      args: [!dag_prev ]
      kwargs:
        coords:  
          spendings:
            - living
            - strength
            - actual investment
            - link addition
            - link removal
            - birth
      tag: data
  hue: spendings
  frames: r

correlations:
  based_on: [correlations, animation_kwargs]
  transform:
    - operation: xr.merge
      args:  [[
              !dag_tag spending_living_mean,
              !dag_tag age_mean,
              !dag_tag spending_birth_mean,
              !dag_tag resources_mean,
              !dag_tag p_synergistic_mean,
              !dag_tag basic_resource_mean,
              !dag_tag strength_mean,
              !dag_tag spending_strength_mean,
              !dag_tag extracted_synergistic_resources_per_agent_mean,
              !dag_tag goods_mean,
              !dag_tag investment_mean,
              !dag_tag old_investment_mean,
              !dag_tag spending_cost_mean,
              !dag_tag actual_investment_mean,
              !dag_tag goods_per_agent_mean,
              !dag_tag payoff_mean,
              !dag_tag add_link_threshold_mean,
              !dag_tag remove_link_threshold_mean,
              !dag_tag spending_link_addition_mean,
              !dag_tag spending_link_removal_mean,
              !dag_tag degree_mean, 
              !dag_tag p_add_local_links_mean]]
      tag: data_raw
    - .to_dataframe: !dag_prev
    - .groupby: [!dag_prev , [r]]
    - callattr: [!dag_prev , corr]
    - callattr: [!dag_prev , to_xarray]
    - .to_array: !dag_prev
    - callattr: [!dag_prev , assign_coords]
      kwargs: 
        variable: !arange [22]
        level_1: !arange [22]
      tag: data
  cmap: RdBu
  col: r
  helpers:
    set_title:
      title: ~
# -- Single Quantity Time developments ----------------------------------------
graph_component_distribution_screenshot:
  based_on: draw_graph_component_distribution_screenshot

graph_component_distribution_development_pcolormesh:
  based_on: draw_graph_component_distribution_development_pcolormesh

graph:
  based_on: graph

graph_components:
  based_on: graph_components
  
