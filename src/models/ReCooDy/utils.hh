#ifndef UTOPIA_MODELS_RECOODY_UTILS_HH
#define UTOPIA_MODELS_RECOODY_UTILS_HH

#include <utopia/core/graph/iterator.hh>
#include <iterator>

namespace Utopia::Models::ReCooDy::Utils
{
namespace impl
{
/** Sample a number of descriptors from graph entity container
 *
 * \tparam Iterator         The iterator type
 * \tparam RNG              The random number generator
 *
 * \param n                 The number of entities to sample
 * \param ref_desc          The reference descriptor specifying iterate_over
 * \param rng               The random number generator
 *
 * \return std::vector<Descriptor> The vector of sampled entities
 */
template<typename Iterator, typename RNG>
decltype(auto)
sample(Iterator it, Iterator it_end, const std::size_t n, RNG&& rng)
{
    using namespace GraphUtils;
    using Container = typename std::vector<
        typename std::iterator_traits<Iterator>::value_type>;

    // Can't sample directly by using the iterators. To make sampling work,
    // create a temporary container in which to copy all descriptors and
    // sample from that container
    Container descriptors(it, it_end);

    Container sample;
    sample.reserve(n);
    std::sample(std::begin(descriptors),
                std::end(descriptors),
                std::back_inserter(sample),
                n,
                rng);

    return sample;
}
}  // namespace impl

/** Sample a number of descriptors from graph entity container
 *
 * \tparam iterate_over Over which graph entity to iterate
 * \tparam Descriptor The descriptor type
 * \tparam Graph The Graph type
 * \tparam RNG The random number generator
 *
 * \param n The number of entities to sample
 * \param ref_desc The reference descriptor specifying iterate_over
 * \param g The Graph
 * \param rng The random number generator
 *
 * \return std::vector<Descriptor> The vector of sampled entities
 */
template<IterateOver iterate_over,
         typename Descriptor,
         typename Graph,
         typename RNG>
decltype(auto) sample(const std::size_t n,
                      const Descriptor ref_desc,
                      const Graph& g,
                      RNG&& rng)
{
    using namespace GraphUtils;

    // Can't sample directly by using the iterators. To make sampling work,
    // create a temporary container in which to copy all descriptors and
    // sample from that container
    auto [it, it_end] = iterator_pair<iterate_over>(ref_desc, g);

    return impl::sample(it, it_end, n, rng);
}

/** Sample a number of descriptors from graph entity container
 *
 * \tparam iterate_over Over which graph entity to iterate
 * \tparam Graph The Graph type
 * \tparam RNG The random number generator
 *
 * \param n The number of entities to sample
 * \param g The Graph
 * \param rng The random number generator
 *
 * \return std::vector<Descriptor> The vector of sampled entities
 */
template<IterateOver iterate_over, typename Graph, typename RNG>
decltype(auto) sample(const std::size_t n, const Graph& g, RNG&& rng)
{
    using namespace GraphUtils;

    // Can't sample directly by using the iterators. To make sampling work,
    // create a temporary container in which to copy all descriptors and
    // sample from that container
    auto [it, it_end] = iterator_pair<iterate_over>(g);

    return impl::sample(it, it_end, n, rng);
}

}  // namespace Utopia::Models::ReCooDy::Utils

#endif  // UTOPIA_MODELS_RECOODY_UTILS_HH