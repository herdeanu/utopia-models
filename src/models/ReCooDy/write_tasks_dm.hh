#ifndef UTOPIA_MODELS_RECOODY_WRITE_TASKS_DM_HH
#define UTOPIA_MODELS_RECOODY_WRITE_TASKS_DM_HH

#include <memory>
#include <string>

#include <utopia/data_io/hdfgroup.hh>
#include <utopia/data_io/graph_utils.hh>

#include "adaptors.hh"
#include "statistics.hh"
#include "death_states.hh"

using namespace Utopia::DataIO;

namespace Utopia::Models::ReCooDy::Write
{
// ++ Helper ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/// Build the base group
auto base_group_builder =
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
    return grp->open_group("/ReCooDy");
};

/// Build the dataset containing vertex properties within a time series group
auto build_dset_vertex_prop_time_series = [](auto& group,
                                             auto& model) -> decltype(auto) {
    return group->open_dataset(std::to_string(model.get_time()),
                               {boost::num_vertices(model.get_graph())});
};

/// Write the attributes for a vertex property dataset
auto write_attr_vertex_prop_dset = [](auto& dset, auto& model) {
    dset->add_attribute("dim_name__0", "agent");
    dset->add_attribute("coords_mode__agent", "linked");
    dset->add_attribute("coords__agent",
                        "../agent_number/" + std::to_string(model.get_time()));
};

/// Build the dataset containing edge properties within a time series group
auto build_dset_edge_prop_time_series = [](auto& group,
                                           auto& model) -> decltype(auto) {
    return group->open_dataset(std::to_string(model.get_time()),
                               {boost::num_edges(model.get_graph())});
};

/// Write the attributes for a edge property dataset
auto write_attr_edge_prop_dset = [](auto& dset, auto&) {
    dset->add_attribute("dim_name__0", "edge_number");
    dset->add_attribute("coords_mode__edge_number", "trivial");
};

// ++ General Write-Outs ++++++++++++++++++++++++++++++++++++++++++++++++++++++

// -- time --------------------------------------------------------------------
/// Task to write out the times
auto time = std::make_tuple(

    // name of the task
    "_times",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dataset, auto& model) {
        dataset->write(model.get_time());
    },

    // builder function
    [](auto& group, auto&) -> decltype(auto) {
        return group->open_dataset("_times");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- remaining basic resources -----------------------------------------------
/// Task to write out the times
auto remaining_basic_resources = std::make_tuple(

    // name of the task
    "remaining_basic_resources",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dataset, auto& model) {
        dataset->write(model.get_remaining_basic_resources());
    },

    // builder function
    [](auto& group, auto&) -> decltype(auto) {
        return group->open_dataset("remaining_basic_resources");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- remaining synergistic resources -----------------------------------------
/// Task to write out the times
auto remaining_synergistic_resources = std::make_tuple(

    // name of the task
    "remaining_synergistic_resources",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dataset, auto& model) {
        dataset->write(model.get_remaining_synergistic_resources());
    },

    // builder function
    [](auto& group, auto&) -> decltype(auto) {
        return group->open_dataset("remaining_synergistic_resources");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// ++ Graph +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// -- num_agents --------------------------------------------------------------
/// Task to write out the times
auto num_agents = std::make_tuple(

    // name of the task
    "num_agents",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dataset, auto& model) {
        dataset->write(boost::num_vertices(model.get_graph()));
    },

    // builder function
    [](auto& group, auto&) -> decltype(auto) {
        return group->open_dataset("num_agents");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- agent_number ------------------------------------------------------------
/// Task to write out the agent_number
auto agent_number = std::make_tuple(

    // name of the task
    "agent_number",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/agent_number");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return g[v].state.agent_number;
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
    },

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "agent");
    });

// -- _vertices ---------------------------------------------------------------
/// Task to write out the vertices
auto _vertices = std::make_tuple(

    // name of the task
    "_vertices",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/_vertices");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return id(v, g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
    },

    // attribute writer for dataset
    [](auto& dset, auto& model) {
        dset->add_attribute("dim_name__0", "agent");
        dset->add_attribute("coords_mode__agent", "linked");
        dset->add_attribute("coords__agent",
                            "../agent_number/" +
                                std::to_string(model.get_time()));
    });

// -- _edges ------------------------------------------------------------------
/// Task to write out the vertices
auto _edges = std::make_tuple(

    // name of the task
    "_edges",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/_edges");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_edges(model.get_graph()) > 0) {
            auto [it, it_end] = GraphUtils::iterator_pair<IterateOver::edges>(
                model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto e) -> std::size_t {
                               return id(boost::source(e, g), g);
                           });

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto e) -> std::size_t {
                               return id(boost::target(e, g), g);
                           });
        }
    },

    // builder function
    [](auto& group, auto& model) -> decltype(auto) {
        return group->open_dataset(std::to_string(model.get_time()),
                                   {2, boost::num_edges(model.get_graph())});
    },

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
    },

    // attribute writer for dataset
    [](auto& dset, [[maybe_unused]] auto& model) {
        dset->add_attribute("dim_name__0", "label");
        dset->add_attribute("coords_mode__label", "values");
        dset->add_attribute("coords__label",
                            std::vector<std::string> {"source", "target"});
        dset->add_attribute("dim_name__1", "edge_idx");
        dset->add_attribute("coords_mode__edge_idx", "trivial");
    });

// -- _edge_ids ---------------------------------------------------------------
/// Task to write out the vertices
auto _edge_ids = std::make_tuple(

    // name of the task
    "_edge_ids",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/_edge_ids");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_edges(model.get_graph()) > 0) {
            auto [it, it_end] = GraphUtils::iterator_pair<IterateOver::edges>(
                model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto e) -> double {
                               return id(e, g);
                           });
        }
    },

    // builder function
    [](auto& group, auto& model) -> decltype(auto) {
        return group->open_dataset(std::to_string(model.get_time()),
                                   {boost::num_edges(model.get_graph())});
    },

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
    },

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "edge_idx");
    });

// -- bins_add_local ------------------------------------
/// Task to write out the bins_add_locals
auto bins_add_local = std::make_tuple(

    // name of the task
    "bins_add_local",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(
            Statistics::link_bins<IterateOver::vertices,
                                  LinkMechanism::add_local>(model.get_graph()));
    },

    // builder function
    [](auto& group, auto& model) -> decltype(auto) {
        return group->open_dataset("bins_add_local",
                                   {model.get_time_max() + 1,
                                    static_cast<std::size_t>(LinkMode::COUNT)});
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("dim_name__1", "link_mode_bins");
        dset->add_attribute("coords_mode__link_mode_bins", "trivial");
    });

// -- bins_add_global --------------------------------------------------
/// Task to write out the bins_add_globals
auto bins_add_global = std::make_tuple(

    // name of the task
    "bins_add_global",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::link_bins<IterateOver::vertices,
                                          LinkMechanism::add_global>(
            model.get_graph()));
    },

    // builder function
    [](auto& group, auto& model) -> decltype(auto) {
        return group->open_dataset("bins_add_global",
                                   {model.get_time_max() + 1,
                                    static_cast<std::size_t>(LinkMode::COUNT)});
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("dim_name__1", "link_mode_bins");
        dset->add_attribute("coords_mode__link_mode_bins", "trivial");
    });

// -- bins_remove -------------------------------------------------------------
/// Task to write out the bins_removes
auto bins_remove = std::make_tuple(

    // name of the task
    "bins_remove",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(
            Statistics::link_bins<IterateOver::vertices, LinkMechanism::remove>(
                model.get_graph()));
    },

    // builder function
    [](auto& group, auto& model) -> decltype(auto) {
        return group->open_dataset("bins_remove",
                                   {model.get_time_max() + 1,
                                    static_cast<std::size_t>(LinkMode::COUNT)});
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("dim_name__1", "link_mode_bins");
        dset->add_attribute("coords_mode__link_mode_bins", "trivial");
    });

// -- bins_BPEM -------------------------------------------------------------
/// Task to write out the bins of strategy classification BPEM
auto bins_BPEM = std::make_tuple(

    // name of the task
    "bins_BPEM",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(
            Statistics::BPEM_bins<IterateOver::vertices>(model.get_graph()));
    },

    // builder function
    [](auto& group, auto& model) -> decltype(auto) {
        return group->open_dataset("bins_BPEM", {model.get_time_max() + 1, 4});
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("dim_name__1", "strategy");
        dset->add_attribute("coords_mode__strategy", "trivial");
    });

// ++ GraphEntity Properties ++++++++++++++++++++++++++++++++++++++++++++++++++
// -- p_synergistic --------------------------------------------------------
/// Task to write out the p_synergistics
auto p_synergistic = std::make_tuple(

    // name of the task
    "p_synergistic",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/p_synergistic");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::p_synergistic(v,
                                                                             g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- investment --------------------------------------------------------------
/// Task to write out the investments
auto investment = std::make_tuple(

    // name of the task
    "investment",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/investment");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::investment(v, g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- actual_investment
// ----------------------------------------------------------
/// Task to write out the actual_investments
auto actual_investment = std::make_tuple(

    // name of the task
    "actual_investment",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/actual_investment");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::actual_investment(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- strength --------------------------------------------------------------
/// Task to write out the strengths
auto strength = std::make_tuple(

    // name of the task
    "strength",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/strength");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::strength(v, g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- p_add_local_links ------------------------------------------------------
/// Task to write out the p_add_local_linkss
auto p_add_local_links = std::make_tuple(

    // name of the task
    "p_add_local_links",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/p_add_local_links");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::p_add_local_links(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- link_mode_add_local --------------------------------------------
/// Task to write out the link_mode_add_locals
auto link_mode_add_local = std::make_tuple(

    // name of the task
    "link_mode_add_local",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/link_mode_add_local");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return static_cast<char>(
                        Utopia::Models::ReCooDy::link_mode_add_local(v, g));
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- link_mode_add_global ----------------------------------------------
/// Task to write out the link_mode_add_globals
auto link_mode_add_global = std::make_tuple(

    // name of the task
    "link_mode_add_global",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/link_mode_add_global");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return static_cast<char>(
                        Utopia::Models::ReCooDy::link_mode_add_global(v, g));
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- link_mode_remove ------------------------------------------------------
/// Task to write out the link_mode_removes
auto link_mode_remove = std::make_tuple(

    // name of the task
    "link_mode_remove",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/link_mode_remove");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return static_cast<char>(
                        Utopia::Models::ReCooDy::link_mode_remove(v, g));
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- add_link_threshold ------------------------------------------------------
/// Task to write out the add_link_thresholds
auto add_link_threshold = std::make_tuple(

    // name of the task
    "add_link_threshold",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/add_link_threshold");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::add_link_threshold(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- remove_link_threshold
// -----------------------------------------------------------
/// Task to write out the remove_link_thresholds
auto remove_link_threshold = std::make_tuple(

    // name of the task
    "remove_link_threshold",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/remove_link_threshold");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::remove_link_threshold(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- goods -------------------------------------------------------------------
/// Task to write out the goods
auto goods = std::make_tuple(

    // name of the task
    "goods",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/goods");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::goods(v, g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- extracted_synergistic_resources_per_agent -------------------------------
/// Task to write out the extracted_synergistic_resources_per_agents
auto extracted_synergistic_resources_per_agent = std::make_tuple(

    // name of the task
    "extracted_synergistic_resources_per_agent",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group(
            "graph/extracted_synergistic_resources_per_agent");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::
                                   extracted_synergistic_resources_per_agent(v,
                                                                             g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- payoff ------------------------------------------------------------------
/// Task to write out the payoffs
auto payoff = std::make_tuple(

    // name of the task
    "payoff",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/payoff");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::payoff(v, g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- basic_resource_extraction -----------------------------------------------
/// Task to write out the basic_resource_extractions
auto basic_resource_extraction = std::make_tuple(

    // name of the task
    "basic_resource_extraction",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/basic_resource_extraction");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::basic_resource(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- resources ---------------------------------------------------------------
/// Task to write out the resourcess
auto resources = std::make_tuple(

    // name of the task
    "resources",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/resources");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::resources(v, g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- age ---------------------------------------------------------------------
/// Task to write out the ages
auto age = std::make_tuple(

    // name of the task
    "age",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/age");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::age(v, g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- extraction_mode ---------------------------------------------------------
/// Task to write out the extraction_modes
auto extraction_mode = std::make_tuple(

    // name of the task
    "extraction_mode",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/extraction_mode");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return static_cast<char>(
                        Utopia::Models::ReCooDy::extraction_mode(v, g));
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- old_investment ----------------------------------------------------------
/// Task to write out the old_investments
auto old_investment = std::make_tuple(

    // name of the task
    "old_investment",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/old_investment");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::old_investment(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- goods_per_agent -------------------------------------------------------
/// Task to write out the goods_per_agents
auto goods_per_agent = std::make_tuple(

    // name of the task
    "goods_per_agent",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/goods_per_agent");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::goods_per_agent(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- spending_living ---------------------------------------------------------
/// Task to write out the goods_per_agents
auto spending_living = std::make_tuple(

    // name of the task
    "spending_living",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/spendings_living");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::spending_living(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- spending_strength -------------------------------------------------------
/// Task to write out the spendings on strength
auto spending_strength = std::make_tuple(

    // name of the task
    "spending_strength",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/spendings_strength");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::spending_strength(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- spending_cost ----------------------------------------------
/// Task to write out the spending on actual investment
auto spending_cost = std::make_tuple(

    // name of the task
    "spending_cost",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/spending_cost");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto v) -> double {
                               return Utopia::Models::ReCooDy::spending_cost(v,
                                                                             g);
                           });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- spending_link_addition ----------------------------------------------
/// Task to write out the goods_per_agents
auto spending_link_addition = std::make_tuple(

    // name of the task
    "spending_link_addition",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/spending_link_addition");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::spending_link_addition(v,
                                                                           g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- spending_link_removal ----------------------------------------------
/// Task to write out the goods_per_agents
auto spending_link_removal = std::make_tuple(

    // name of the task
    "spending_link_removal",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/spending_link_removal");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::spending_link_removal(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// -- spending_birth ----------------------------------------------
/// Task to write out the birth spendings
auto spending_birth = std::make_tuple(

    // name of the task
    "spending_birth",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/spending_birth");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(
                it,
                it_end,
                [g = model.get_graph()](auto v) -> double {
                    return Utopia::Models::ReCooDy::spending_birth(v, g);
                });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// ++ Edge Properties +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// -- link_age ----------------------------------------------------------------
/// Task to write out the link ages
auto link_age = std::make_tuple(

    // name of the task
    "link_age",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/link_age");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] = GraphUtils::iterator_pair<IterateOver::edges>(
                model.get_graph());

            dataset->write(it,
                           it_end,
                           [g = model.get_graph()](auto e) -> double {
                               return Utopia::Models::ReCooDy::link_age(e, g);
                           });
        }
    },

    // builder function
    build_dset_edge_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_edge_property", true);
    },

    // attribute writer for dataset
    write_attr_edge_prop_dset);

// -- death_states ----------------------------------------------
/// Task to write out the death states
auto death_states = std::make_tuple(

    // name of the task
    "death_states",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        const auto& states = model.get_death_states();

        if (not states.empty()) {
            for (const auto& s : states) {
                dset->write(std::vector<double> {
                    static_cast<double>(s.classification),
                    s.p_synergistic,
                    s.investment,
                    s.actual_investment,
                    (s.investment *
                     s.p_synergistic),  // equals expected_investment
                    s.strength,
                    (s.strength *
                     (1. - s.p_synergistic)),  // equals expected strength
                    s.p_add_local_links,
                    static_cast<double>(s.link_mode_add_local),
                    static_cast<double>(s.link_mode_add_global),
                    static_cast<double>(s.link_mode_remove),
                    static_cast<double>(s.add_link_threshold),
                    static_cast<double>(s.remove_link_threshold),
                    static_cast<double>(s.num_added_links),
                    static_cast<double>(s.num_removed_links),
                    static_cast<double>(s.num_links),
                    static_cast<double>(s.num_active_links),
                    s.goods,
                    s.cooperativity,
                    s.payoff,
                    s.payoffs,
                    s.basic_resource,
                    s.basic_resources,
                    s.resources,
                    static_cast<double>(s.agent_number),
                    s.spendings_living,
                    s.spendings_strength,
                    s.spendings_cost,
                    s.spendings_link_addition,
                    s.spendings_link_removal,
                    s.spendings_birth,
                    s.grabbings,
                    static_cast<double>(s.age),
                    static_cast<double>(s.time_birth),
                    static_cast<double>(s.time_death),
                    static_cast<double>(s.death_cause),
                    static_cast<double>(s.num_offspring),
                    static_cast<double>(s.resources_throughput),

                    // Normed quantities
                    (static_cast<double>(s.num_added_links) /
                     static_cast<double>(s.age)),
                    (static_cast<double>(s.num_removed_links) /
                     static_cast<double>(s.age)),
                    (static_cast<double>(s.num_links) /
                     static_cast<double>(s.age)),
                    (static_cast<double>(s.num_active_links) /
                     static_cast<double>(s.age)),
                    (s.goods / static_cast<double>(s.age)),
                    (s.cooperativity / static_cast<double>(s.age)),
                    (s.payoffs / static_cast<double>(s.age)),
                    (s.basic_resources / static_cast<double>(s.age)),
                    (s.spendings_living / static_cast<double>(s.age)),
                    (s.spendings_strength / static_cast<double>(s.age)),
                    (s.spendings_cost / static_cast<double>(s.age)),
                    (s.spendings_link_addition / static_cast<double>(s.age)),
                    (s.spendings_link_removal / static_cast<double>(s.age)),
                    (s.spendings_birth / static_cast<double>(s.age)),
                    (s.grabbings / static_cast<double>(s.age)),
                    (static_cast<double>(s.num_offspring) /
                     static_cast<double>(s.age)),
                    (static_cast<double>(s.resources_throughput) /
                     static_cast<double>(s.age))});
            }

            model.clear_death_states_next_time();
        }
    },

    // builder function
    [](auto& group, auto&) -> decltype(auto) {
        return group->open_dataset("death_states", {H5S_UNLIMITED, 55});
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "agent");
        dset->add_attribute("coords_mode__agent", "trivial");
        dset->add_attribute("dim_name__1", "quantity");
        dset->add_attribute("coords_mode__quantity", "values");
        dset->add_attribute(
            "coords__quantity",
            std::vector<std::string> {"classification",
                                      "p_synergistic",
                                      "investment",
                                      "actual_investment",
                                      "expected_investment",
                                      "strength",
                                      "expected_strength",
                                      "p_add_local_links",
                                      "link_mode_add_local",
                                      "link_mode_add_global",
                                      "link_mode_remove",
                                      "add_link_threshold",
                                      "remove_link_threshold",
                                      "num_added_links",
                                      "num_removed_links",
                                      "num_links",
                                      "num_active_links",
                                      "goods",
                                      "cooperativity",
                                      "payoff",
                                      "payoffs",
                                      "basic_resource",
                                      "basic_resources",
                                      "resources",
                                      "agent_number",
                                      "spendings_living",
                                      "spendings_strength",
                                      "spendings_cost",
                                      "spendings_link_addition",
                                      "spendings_link_removal",
                                      "spendings_birth",
                                      "grabbings",
                                      "age",
                                      "time_birth",
                                      "time_death",
                                      "death_cause",
                                      "num_offspring",
                                      "resources_throughput",

                                      // Normalized quantities
                                      "num_added_links_norm",
                                      "num_removed_links_norm",
                                      "num_links_norm",
                                      "num_active_links_norm",
                                      "goods_norm",
                                      "cooperativity_norm",
                                      "payoffs_norm",
                                      "basic_resources_norm",
                                      "spendings_living_norm",
                                      "spendings_strength_norm",
                                      "spendings_cost_norm",
                                      "spendings_link_addition_norm",
                                      "spendings_link_removal_norm",
                                      "spendings_birth_norm",
                                      "grabbings_norm",
                                      "num_offspring_norm",
                                      "resources_throughput_norm"});
    });

// -- death_states investments ----------------------------------------------
/// Task to write out the death states
auto d_investment = std::make_tuple(

    // name of the task
    "d_investment",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        const auto& states = model.get_death_states();

        if (not states.empty()) {
            for (const auto& s : states) {
                dset->write(s.investment);
            }
        }

        model.clear_death_states_next_time();
    },

    // builder function
    [](auto& group, auto&) -> decltype(auto) {
        return group->open_dataset("d_investment", {H5S_UNLIMITED});
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "agent");
        dset->add_attribute("coords_mode__agent", "trivial");
    });

// -- State classification ----------------------------------------------------
/// Task to write out the agent classification B, P, E, M
auto BPEM = std::make_tuple(
    // name of the task
    "BPEM",

    // basegroup builder
    [](std::shared_ptr<HDFGroup>&& grp) -> std::shared_ptr<HDFGroup> {
        return grp->open_group("graph/BPEM");
    },

    // writer function
    [](auto& dataset, auto& model) {
        if (boost::num_vertices(model.get_graph()) > 0) {
            auto [it, it_end] =
                GraphUtils::iterator_pair<IterateOver::vertices>(
                    model.get_graph());

            dataset->write(it, it_end, [g = model.get_graph()](auto v) -> char {
                return static_cast<char>(
                    Utopia::Models::ReCooDy::classification(v, g));
            });
        }
    },

    // builder function
    build_dset_vertex_prop_time_series,

    // attribute writer for basegroup
    [](auto& grp, auto&) {
        grp->add_attribute("content", "time_series");
        grp->add_attribute("is_vertex_property", true);
    },

    // attribute writer for dataset
    write_attr_vertex_prop_dset);

// ++ Statistics
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// -- link_age_mean
// --------------------------------------------------------------
/// Task to write out the times
auto link_age_mean = std::make_tuple(

    // name of the task
    "link_age_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        const auto mean = Statistics::mean<IterateOver::edges>(
            Utopia::Models::ReCooDy::link_age,
            model.get_graph());

        dset->write(mean);
    },

    // builder function
    [](auto& group, auto&) -> decltype(auto) {
        return group->open_dataset("link_age_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- degree_mean -------------------------------------------------------------
/// Task to write out the times
auto degree_mean = std::make_tuple(

    // name of the task
    "degree_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dataset, auto& model) {
        const double num_edges    = boost::num_edges(model.get_graph());
        const double num_vertices = boost::num_vertices(model.get_graph());

        const double mean_degree = [&]() {
            if (num_vertices != 0.) {
                return static_cast<double>(2. * num_edges) / num_vertices;
            }
            else {
                return 0.;
            }
        }();

        dataset->write(mean_degree);
    },

    // builder function
    [](auto& group, auto&) -> decltype(auto) {
        return group->open_dataset("degree_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- p_synergistic_mean ---------------------------------------------------
/// Task to write out the p_synergistic_means
auto p_synergistic_mean = std::make_tuple(

    // name of the task
    "p_synergistic_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        const auto mean = Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::p_synergistic,
            model.get_graph());

        dset->write(mean);
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("p_synergistic_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- p_synergistic_std ---------------------------------------------------
/// Task to write out the p_synergistic_stds
auto p_synergistic_std = std::make_tuple(

    // name of the task
    "p_synergistic_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::p_synergistic,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("p_synergistic_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- investment_mean ---------------------------------------------------
/// Task to write out the investment_means
auto investment_mean = std::make_tuple(

    // name of the task
    "investment_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::investment,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("investment_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- investment_std ---------------------------------------------------
/// Task to write out the investment_stds
auto investment_std = std::make_tuple(

    // name of the task
    "investment_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::investment,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("investment_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- expected_investment_mean ---------------------------------------------------
/// Task to write out the expected_investment_means
auto expected_investment_mean = std::make_tuple(

    // name of the task
    "expected_investment_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::expected_investment,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("expected_investment_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- expected_investment_std ---------------------------------------------------
/// Task to write out the expected_investment_stds
auto expected_investment_std = std::make_tuple(

    // name of the task
    "expected_investment_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::expected_investment,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("expected_investment_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- actual_investment_mean
// ---------------------------------------------------
/// Task to write out the actual_investment_means
auto actual_investment_mean = std::make_tuple(

    // name of the task
    "actual_investment_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::actual_investment,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("actual_investment_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- actual_investment_std
// ---------------------------------------------------
/// Task to write out the actual_investment_stds
auto actual_investment_std = std::make_tuple(

    // name of the task
    "actual_investment_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::actual_investment,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("actual_investment_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- strength_mean ---------------------------------------------------
/// Task to write out the strength_means
auto strength_mean = std::make_tuple(

    // name of the task
    "strength_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::strength,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("strength_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- strength_std ---------------------------------------------------
/// Task to write out the strength_stds
auto strength_std = std::make_tuple(

    // name of the task
    "strength_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::strength,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("strength_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- p_add_local_links_mean
// ---------------------------------------------------
/// Task to write out the p_add_local_links_means
auto p_add_local_links_mean = std::make_tuple(

    // name of the task
    "p_add_local_links_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::p_add_local_links,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("p_add_local_links_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- p_add_local_links_std
// ---------------------------------------------------
/// Task to write out the p_add_local_links_stds
auto p_add_local_links_std = std::make_tuple(

    // name of the task
    "p_add_local_links_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::p_add_local_links,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("p_add_local_links_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- add_link_threshold_mean
// ---------------------------------------------------
/// Task to write out the add_link_threshold_means
auto add_link_threshold_mean = std::make_tuple(

    // name of the task
    "add_link_threshold_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::add_link_threshold,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("add_link_threshold_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- add_link_threshold_std ---------------------------------------------------
/// Task to write out the add_link_threshold_stds
auto add_link_threshold_std = std::make_tuple(

    // name of the task
    "add_link_threshold_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::add_link_threshold,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("add_link_threshold_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- remove_link_threshold_mean
// ---------------------------------------------------
/// Task to write out the remove_link_threshold_means
auto remove_link_threshold_mean = std::make_tuple(

    // name of the task
    "remove_link_threshold_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::remove_link_threshold,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("remove_link_threshold_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- remove_link_threshold_std
// ---------------------------------------------------
/// Task to write out the remove_link_threshold_stds
auto remove_link_threshold_std = std::make_tuple(

    // name of the task
    "remove_link_threshold_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::remove_link_threshold,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("remove_link_threshold_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- goods_mean ---------------------------------------------------
/// Task to write out the goods_means
auto goods_mean = std::make_tuple(

    // name of the task
    "goods_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::goods,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("goods_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- goods_std ---------------------------------------------------
/// Task to write out the goods_stds
auto goods_std = std::make_tuple(

    // name of the task
    "goods_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::goods,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("goods_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- extracted_synergistic_resources_per_agent_mean
// ---------------------------------------------------
/// Task to write out the extracted_synergistic_resources_per_agent_means
auto extracted_synergistic_resources_per_agent_mean = std::make_tuple(

    // name of the task
    "extracted_synergistic_resources_per_agent_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::extracted_synergistic_resources_per_agent,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset(
            "extracted_synergistic_resources_per_agent_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- extracted_synergistic_resources_per_agent_std
// ---------------------------------------------------
/// Task to write out the extracted_synergistic_resources_per_agent_stds
auto extracted_synergistic_resources_per_agent_std = std::make_tuple(

    // name of the task
    "extracted_synergistic_resources_per_agent_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::extracted_synergistic_resources_per_agent,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset(
            "extracted_synergistic_resources_per_agent_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- payoff_mean ---------------------------------------------------
/// Task to write out the payoff_means
auto payoff_mean = std::make_tuple(

    // name of the task
    "payoff_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::payoff,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("payoff_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- payoff_std ---------------------------------------------------
/// Task to write out the payoff_stds
auto payoff_std = std::make_tuple(

    // name of the task
    "payoff_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::payoff,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("payoff_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- basic_resource_extraction_mean
// ------------------------------------------
/// Task to write out the basic_resource_extraction_means
auto basic_resource_extraction_mean = std::make_tuple(

    // name of the task
    "basic_resource_extraction_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::basic_resource,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("basic_resource_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- basic_resource_extraction_std
// -------------------------------------------
/// Task to write out the basic_resource_extraction_stds
auto basic_resource_extraction_std = std::make_tuple(

    // name of the task
    "basic_resource_extraction_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::basic_resource,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("basic_resource_extraction_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- resources_mean ---------------------------------------------------
/// Task to write out the resources_means
auto resources_mean = std::make_tuple(

    // name of the task
    "resources_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::resources,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("resources_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- resources_std ---------------------------------------------------
/// Task to write out the resources_stds
auto resources_std = std::make_tuple(

    // name of the task
    "resources_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::resources,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("resources_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- age_mean ---------------------------------------------------
/// Task to write out the age_means
auto age_mean = std::make_tuple(

    // name of the task
    "age_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::age,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("age_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- age_std ---------------------------------------------------
/// Task to write out the age_stds
auto age_std = std::make_tuple(

    // name of the task
    "age_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(
            Statistics::std<IterateOver::vertices>(Utopia::Models::ReCooDy::age,
                                                   model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("age_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- old_investment_mean
// ---------------------------------------------------
/// Task to write out the old_investment_means
auto old_investment_mean = std::make_tuple(

    // name of the task
    "old_investment_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::old_investment,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("old_investment_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- old_investment_std ---------------------------------------------------
/// Task to write out the old_investment_stds
auto old_investment_std = std::make_tuple(

    // name of the task
    "old_investment_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::old_investment,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("old_investment_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- goods_per_agent_mean
// ---------------------------------------------------
/// Task to write out the goods_per_agent_means
auto goods_per_agent_mean = std::make_tuple(

    // name of the task
    "goods_per_agent_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::goods_per_agent,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("goods_per_agent_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- goods_per_agent_std
// ---------------------------------------------------
/// Task to write out the goods_per_agent_stds
auto goods_per_agent_std = std::make_tuple(

    // name of the task
    "goods_per_agent_std",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::std<IterateOver::vertices>(
            Utopia::Models::ReCooDy::goods_per_agent,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("goods_per_agent_std");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- spending_living_mean ----------------------------------------------
/// Task to write out the spending_livings
auto spending_living_mean = std::make_tuple(

    // name of the task
    "spending_living_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::spending_living,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("spending_living_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- spending_strength_mean
// ----------------------------------------------------
/// Task to write out the spending_strength_mean
auto spending_strength_mean = std::make_tuple(

    // name of the task
    "spending_strength_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::spending_strength,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("spending_strength_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- spending_cost_mean -----------------------------------------
/// Task to write out the spending_cost_means
auto spending_cost_mean = std::make_tuple(

    // name of the task
    "spending_cost_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::spending_cost,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("spending_cost_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- spending_link_addition_mean
// ---------------------------------------------
/// Task to write out the spending_link_addition_means
auto spending_link_addition_mean = std::make_tuple(

    // name of the task
    "spending_link_addition_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::spending_link_addition,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("spending_link_addition_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- spending_link_removal_mean
// ----------------------------------------------
/// Task to write out the spending_link_removal_means
auto spending_link_removal_mean = std::make_tuple(

    // name of the task
    "spending_link_removal_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::spending_link_removal,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("spending_link_removal_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

// -- spending_birth_mean
// -----------------------------------------------------
/// Task to write out the spending_birth_means
auto spending_birth_mean = std::make_tuple(

    // name of the task
    "spending_birth_mean",

    // basegroup builder
    base_group_builder,

    // writer function
    [](auto& dset, auto& model) {
        dset->write(Statistics::mean<IterateOver::vertices>(
            Utopia::Models::ReCooDy::spending_birth,
            model.get_graph()));
    },

    // builder function
    [](auto& grp, auto&) {
        return grp->open_dataset("spending_birth_mean");
    },

    // attribute writer for basegroup
    [](auto&, auto&) {},

    // attribute writer for dataset
    [](auto& dset, auto&) {
        dset->add_attribute("dim_name__0", "time");
        dset->add_attribute("coords_mode__time", "linked");
        dset->add_attribute("coords__time", "_times");
    });

}  // namespace Utopia::Models::ReCooDy::Write

#endif  // UTOPIA_MODELS_RECOODY_WRITE_TASKS_DM_HH
