#ifndef UTOPIA_MODELS_RECOODY_WRITE_TASKS_HH
#define UTOPIA_MODELS_RECOODY_WRITE_TASKS_HH

#include <tuple>

namespace Utopia::Models::ReCooDy::WriteUtils
{
const auto vertex_desc_vals = std::make_tuple(
    std::make_tuple("_vertices",
                    [](const auto vd, auto& g) {
                        return g[vd].id();
                    }),
    std::make_tuple("age",
                    [](const auto vd, auto& g) {
                        return g[vd].state.age;
                    }),
    std::make_tuple("investment",
                    [](const auto vd, auto& g) {
                        return g[vd].state.investment;
                    }),
    std::make_tuple("resources",
                    [](const auto vd, auto& g) {
                        return g[vd].state.resources;
                    }),
    std::make_tuple("payoff",
                    [](const auto vd, auto& g) {
                        return g[vd].state.payoff;
                    }),
    std::make_tuple("basic_resource",
                    [](const auto vd, auto& g) {
                        return g[vd].state.basic_resource;
                    }),
    std::make_tuple("p_add_local_links",
                    [](const auto vd, auto& g) {
                        return g[vd].state.p_add_local_links;
                    }),
    std::make_tuple("link_mode_add_local",
                    [](const auto vd, auto& g) {
                        return static_cast<char>(
                            g[vd].state.link_mode_add_local);
                    }),
    std::make_tuple("link_mode_add_global",
                    [](const auto vd, auto& g) {
                        return static_cast<char>(
                            g[vd].state.link_mode_add_global);
                    }),
    std::make_tuple("link_mode_remove",
                    [](const auto vd, auto& g) {
                        return static_cast<char>(g[vd].state.link_mode_remove);
                    }),
    std::make_tuple("add_link_threshold",
                    [](const auto vd, auto& g) {
                        return g[vd].state.add_link_threshold;
                    }),
    std::make_tuple("remove_link_threshold", [](const auto vd, auto& g) {
        return g[vd].state.remove_link_threshold;
    }));

const auto edge_desc_vals = std::make_tuple(
    std::make_tuple("_edges",
                    "ids",
                    std::make_tuple("source",
                                    [](auto& ed, auto& g) {
                                        return g[source(ed, g)].id();
                                    }),
                    std::make_tuple("target", [](auto& ed, auto& g) {
                        return g[target(ed, g)].id();
                    })));

}  // namespace Utopia::Models::ReCooDy::WriteUtils

#endif  // UTOPIA_MODELS_RECOODY_WRITE_TASKS_HH