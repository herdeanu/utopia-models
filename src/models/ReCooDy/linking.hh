#ifndef UTOPIA_MODELS_RECOODY_LINKING_HH
#define UTOPIA_MODELS_RECOODY_LINKING_HH

#include <limits>
#include <boost/graph/adjacency_list.hpp>

#include "utopia/core/graph.hh"
#include "adaptors.hh"
#include "utils.hh"
#include "types.hh"

namespace Utopia::Models::ReCooDy
{
// -- Link Addition -----------------------------------------------------------
/// Add a link between source and target
/** If a link is successfully added a link formation cost is subtracted from the
 * source agent as well as the target agent.
 */
auto add_link = [](const auto source,
                   const auto target,
                   const auto cost_source,
                   const auto cost_target,
                   auto& g) {
    // Check whether the edge already exists and only if it does not exist
    // add the edge and subtract the corresponding costs.
    if (not boost::edge(source, target, g).second) {
        const auto [edge, success] = boost::add_edge(source, target, g);
        g[edge].state.age          = 0;

        g[source].state.resources -= cost_source;
        g[target].state.resources -= cost_target;

        g[source].state.spending_link_addition = cost_source;
        g[source].state.spendings_link_addition += cost_source;
        g[target].state.spending_link_addition = cost_target;
        g[target].state.spendings_link_addition += cost_target;

        ++g[source].state.num_added_links;
        ++g[target].state.num_added_links;
    }
};

// -- Link Removal ------------------------------------------------------------
/// Remove a link between source and target
/** If a link is successfully removed a removal cost for the source and target
 * vertex is subtracted from the resources.
 */
auto remove_link = [](const auto source,
                      const auto target,
                      const auto cost_source,
                      const auto cost_target,
                      auto& g) {
    // Get the number of edges
    const auto num_edges = boost::num_edges(g);

    boost::remove_edge(source, target, g);

    // Subtract a cost only if the number of edges has changed
    if (num_edges != boost::num_edges(g)) {
        g[source].state.resources -= cost_source;
        g[target].state.resources -= cost_target;

        g[source].state.spending_link_removal = cost_source;
        g[source].state.spendings_link_removal += cost_source;
        g[target].state.spending_link_removal = cost_target;
        g[target].state.spendings_link_removal += cost_target;

        ++g[source].state.num_removed_links;
        ++g[target].state.num_removed_links;
    };
};

/// Remove one link in graph g from source v with the lowest value returned by
/// the predicate.
auto nb_to_remove = [](const auto v, auto& g, auto&& predicate) {
    // Get the vertex descriptor type
    using VertexDesc = typename boost::graph_traits<
        std::remove_reference_t<decltype(g)>>::vertex_descriptor;

    // Go through the neighborhood and if a neighbor has a lower predicate
    // then the previous neighbor update the nb_to_remove.
    double quantity_tracker  = std::numeric_limits<double>::max();
    VertexDesc to_be_removed = v;

    // It needs to be the neighborhood of the parent
    for (auto nb : range<IterateOver::neighbors>(v, g)) {
        if (predicate(nb, g) < quantity_tracker) {
            to_be_removed = nb;

            // Update the quantity_tracker
            quantity_tracker = predicate(nb, g);
        }
    }

    return to_be_removed;
};

/// Add one link in graph g from source v to target within the next-neighborhood
/// with the highest value returned by the predicate.
/** Note that if no suitable next-neighbor is found the agent itself is
 * returned.
 */
auto nb_to_add = [](const auto v, auto& g, auto&& predicate) {
    // Get the vertex descriptor type
    using VertexDesc = typename boost::graph_traits<
        std::remove_reference_t<decltype(g)>>::vertex_descriptor;

    // Go through the neighborhood and if a neighbor has a lower payoff
    // then the previous neighbor update the nb_to_add.
    double quantity_tracker = std::numeric_limits<double>::lowest();

    // Default initialize the agent as nb_to_add;
    VertexDesc to_add = v;
    // It needs to be the neighborhood of the parent
    for (auto nb : range<IterateOver::neighbors>(v, g)) {
        for (auto nnb : range<IterateOver::neighbors>(nb, g)) {
            if (predicate(nnb, g) > quantity_tracker) {
                // Check that _not_ the vertex is selected
                if (nnb != v) {
                    to_add = nnb;

                    // Update the quantity_tracker
                    quantity_tracker = predicate(nnb, g);
                }
            }
        }
    }

    return to_add;
};

/// Add one link in graph g from source v to a selected target within the whole
/// population with the highest value returned by the predicate.
auto agent_from_population_to_add_link =
    [](const auto, auto& g, auto&& predicate) {
        // Get the vertex descriptor type
        using VertexDesc = typename boost::graph_traits<
            std::remove_reference_t<decltype(g)>>::vertex_descriptor;

        // Go through the population and if a neighbor has a higher payoff
        // then the previous neighbor update the
        // agent_from_population_to_add_link.
        double quantity_tracker = std::numeric_limits<double>::lowest();
        // Initialize with the first agent. The initialization should be
        // irrelevant because the agent is replaced immediately in the for loop
        VertexDesc to_add = *boost::vertices(g).first;
        for (auto high_predicate_agent : range<IterateOver::vertices>(g)) {
            if (predicate(high_predicate_agent, g) > quantity_tracker) {
                to_add = high_predicate_agent;

                // Update the quantity_tracker
                quantity_tracker = predicate(high_predicate_agent, g);
            }
        }

        return to_add;
    };

// -- Link Manipulation -------------------------------------------------------
auto change_link = [](const auto link_mode,
                      const auto& link_mode_params,
                      const auto v,
                      const bool sample_within_neighborhood,
                      auto&& change_link_func,
                      auto&& target_selection_func,
                      auto& g,
                      auto& rng) {
    if (link_mode == LinkMode::random) {
        if (sample_within_neighborhood) {
            const auto sample =
                Utils::sample<IterateOver::neighbors>(1, v, g, rng);

            if (sample.size() > 0) {
                change_link_func(v,
                                 sample[0],
                                 link_mode_params.random.cost_source,
                                 link_mode_params.random.cost_target,
                                 g);
            }
        }
        // Do not sample within the neighborhood
        else {
            const auto sample = Utils::sample<IterateOver::vertices>(1, g, rng);

            if (sample.size() > 0) {
                change_link_func(v,
                                 sample[0],
                                 link_mode_params.random.cost_source,
                                 link_mode_params.random.cost_target,
                                 g);
            }
        }
    }
    else if (link_mode == LinkMode::resources) {
        change_link_func(v,
                         target_selection_func(v, g, resources),
                         link_mode_params.resources.cost_source,
                         link_mode_params.resources.cost_target,
                         g);
    }
    else if (link_mode == LinkMode::p_synergistic) {
        change_link_func(v,
                         target_selection_func(v, g, p_synergistic),
                         link_mode_params.p_synergistic.cost_source,
                         link_mode_params.p_synergistic.cost_target,
                         g);
    }
    else if (link_mode == LinkMode::investment) {
        change_link_func(v,
                         target_selection_func(v, g, investment),
                         link_mode_params.investment.cost_source,
                         link_mode_params.investment.cost_target,
                         g);
    }
    else if (link_mode == LinkMode::strength) {
        change_link_func(v,
                         target_selection_func(v, g, strength),
                         link_mode_params.strength.cost_source,
                         link_mode_params.strength.cost_target,
                         g);
    }
    else if (link_mode == LinkMode::goods) {
        change_link_func(v,
                         target_selection_func(v, g, goods),
                         link_mode_params.goods.cost_source,
                         link_mode_params.goods.cost_target,
                         g);
    }
    else if (link_mode == LinkMode::payoff) {
        change_link_func(v,
                         target_selection_func(v, g, payoff),
                         link_mode_params.payoff.cost_source,
                         link_mode_params.payoff.cost_target,
                         g);
    }
};

}  // namespace Utopia::Models::ReCooDy

#endif  // UTOPIA_MODELS_RECOODY_LINKING_HH
