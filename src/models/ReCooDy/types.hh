#ifndef UTOPIA_MODELS_RECOODY_TYPES_HH
#define UTOPIA_MODELS_RECOODY_TYPES_HH

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/filtered_graph.hpp>

#include <utopia/core/graph.hh>
#include <utopia/core/model.hh>

#include "generators.hh"

namespace Utopia::Models::ReCooDy
{
// ++ Type definitions ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// -- Resource Extraction -----------------------------------------------------
enum class ExtractionMode : bool
{
    basic,
    synergistic
};

// -- Linking ---------------------------------------------------------------

/// The link mode specifying the underlying mechanism specifying linking
enum class LinkMode
{
    NONE,           // 0
    resources,      // 1
    p_synergistic,  // 2
    investment,     // 3
    payoff,         // 4
    random,         // 5
    strength,       // 6
    goods,          // 7
    /// The number of elements in the LinkMode
    COUNT  // 8
    /// The NONE type that should not be counted
};

/// The link mechanism
enum class LinkMechanism
{
    add_local,
    add_global,
    remove
};

/// The agent classification
enum class Classification
{
    B,
    P,
    E,
    M
};

/// The death cause
enum class DeathCause
{
    NONE, 
    random,
    exhaustion
};

// -- Agent -------------------------------------------------------------------

/// Agent struct for ReCooDy model that will be set on a Graph node.
struct AgentState
{
    // .. Evolving properties .................................................

    /// The probability whether to interact or not
    /** If an agent does not interact, it extracts a fixed resources.
     */
    double p_synergistic;

    /// The investment in the continuous public goods game
    double investment;

    /// The net investment actually invested in the continuous PGG
    double actual_investment;

    /// The strength success in basic resource extraction
    double strength;

    /// The probability to add local links
    /** The inverse probability (1-p_add_local_links) equals the
     * probability to add links within the whole population.
     */
    double p_add_local_links;

    /// The link mode for local link addition
    LinkMode link_mode_add_local;

    /// The link mode to add links wihtin the population
    LinkMode link_mode_add_global;

    /// The link mode to remove links
    LinkMode link_mode_remove;

    /// Threshold for link addition
    /** This number specifies whether the agent wants to add a new link to
     * another agent or not. Of the current number of agents is greater or equal
     * to the link adding threshold no additional link is added.
     */
    unsigned add_link_threshold;

    /// The remove link threshold
    /** This number specifies the maximal number of links that an agent accepts
     * as incoming link requests. If another agent wants to create a link to the
     * agent but the agent already has more or equal to remove_link_threshold no
     * new link will be formed. However, if the remove threshold is not reached
     * a new link is created.
     */
    unsigned remove_link_threshold;

    /// The accumulated number of links of an agent
    unsigned num_links;

    /// The number of accumulated active links
    unsigned num_active_links;

    // .. Resulting properties ................................................

    /// The goods generated in the subgame centered around the vertex
    double goods;

    /// The extracted resources through synergistic interaction
    double extracted_synergistic_resources_per_agent;

    /// The cooperativity of the agent
    double cooperativity;

    /// The payoff from the last interactions
    double payoff;

    /// The summed payoffs over a lifetime
    double payoffs;

    /// The basic resource from the last basic extraction
    double basic_resource;

    /// The summed basic resources over a lifetime
    double basic_resources;

    /// The resources of the agent: payoff + resource
    double resources;

    /// The agent's cost of living in the last step
    double spending_living;

    /// The agent's accumulated cost of living
    double spendings_living;

    /// The agent's spending on strength in the last step
    double spending_strength;

    /// The agent's accumulated spending on strength
    double spendings_strength;

    /// The agent's spending on cost in the last step
    double spending_cost;

    /// The agent's accumulated spending on cost
    double spendings_cost;

    /// The agent's spending on link addition in the last step
    double spending_link_addition;

    /// The agent's accumulated spending on link addition
    double spendings_link_addition;

    /// The agent's spending on link removal in the last step
    double spending_link_removal;

    /// The agent's accumulated spending on link removal
    double spendings_link_removal;

    /// The agent's spending on birth in the last step
    /** It consists of the cost_of_birth and the transferred resources to the
     * offspring.
     */
    double spending_birth;

    /// The agent's accumulated spending on birth
    /** It consists of the cost_of_birth and the transferred resources to the
     * offspring.
     */
    double spendings_birth;

    /// The agents accumulated grabbings
    double grabbings;

    /// The agents grabbing in the last step
    double grabbing;

    /// The age of the agent
    std::size_t age;

    /// The birth time step
    std::size_t time_birth;

    /// The death time step
    std::size_t time_death;

    /// The death cause
    DeathCause death_cause;

    /// The number of created offspring
    std::size_t num_offspring;

    /// The number of added links
    std::size_t num_added_links;

    /// The number of removed links
    std::size_t num_removed_links;

    /// The total resources throughput
    /** Every time an agent receives resources, it will also be added to the
     * resources_throughput. So, the resources_throughput is the sum of all
     * incoming resources fluxes.
     * If an agent dies, it's resources is removed from the system, too, so
     * the sum of all resources influxes is removed, thus corresponding to the
     * resources throughput.
     */
    double resources_throughput;

    // .. Helper variables ....................................................

    /// Switch that determines the resource extraction mode
    ExtractionMode extraction_mode;

    /// The agent classification
    Classification classification;

    /// The old (previous) investment enabling the investment changes
    /// calculation
    double old_investment;

    /// The goods_per_agent of the subinteraction centered around this agent
    double goods_per_agent;

    /// The agent number
    std::size_t agent_number;
};

/// Link struct that will be set on the edges.
struct LinkState
{
    /// The age of the link
    std::size_t age;
};

// -- Graph -----------------------------------------------------------------

/// The traits of a vertex are just the traits of a graph entity
using VertexTraits = Utopia::GraphEntityTraits<AgentState>;

/// The traits of an edge are just the traits of a graph entity
using EdgeTraits = Utopia::GraphEntityTraits<LinkState>;

/// An agent is a graph entity with vertex traits
using Agent = GraphEntity<VertexTraits>;

/// A link is a graph entity with edge traits
using Link = GraphEntity<EdgeTraits>;

/// The edge container type
/**
 * \warning Do not change this to boost::setS because it will result in
 *          stochastic simultation outcomes due to the sampling in the
 *          random linking process.
 */
using EdgeContType = boost::listS;

/// Tye vertex container type
using VertexContType = boost::listS;

/// Graph type for the ReCooDy model
using Graph = boost::adjacency_list<EdgeContType,
                                    VertexContType,
                                    boost::undirectedS,
                                    Agent,
                                    Link>;

/// The random number generator to use
using RNG = Utopia::Models::Amee::Generators::Xoroshiro512starstar;

/// Typehelper to define data types of ReCooDy model
/** \note Use WriteMode::manual to increase compilation times roughly by a
 * factor of three.
 */
using ReCooDyTypes = ModelTypes<RNG, WriteMode::managed>;

}  // namespace Utopia::Models::ReCooDy

#endif  // UTOPIA_MODELS_RECOODY_TYPES_HH
