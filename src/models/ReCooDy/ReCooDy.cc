#include <iostream>

#include "ReCooDy.hh"
#include "write_tasks_dm.hh"

#include "generators.hh"

using namespace Utopia::Models::ReCooDy;

int main(int, char** argv)
{
    try {
        // Initialize the PseudoParent from config file path
        Utopia::PseudoParent<RNG> parent(argv[1]);

        // Build the writer args tuple
        const auto writer_args = std::make_tuple(
            Write::num_agents,
            Write::agent_number,
            Write::_vertices,
            Write::_edges,
            Write::_edge_ids,
            // General
            Write::remaining_basic_resources,
            Write::remaining_synergistic_resources,
            Write::time,
            Write::p_synergistic,
            Write::actual_investment,
            Write::investment,
            Write::strength,
            Write::p_add_local_links,
            Write::link_mode_add_local,
            Write::link_mode_add_global,
            Write::link_mode_remove,
            Write::add_link_threshold,
            Write::remove_link_threshold,
            Write::goods,
            Write::extracted_synergistic_resources_per_agent,
            Write::payoff,
            Write::basic_resource_extraction,
            Write::resources,
            Write::age,
            Write::extraction_mode,
            Write::old_investment,
            Write::goods_per_agent,
            Write::spending_living,
            Write::spending_strength,
            Write::spending_cost,
            Write::spending_link_addition,
            Write::spending_link_removal,
            Write::spending_birth,
            Write::d_investment,
            Write::death_states,
            Write::BPEM,
            // Edge-specific
            Write::link_age,
            // Statistics
            Write::degree_mean,
            Write::bins_add_local,
            Write::bins_add_global,
            Write::bins_remove,
            Write::bins_BPEM,
            Write::p_synergistic_mean,
            Write::p_synergistic_std,
            Write::actual_investment_mean,
            Write::actual_investment_std,
            Write::expected_investment_mean,
            Write::expected_investment_std,
            Write::investment_mean,
            Write::investment_std,
            Write::strength_mean,
            Write::strength_std,
            Write::p_add_local_links_mean,
            Write::p_add_local_links_std,
            Write::add_link_threshold_mean,
            Write::add_link_threshold_std,
            Write::remove_link_threshold_mean,
            Write::remove_link_threshold_std,
            Write::goods_mean,
            Write::goods_std,
            Write::extracted_synergistic_resources_per_agent_mean,
            Write::extracted_synergistic_resources_per_agent_std,
            Write::payoff_mean,
            Write::payoff_std,
            Write::basic_resource_extraction_mean,
            Write::basic_resource_extraction_std,
            Write::resources_mean,
            Write::resources_std,
            Write::age_mean,
            Write::age_std,
            Write::old_investment_mean,
            Write::old_investment_std,
            Write::goods_per_agent_mean,
            Write::goods_per_agent_std,
            Write::spending_living_mean,
            Write::spending_strength_mean,
            Write::spending_cost_mean,
            Write::spending_link_addition_mean,
            Write::spending_link_removal_mean,
            Write::spending_birth_mean,
            // Link states
            Write::link_age_mean);

        // Initialize the main model instance
        ReCooDy model("ReCooDy", parent, {}, writer_args);
        model.run();

        return 0;
    }
    catch (Utopia::Exception& e) {
        return Utopia::handle_exception(e);
    }
    catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
    catch (...) {
        std::cerr << "Exception occurred!" << std::endl;
        return 1;
    }
}
