"""Tests of the linking mechanism of the ReCooDy model"""

import numpy as np

import pytest

from utopya.testtools import ModelTest

# Configure the ModelTest class
mtc = ModelTest("ReCooDy", test_file=__file__)


# Fixtures --------------------------------------------------------------------
# Define fixtures


# Tests -----------------------------------------------------------------------
def test_link_bins():
    """Test that the bins collecting link modes sum up to the number of agents
    """
    _, dm = mtc.create_run_load(from_cfg="link_bins.yml",
                                perform_sweep=False)

    for _, uni in dm['multiverse'].items():
        # Get the data and configuration
        data = uni['data/ReCooDy']

        # Check that the number of agents does not change
        num_agents = data['num_agents']

        bins_add_local = data['bins_add_local']
        assert (bins_add_local.sum(
            dim='link_mode_bins') == num_agents).all()
