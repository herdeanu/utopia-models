"""Tests of the interaction output of the ReCooDy model"""

import numpy as np

import pytest

from utopya.testtools import ModelTest

# Configure the ModelTest class
mtc = ModelTest("ReCooDy", test_file=__file__)

# Fixtures --------------------------------------------------------------------
# Define fixtures


# Tests -----------------------------------------------------------------------
def test_macroscopic_investment_payoff_relation():
    """Test the correct interation results on ReCooDy networks.
    Fortunately, in the case that the investments are normalized by the number of
    players participating in one sub-interaction, it is easily possible
    to check for the correct interaction results by a macroscopic relation.
    This relation is that the sum of all payoffs is equal to the sum of all
    investments multiplied by the synergy factor minus 1.
    This, however, is only true if payoffs are not accumulated. In this case,
    it is only valid in the first time step. This is tested here.
    It can be easily derived, as done in the master thesis of Herdeanu (2018)
    and is valid on all network types.
    """
    mv, dm = mtc.create_run_load(from_cfg="investment_payoff_relation.yml",
                                 perform_sweep=True)

    for uni_no, uni in dm['multiverse'].items():
        # Get the data
        data = uni['data/ReCooDy/graph']
        investment = data['investment']
        actual_investment = data['actual_investment']
        payoff = data['payoff']
        goods = data['goods']

        # Get the synergy factor
        r = uni['cfg']['ReCooDy']['resources']['synergistic']['r']

        ### Check that the payoff-investment relation holds for all times
        for t, p in payoff.sel().groupby("time"):
            p_sum = p.sum()
            i_sum = investment.isel(time=t).sum()
            g_sum = goods.isel(time=t).sum()

            assert (p_sum - i_sum * (r - 1.)) < 1e-6
            assert (p_sum - g_sum - i_sum ) < 1e-6


        ### Check that tgEhe extracted energy is >= the goods per agent 
        goods_p_a = data['goods_per_agent']
        j_p_a = data['extracted_synergistic_resources_per_agent']
        # NOTE .sel() is needed to transform the TimeSeriesGroup into an
        #       xr.Dataarray. Also, combining time data results in
        #       many nans that need to be replaced by zeroes.
        assert ((j_p_a.sel() - goods_p_a.sel()).fillna(0.) < 1e-6).all()


def test_non_negative_investments():
    """Tests that (net and normal) investments never get negative."""
    mv, dm = mtc.create_run_load(from_cfg="negative_investments.yml",
                                 perform_sweep=True)

    for uni_no, uni in dm['multiverse'].items():
        # Get the data
        data = uni['data/ReCooDy']
        cfg = uni['cfg']['ReCooDy']
        mean_investment = data['investment_mean']
        mean_actual_investment = data['actual_investment_mean']
        allow_negative_investment = cfg['resources']['synergistic']['allow_negative_investment']

        # Assert that the mean investment is always greater than zero
        if allow_negative_investment:
            # There are negative as well as positive investments
            assert (mean_investment < 0.).any()
            assert (mean_investment >= 0.).any()
            assert (mean_actual_investment < 0.).any()
            assert (mean_actual_investment >= 0.).any()
        else:
            # All investments are positive
            assert (mean_investment >= 0.).all()
            assert (mean_actual_investment >= 0.).all()


def test_actual_investments():
    """Tests that the actual_investments are calculated correctly."""
    mv, dm = mtc.create_run_load(from_cfg="actual_investments.yml",
                                 perform_sweep=True)

    for uni_no, uni in dm['multiverse'].items():
        # Get the data and configuration
        data = uni['data/ReCooDy']
        actual_investment = data['graph/actual_investment']
        investment = data['graph/investment']

        # Assert that the actual_investment is either smaller or equal to the
        # investment or zero.
        # NOTE this can be made more performant
        for net_i_t, i_t in zip(actual_investment, investment):
            for net_i, i in zip(actual_investment[net_i_t], investment[i_t]):
                assert (net_i <= i) or (net_i == 0)
