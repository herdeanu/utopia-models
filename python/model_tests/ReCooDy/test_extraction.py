"""Tests of the linking mechanism of the ReCooDy model"""

import numpy as np

import pytest

from utopya.testtools import ModelTest

# Configure the ModelTest class
mtc = ModelTest("ReCooDy", test_file=__file__)


# Fixtures --------------------------------------------------------------------
# Define fixtures


# Tests -----------------------------------------------------------------------
def test_resource_conservation():
    """Test that the energy throughput through all agents is not greater
    than the sum of extracted resources"""

    _, dm = mtc.create_run_load(from_cfg="resource_conservation.yml",
                                perform_sweep=True)

    for _, uni in dm['multiverse'].items():
        # Check that the extracted resources equal the obtained resources
        data = uni['data/ReCooDy']
        cfg = uni['cfg/ReCooDy']
        cfg_res = cfg['resources']

        # Calculate the extracted basic resources
        b_amount = cfg_res['basic']['amount']
        b_remaining = data["remaining_basic_resources"].isel(time=slice(1, None))
        b_extracted_global = (b_amount - b_remaining).sum().rename('b_extracted_global')
        b_extracted_agents = data['graph/basic_resource_extraction'].isel(time=slice(1, None)).sum(dim='agent')

        assert (b_remaining >= 0.).all()
        assert (b_remaining <= b_amount).all()

        # The extracted resources from the global amount should be larger or 
        # equal to the summed agent basic resources.
        # Normally, they should be equal, but agents die and if they do so,
        # their state is deleted. Thus, they do not contribute a basic_resource.
        assert (b_extracted_global >= b_extracted_agents).all()

        s_amount = cfg_res['synergistic']['amount']
        s_remaining = data["remaining_synergistic_resources"].isel(time=slice(1, None))
        s_extracted_global = (s_amount - s_remaining).sum().rename('s_extracted_global')

        assert (s_remaining >= 0.).all()
        assert (s_extracted_global).all() >= 0.

        # The total resource throughput of the death agents should be smaller
        # or equal to the extracted resource. Smaller because there are still
        # living agents for which the death_states are not yet written out.
        # NOTE: This test is diabled because the resource throughput 
        #       also entails subtracted resources during the synergistic 
        #       interactions, e.g. through negative costs or negative good
        #       shares. These leads to the test failing.
        #       If at some point, only positive additions to the energy 
        #       throughput are added, this test can be enabled again.
        # extracted = (s_extracted_global + b_extracted_global).sum()
        # init_resources = data['graph/resources'].isel(time=0).sum()
        # throughput = data['death_states'].sel(quantity="resources_throughput").sum()
        # extracted_and_initial = (extracted + init_resources)
        # assert throughput <= extracted_and_initial


def test_extraction():
    """Test that the resource extraction works correctly
    """
    _, dm = mtc.create_run_load(from_cfg="extraction.yml",
                                perform_sweep=False)

    for _, uni in dm['multiverse'].items():
        # Get the data and configuration
        data = uni['data/ReCooDy']
        cfg = uni['cfg/ReCooDy']

        # -- Test: Get either payoff _or_ basic_resource_extraction
        # Get the payoff and the basic resource
        payoff = data['graph/payoff']
        basic_resource_extraction = data['graph/basic_resource_extraction']

        # NOTE This test can be improved performance wise by not using raw
        #      loops.
        for b, p in zip(basic_resource_extraction.values(), payoff.values()):
            for be, pe in zip(b, p):
                assert (be == 0) or (pe == 0)

        # -- Test: Check that the remaining resources are always smaller than
        #          the resource reservoir.
        remaining_resource_b = data['remaining_basic_resources']
        remaining_resource_s = data['remaining_synergistic_resources']
        resource_reservoir_b = cfg['resources']['basic']['amount']
        resource_reservoir_s = cfg['resources']['synergistic']['amount']

        assert (remaining_resource_b <= resource_reservoir_b).all()
        assert (remaining_resource_s <= resource_reservoir_s).all()


def test_resources_cycle():
    """Test that the number of agents does not exceed the theoretically maximal
    resources inflow after a few generations.
    """
    _, dm = mtc.create_run_load(from_cfg="resources_cycle.yml",
                                perform_sweep=False)

    for _, uni in dm['multiverse'].items():
        # Get the data and configuration
        data = uni['data/ReCooDy']
        cfg = uni['cfg/ReCooDy']
        cfg_res = cfg['resources']

        sum_resources_inflow = cfg_res['basic']['amount'] + \
            cfg_res['synergistic']['amount']

        # Estimate the bare minimum of resources outflow defined by the sum of
        # all cost of livings or the agents. This estimate of course neglects
        # other costs such as the cost of link formation, etc. However, it is
        # useful to catch resources-flow leaks
        cost_of_living = cfg['cost_of_living']
        num_agents = data['num_agents']
        sum_resources_outflow = np.multiply(num_agents, cost_of_living)

        # Note that write start is set to 100, so in the given parameter
        # configuration the spin-up phase should be already surpassed.
        assert (sum_resources_outflow <= sum_resources_inflow).all()


def test_basic_resource():
    """Test that the basic resource extraction works as expected."""

    _, dm = mtc.create_run_load(from_cfg="basic_resource_extraction.yml",
                                perform_sweep=False)

    for _, uni in dm['multiverse'].items():
        # Get the data and configuration
        data = uni['data/ReCooDy']
        cfg = uni['cfg/ReCooDy']
        cfg_res = cfg['resources']
        amount = cfg_res['basic']['amount']

        # Get the resources and skip the initial state because there the resources
        # are all zero
        resource = data['graph/basic_resource_extraction'].isel(
            time=slice(1, None))
        for r in resource:
            # The sum of extracted resources does not exceed the amount of resources
            assert r.sum() <= amount

            # 10 agents extract 1 unit of resource and
            # 10 agents extract 0 units
            unique, counts = np.unique(r.data, return_counts=True)
            assert 0 in unique
            assert 1 in unique
            assert unique.size == 2
            assert counts[0] == 10
            assert counts[1] == 10