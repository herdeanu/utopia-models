"""Plots that are specific to the ReCooDy model"""

# Make them all available here to allow easier import
from .graph import *
from .time_series import *
from .snsplot import *
from .pcolormesh_ds import *

from utopya.plotting import register_operation
from .operations import *

register_operation(name="calc_hist2D", func=calc_hist2D)
register_operation(name="calc_hist2D_data_ready", func=calc_hist2D)

register_operation(name="where_bigger", func=where_bigger)

register_operation(name="calc_cc", func=calc_cc)
register_operation(name="fit_powerlaw", func=fit_powerlaw)


register_operation(name="calc_out_degree", func=calc_out_degree)
register_operation(name="calc_final_out_degree", func=calc_final_out_degree)