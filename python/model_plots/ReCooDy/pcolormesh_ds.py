"""Implements the 2d histogram plot function"""

import copy
import logging
from typing import Union, Sequence, Tuple

import xarray as xr
from matplotlib.pyplot import pcolormesh

from dantro.plot_creators.ext_funcs.generic import make_facet_grid_plot

from utopya.plotting import is_plot_func, PlotHelper


# Local variables
log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------

@make_facet_grid_plot(
    map_as="dataset",
    encodings=("x", "y", "hue"),
    supported_hue_styles=("discrete", "continuous"),
    #
    # defaults
    hue_style="continuous",  # needs to be set, doesn't work otherwise
    add_guide=False,
)
def pcolormesh_ds(
    ds: xr.Dataset,
    *,
    hlpr: PlotHelper,
    _is_facetgrid: bool,
    x: str,
    y: str = None,
    hue: str = None,
    **kwargs
) -> None:
    """Plots value spans, i.e.: lines that go from ``x0`` to ``x1``, located
    at a constant value ``y`` and (optionally) with a ``hue``.

    This plot function expects the ``data`` tag being available, being an
    xr.Dataset, and containing data variables that match the names given in the
    ``x``, ``x_end``, ``y`` and ``hue`` arguments.

    Args:
        ds (xr.Dataset): The dataset containing the variables specified in the
            arguments ``x``, ``x_end``, ``y`` and ``hue``.
        hlpr (PlotHelper): The PlotHelper
        _is_facetgrid (bool): Whether this is part of a facet grid iteration
        x (str): Data variable containing beginnings of lines
        x_end (str): Data variable containing ends of lines
        y (str, optional): data varibale to use for y positions. If this is not
            available upon the data variables, uses dimension coordinates.
        y_fstr (str, optional): Format string to use for labels; available
            keys are: ``y``, ``y_coord``
        hue (str, optional): Data variable to use for line color
        min_length (float, optional): If given, will extend the end coordinate
            of each line segments to ``(start + min_length)`` in case the
            segment would otherwise be shorter than the specified length.
            NOTE That this distorts the actual value of the elements and should
            only be used for making very short segments visible.
        use_line_collection (Union[bool, int], optional): Whether to use a line
            collection or (if an integer) *above* which number of lines to use
            a line collection instead of a labelled line. If this is set, there
            will be no labels and ``y_fstr`` is ignored.
        **kwargs: Passed on to ``hlpr.ax.plot`` or ``LineCollection``.

    Raises:
        ValueError: On bad data dimensionality
    """
    # Get the x-data
    x_data = ds.data_vars[x]
    
    # Determine the y dimension automatically, if needed
    y = y if y else x_data.dims[0]

    # Set hue to y, if not given
    hue = hue if hue else y

    # Get y data from data variables or (alternatively: coordinates)
    if y in ds.data_vars:
        y_data = ds.data_vars[y]
        log.debug("Using data variable '%s' for y-data ...", y)
    else:
        y_data = ds.coords[y]
        log.debug("Using coordinates of dimension '%s' for y-data ...", y)

    if y_data.ndim != 1:
        raise ValueError(f"Expected 1D y-data, got:\n\n{y_data}")

    # # Set some plot aesthetics
    # if not _is_facetgrid:
    #     hlpr.provide_defaults("set_labels", x=f"{x0} & {x1}", y=y)

    #     if not use_lc:
    #         hlpr.provide_defaults("set_legend", title=y)

    # .. Draw lines now .......................................................
    hlpr.ax.pcolormesh(x, y, hue, **kwargs)

    # Need to autoscale, because `add_collection` doesn't do it.
    hlpr.ax.autoscale_view()

    # hlpr.track_handles_labels(handles, labels)
    # # NOTE If supporting hue, will need to make the labels contain both the
    # #      y and hue coordinates!
