import numpy as np
import xarray as xr
import pandas as pd
import networkx as nx
from scipy.optimize import curve_fit

from typing import Union


def calc_hist2D_general(data,
                        quantity: Union[str, list, dict] = None,
                        *,
                        xbins: int = 100,
                        ybins: int = 100,
                        threshold: float = 0.,
                        log: bool = False):
    """Calculate a 2D histogram. 
    
    Bin the agents into xbins groups. 
    """
    # Wrap np.histogram to just return the bin counts
    hist_func = lambda *a, **k: np.histogram(*a, **k)[0]
    hist_bin_edges = lambda *a, **k: np.histogram_bin_edges(*a, **k)

    # Select the quantities for which to calculate the histogram and transform
    # the dataArray to a dataset to conveniently apply xr.apply_ufunc.
    data = data.sel(quantity=quantity).to_dataset(dim='quantity')

    # if isinstance(ybins, int):
    #     ybins = np.linspace(data.to_array().min(), data.to_array().max(), ybins)
    # elif isinstance(ybins, list):
    #     ybins = np.linspace(ybins[0], ybins[1], ybins[2])

    # Bin the agents into groups and calculate the np.histogram for each group.
    hists = []
    agent_coords = []
    for x_bin, g in data.groupby_bins('agent', xbins):
        bin_edges = xr.apply_ufunc(hist_bin_edges,
                                   g,
                                   kwargs={'bins': ybins},
                                   input_core_dims=[['agent']],
                                   output_core_dims=[['quantity']],
                                   join='exact',
                                   dataset_fill_value=np.nan)

        # Unfortunately, it is necessary to recalculate the ybins here
        # because we cannot pass coordinate-specific kwargs to the apply_ufunc
        # method.
        hist_da = xr.apply_ufunc(hist_func,
                                 g,
                                 kwargs={'bins': ybins},
                                 input_core_dims=[['agent']],
                                 output_core_dims=[['quantity']],
                                 join='exact',
                                 dataset_fill_value=np.nan)

        # Calculate the mid points of the bin edges.
        # Note that multiplying 0.5 is faster than dividing by 2.
        bin_edges = 0.5 * (bin_edges.isel(quantity=slice(1, None)) +
                           bin_edges.isel(quantity=slice(None, -1)))

        hist_da = hist_da.assign_coords({
            "investments": bin_edges['investment'],
            "strengths": bin_edges['strength']
        })
        hists.append(hist_da)

        # Use the middle agent as agent coordinate
        agent_coords.append(x_bin.mid)

    # Gather all histograms and assign the coordinates to the new agent dim
    # and the quantity mid-point.
    d = xr.concat(hists, dim='agent')
    # print(d)
    # d = xr.Dataset({'investment': d['investment'], 'strength': d['strength'] })

    d = d.assign_coords({"agent":
                         agent_coords})  # , "quantity": quantity_coords})

    # print(d)
    # print(d['quantity'])

    # Only display values above a given threshold and transform back to a
    # xr.dataArray to be able to plot a pcolormesh.
    d = d.where(d > threshold)

    # d = xr.concat([d['investment'], d['strength']], dim='quantity')
    print(d)

    if (log):
        d = np.log10(d)

    return d


def calc_hist2D(data,
                quantity: Union[str, list] = None,
                *,
                xbins: int = 100,
                ybins: int = 100,
                threshold: float = 0.,
                log: bool = False):
    """Calculate a 2D histogram. 
    
    Bin the agents into xbins groups. 
    """
    # Wrap np.histogram to just return the bin counts
    hist_func = lambda *a, **k: np.histogram(*a, **k)[0]

    # Select the quantities for which to calculate the histogram and transform
    # the dataArray to a dataset to conveniently apply xr.apply_ufunc.
    try:
        data = data.sel(quantity=quantity, drop=True)
    except:
        if quantity == 'expected_investment':
            data = data.sel(quantity='investment', drop=True) * data.sel(
                quantity='p_synergistic', drop=True)
        elif quantity == 'expected_strength':
            data = data.sel(quantity='strength', drop=True) * (
                1. - data.sel(quantity='p_synergistic', drop=True))

    if isinstance(ybins, int):
        ybins = np.linspace(data.min(), data.max(), ybins)
    elif isinstance(ybins, list):
        ybins = np.linspace(ybins[0], ybins[1], ybins[2])

    if isinstance(xbins, list):
        xbins = np.linspace(xbins[0], xbins[1], xbins[2])
    
    # Bin the agents into groups and calculate the np.histogram for each group.
    hists = []
    agent_coords = []
    for x_bin, g in data.groupby_bins('agent', xbins):
        hist_da = xr.apply_ufunc(hist_func,
                                 g,
                                 kwargs={'bins': ybins},
                                 input_core_dims=[['agent']],
                                 output_core_dims=[[quantity]],
                                 join='exact')
                                #  ,
                                #  dataset_fill_value=np.nan)
        hists.append(hist_da)

        # Use the middle agent as agent coordinate
        agent_coords.append(x_bin.mid)

    # Gather all histograms and assign the coordinates to the new agent dim
    # and the quantity mid-point.
    d = xr.concat(hists, dim='agent')
    quantity_coords = (ybins[1:] + ybins[:-1]) / 2
    d = d.assign_coords({"agent": agent_coords, quantity: quantity_coords})

    # Only display values above a given threshold and transform back to a
    # xr.dataArray to be able to plot a pcolormesh.
    d = d.where(d > threshold)

    if (log):
        d = np.log10(d)

    return d

def calc_hist2D_data_ready(data,
                quantity: Union[str, list] = None,
                *,
                xbins: int = 100,
                ybins: int = 100,
                threshold: float = 0.,
                log: bool = False):
    """Calculate a 2D histogram. 
    
    Bin the agents into xbins groups. 
    """
    # Wrap np.histogram to just return the bin counts
    hist_func = lambda *a, **k: np.histogram(*a, **k)[0]

    if isinstance(ybins, int):
        ybins = np.linspace(data.min(), data.max(), ybins)
    elif isinstance(ybins, list):
        ybins = np.linspace(ybins[0], ybins[1], ybins[2])

    # Bin the agents into groups and calculate the np.histogram for each group.
    hists = []
    agent_coords = []
    for x_bin, g in data.groupby_bins('agent', xbins):
        hist_da = xr.apply_ufunc(hist_func,
                                 g,
                                 kwargs={'bins': ybins},
                                 input_core_dims=[['agent']],
                                 output_core_dims=[[quantity]],
                                 join='exact',
                                 dataset_fill_value=np.nan)
        hists.append(hist_da)

        # Use the middle agent as agent coordinate
        agent_coords.append(x_bin.mid)

    # Gather all histograms and assign the coordinates to the new agent dim
    # and the quantity mid-point.
    d = xr.concat(hists, dim='agent')
    quantity_coords = (ybins[1:] + ybins[:-1]) / 2
    d = d.assign_coords({"agent": agent_coords, quantity: quantity_coords})

    # Only display values above a given threshold and transform back to a
    # xr.dataArray to be able to plot a pcolormesh.
    d = d.where(d > threshold)

    if (log):
        d = np.log10(d)

    return d


def where_bigger(data, *, threshold: float = 0.):
    """Return the xarray data with elements bigger than the threshold
    """
    return data.where(data > threshold)


def calc_compl_cumm_cc(gg, *,
                       itime: bool=-1):
    """Calculate the complementary cummulative connected components distribution
    """
    g = gg.create_graph(at_time_idx=itime)
    cc = [len(s) for s in nx.connected_components(g)]

    [coords, counts] = np.unique(cc, return_counts=True)

    # Sort the coords and counts
    sort_idx = np.argsort(counts)
    coords = np.log10(coords[sort_idx][::-1])
    counts = counts[sort_idx][::-1]

    # Calculate the accumulate sum of counts
    cumsum = np.cumsum(counts)[::-1]
    
    return xr.DataArray(cumsum, name="Connected Component", dims=("Connected Component",), coords={"Connected Component": coords})

def calc_cc(gg, *, at_time_idx: bool=-1):
    """Calculate the connected component sizes
    """
    # Assure that 
    try:
        g = gg.create_graph(at_time_idx=at_time_idx)
        cc = sorted(nx.connected_components(g), key=len, reverse=True)
        cc = [len(s) for s in cc]
    except:
        cc = []

    coords = np.arange(len(cc))

    return xr.DataArray(cc, name="connected component size", dims=("connected component size",), coords={"connected component size": coords})

def fit_powerlaw(data, x_values: int=200, **curve_fit_kwargs):
    """Fit a powerlaw to the two dimensional data

    """
    def powerlaw(x, m, b):
        return np.power(x, m) * b
    def linear(x, m, b):
        return m * x + b

    # Count the values. This works best in pandas
    s = data.to_pandas().stack(0).value_counts()

    # Fitting works the easiest in linear space. 
    # Thus, take the logarithm, fit a linear function and transform the result
    # back.
    coords = np.log10(np.array(s.index))
    counts = np.log10(np.array(s))

    popt, pcov = curve_fit(linear, xdata=coords, ydata=counts, **curve_fit_kwargs)

    m = popt[0]
    b = popt[1]

    sigma_m = pcov[0][0]
    sigma_b = pcov[1][1]

    m_txt = f'({m:.2f} \pm {sigma_m:.2f})'
    b_txt = f'({b:.2f} \pm {sigma_b:.2f})'

    x = np.linspace(s.index.min(), s.index.max(), x_values)
    y = powerlaw(x, m, np.power(10, b))

    result = xr.Dataset()

    result['powerlaw fit'] = xr.DataArray(y, attrs={'fit_eq': f"fit: $f(x) = 10^{{{b_txt}}}\cdot x^{{{m_txt}}}$"}, dims=("fit",), coords={"fit": np.power(10, x)})
    result['popt'] = xr.DataArray(popt, name='popt')
    result['pcov'] = xr.DataArray(pcov, name='pcov')

    return result


def calc_out_degree(edges):
    """Calculate the out degree from the edges container
    """
    return edges.to_pandas().stack(0).value_counts().reset_index(drop=True).rename('out degree').to_xarray() 

def calc_final_out_degree(edges):
    """Calculate the out degree from the edges container
    """
    # TODO FIX
    try: 
        edges = edges.isel(time=-1)
    except:
        return edges

    return edges.isel(time=-1).to_pandas().stack(0).value_counts().reset_index(drop=True).rename('out degree').to_xarray() 