"""Implements the 2d histogram plot function"""

import copy
import logging
from typing import Union, Sequence, Tuple

import xarray as xr
from matplotlib.pyplot import pcolormesh

from dantro.plot_creators.ext_funcs.generic import make_facet_grid_plot

from utopya.plotting import is_plot_func, PlotHelper


# Local variables
log = logging.getLogger(__name__)


# -----------------------------------------------------------------------------


def _lin_func(x, m, b):
    return m*x + b

@is_plot_func()
def investment_velocity(*, data: dict, hlpr: PlotHelper,
              **kwargs) -> None:
    """"""
    r = [2.2, 3, 4., 6.5, 10.]
    m = [2.291e-05, 1.705e-05, 1.474e-05, 9.47e-06, 7.63e-06]
    m_std = [1.488e-06, 1.32e-06, 1.805e-06, 1.863e-06, 2.448e-06]

    hlpr.errorbar(r, m, m_std)


# Fit results: 
# r=2.2
#     mean (degree) polyfit_coefficients  float64 2.291e-05 -4.697
#     std: polyfit_coefficients  (degree) float64 1.488e-06 9.144
# r=3     
#     mean:  polyfit_coefficients  (degree) float64 1.705e-05 1.124
#     std:   polyfit_coefficients  (degree) float64 1.32e-06 2.292

# r=4
#     mean: polyfit_coefficients  (degree) float64 1.474e-05 -0.8365
#     std: polyfit_coefficients  (degree) float64 1.805e-06 3.912

# r=6.5
#     mean: polyfit_coefficients  (degree) float64 9.47e-06 4.401
#     std:     polyfit_coefficients  (degree) float64 1.863e-06 4.829

# r=10
#     mean: polyfit_coefficients  (degree) float64 7.63e-06 -1.321
#     std: polyfit_coefficients  (degree) float64 2.448e-06 13.22

