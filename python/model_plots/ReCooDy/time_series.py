"""ReCooDy-model specific plot function for the state"""

import logging

import numpy as np
import matplotlib.pyplot as plt

from utopya import DataManager, UniverseGroup
from utopya.plotting import is_plot_func, PlotHelper, UniversePlotCreator

# Get a logger
log = logging.getLogger(__name__)

# -----------------------------------------------------------------------------
# -- Single universe plots -- #


@is_plot_func(use_dag=True,
              required_dag_tags=('y',
                                 'quantiles',
                                 'num_quantiles',
                                 'quantile_dim'))
def quantiles(*, data: dict, hlpr: PlotHelper,
              x: str = 'time',
              plot_mean: bool = True,
              plot_median: bool = False,
              fill_between_kwargs: dict = None,
              mean_plot_kwargs: dict = None,
              median_plot_kwargs: dict = None,
              **quantiles_kwargs) -> None:
    """A DAG-based generic quantiles plot.

    Args:
        data (dict): The data returned by the DAG
        hlpr (PlotHelper): The PlotHelper
        x (str, optional): The x-coordinate to be plotted. Defaults to 'time'.
        plot_mean (bool, optional): Whether to plot the mean of the 
            data['quantile_dim'] alongside the quantiles. Defaults to True.
        plot_median (bool, optional): Whether to plot the median of the 
            data['quantile_dim'] alongside the quantiles. Defaults to False.
        fill_between_kwargs (dict, optional): The plot kwargs passed on to 
            matplotlib's plt.fill_between function. Defaults to None.
        mean_plot_kwargs (dict, optional): The plot kwargs passed on to 
            the mean plot. Defaults to None.
        median_plot_kwargs (dict, optional): The plot kwargs passed on to
            the median plot. Defaults to None.
    """
    # Retrieve the data
    y = data['y']
    quantiles = data['quantiles']

    if fill_between_kwargs is None:
        fill_between_kwargs = {}
    if mean_plot_kwargs is None:
        mean_plot_kwargs = {}
    if median_plot_kwargs is None:
        median_plot_kwargs = {}

    if 'alpha' in quantiles_kwargs:
        alpha = quantiles_kwargs.pop('alpha')
    else:
        # Flooring the halved number of quantiles yields the number of
        # fill-betweens
        alpha = 1 / (data['num_quantiles'] // 2)

    for idx in range(data['num_quantiles'] // 2):
        hlpr.ax.fill_between(
            quantiles[x],
            quantiles.isel(quantile=idx),
            quantiles.isel(quantile=quantiles['quantile'].size - idx - 1),
            alpha=alpha, **fill_between_kwargs)

    if plot_median:
        # The median is exactly the quantile at the center, so the 0.5 quantile
        quantiles.isel(quantile=data['num_quantiles'] // 2).plot(
            x=x, ax=hlpr.ax, **median_plot_kwargs)

    if plot_mean:
        y.mean(dim=data['quantile_dim']).plot(
            x=x, ax=hlpr.ax, **mean_plot_kwargs)
